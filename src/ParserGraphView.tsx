import React from "react";
import { ParserGraphModel } from "./ParserGraphModel";
import Graph from "react-graph-vis";

export class GraphColours {
	activeNode   : string;
	inactiveNode : string;
	nodeText     : string;
	line         : string;
	lineText     : string;
	stroke       : string;

	private constructor(activeNode: string, inactiveNode: string, nodeText: string, line: string, lineText: string, stroke: string) {
		this.activeNode   = activeNode;
		this.inactiveNode = inactiveNode;
		this.nodeText     = nodeText;
		this.line         = line;
		this.lineText     = lineText;
		this.stroke       = stroke;
	}

	static LIGHT = new GraphColours("#00B1C1", "#aaaaaa", "#000000", "#808080", "#00627A", "#ffffff");
	static DARK  = new GraphColours("#CC7832", "#666666", "#ffffff", "#AAAAAA", "#FFC66D", "#2b2b2b");

	toString(): string {
		return "{ active: " + this.activeNode + ", inactive: " + this.inactiveNode + ", line: " + this.line + ", text: " + this.nodeText + " }";
	}
}

interface Props {
	model   : ParserGraphModel;
	colours : GraphColours;
}

type VisNode  = { id: number, label: string, title: string, color: string, font: { color: string } };
type VisEdge  = { from: number, to: number, label: string, color: string, font: { color: string } };
type VisGraph = { nodes: VisNode[], edges: VisEdge[] };

type Options  = { height : string };

interface State {
	graph   : VisGraph;
	options : Options;
	colours : GraphColours;
}

export class ParserGraphView extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			graph : {
				nodes: this.props.model.graphNodes.map(node => {
					return {
						id    : node.state,
						label : node.getName(),
						title : node.formatContent(),
						color : node === this.props.model.currentState ? this.props.colours.activeNode : this.props.colours.inactiveNode,
						font  : { color: this.props.colours.nodeText, face: "JetBrains Mono" }
					};
				}),
				edges: this.props.model.graphNodes.map(node => {
					return Array.from(node.neighbours).map(([symbol, neighbour]) => {
						const width = node.state === this.props.model.currentState.state ? 2 : 0.5;
						return {
							from  : node.state,
							to    : neighbour.state,
							label : symbol,
							width : width,
							color : this.props.colours.line,
							font  : { color: this.props.colours.lineText, strokeColor: this.props.colours.stroke, face: "JetBrains Mono" }
						}
					});
				}).flat()
			},
			options : {
				height : "500px"
			},
			colours : this.props.colours
		};

		this.props.model.setView(this);
	}

	updateGraph(nextState: number) {
		this.setState((({ graph: { nodes, edges }, ...rest }) => {
			return {
				graph: {
					nodes: nodes.map(node => {
						if (node.id === nextState) return {
							id    : node.id,
							label : node.label,
							title : node.title,
							color : this.state.colours.activeNode,
							font  : { color: this.state.colours.nodeText }
						};
						return {
							id    : node.id,
							label : node.label,
							title : node.title,
							color : this.state.colours.inactiveNode,
							font  : { color: this.state.colours.nodeText, face: "JetBrains Mono" }
						};
					}),
					edges: edges.map(edge => {
						const width = edge.from === nextState ? 2 : 0.5;
						return {
							...edge,
							width : width,
							color : this.state.colours.line,
							font  : { color: this.state.colours.lineText, strokeColor: this.state.colours.stroke, face: "JetBrains Mono" }
						}
					}),
					...rest
				}
			}
		}));
	}

	// TODO: This does not change the colours immediately.
	//       They are changed, for whatever reason, only when `updateGraph()` is called.
	setNodeColours(colours: GraphColours) {
		this.setState({
			colours : colours
		});
		this.updateGraph(this.props.model.currentState.state);
	}

	override render() {
		return (
			<div>
				{ this.state.graph !== null && <Graph graph={this.state.graph} options={this.state.options}/> }
			</div>
		);
	}
}
