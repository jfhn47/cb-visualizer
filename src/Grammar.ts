import { Comparable, mkString, exists, foldLeft } from "./util";

/**
 * Maps a symbol (non-terminal) to a two-dimensional array of symbols (handles).
 * The outer array contains the handles and the inner array contains the elements
 * of one handle.
 */
export type Rules = Map<string, string[][]>;

export enum TokenType {
	Symbol,
	Arrow,
	Separator
}

/**
 * Contains the data for a provided grammar.
 * @var symbols      The symbols of the grammar.
 * @var terminals    The terminals of the grammar.
 * @var nonTerminals The non-terminals of the grammar.
 * @var rules        The rules of the grammar.
 */
export class Grammar {
	// TODO: Make them private?
	symbols      : Set<string> = new Set();
	terminals    : Set<string> = new Set();
	nonTerminals : Set<string> = new Set();
	rules        : Rules       = new Map();

	extendedStartSymbol : string;
	startSymbol         : string;
	emptyWord           : string;

	constructor(startSymbol: string = "S", emptyWord: string = "epsilon") {
		this.startSymbol         = startSymbol;
		this.emptyWord           = emptyWord;
		this.extendedStartSymbol = startSymbol + "'";

		// Add S' -> S
		this.symbols.add(this.extendedStartSymbol);
		this.nonTerminals.add(this.extendedStartSymbol);
		this.rules.set(this.extendedStartSymbol, [[this.startSymbol]]);
	}

	/**
	 * Creates a new grammar object for a given input string.
	 * Use a try-catch-block when calling this method.
	 * The format for the input has to have the following form:
	 * <Non-Terminal> = <HandleList>
	 * <HandleList>   = <Handle> <HandleList> | <Handle>
	 * <Handle>       = <Terminal> | <Non-Terminal>
	 *
	 * @param text        The input text.
	 * @param startSymbol The start symbol.
	 * @param emptyWord   The empty word.
	 * @returns A new grammar object if the input is valid.
	 * @throws Error when the input is invalid.
	 */
	static read(text: string, startSymbol: string = "S", emptyWord: string = "epsilon"): Grammar {
		let result = new Grammar(startSymbol, emptyWord);

		const tokens = tokenize(text);
		parseTokens(tokens);

		let currentTokenIndex = 0;

		const currentToken = ()               : Token | undefined => tokens[currentTokenIndex];
		const peekToken    = (offset: number) : Token | undefined => tokens[currentTokenIndex + offset];
		const advanceToken = (st: number = 1) : void              => { currentTokenIndex += st };

		const isRuleBegin  = (): boolean => exists(currentToken()) && currentToken()!.type === TokenType.Symbol && exists(peekToken(1)) && peekToken(1)!.type === TokenType.Arrow;
		const isRuleEnd    = (): boolean => exists(peekToken(1)) && peekToken(1)!.type === TokenType.Arrow;

		let safety1 = 0;
		let safety2 = 0;

		while (isRuleBegin() && safety1 < 10000) {
			const lhs = currentToken()!.value!
			result.addSymbol(lhs);
			result.addNonTerminal(lhs);
			advanceToken(2);

			let rhs: string[][] = [[]];
			let idx: number     = 0;
			do {
				++safety2;
				if (!exists(currentToken())) { break; }

				if (currentToken()!.type === TokenType.Symbol) {
					rhs[idx].push(currentToken()!.value!);
					result.addSymbol(currentToken()!.value!);
				} else if (currentToken()!.type === TokenType.Separator) {
					rhs.push([]);
					++idx;
				}
				advanceToken();
			} while (!isRuleEnd() && safety2 < 100000);

			result.addRule(lhs, rhs);
		}

		result.genTerminals();

		checkGrammar(result);

		return result;
	}

	/**
	 * Adds a symbol to the grammar.
	 * @param s The new symbol.
	 */
	addSymbol(s: string) {
		this.symbols.add(s);
	}

	/**
	 * Adds a non terminal to the grammar.
	 * @param nt The new non terminal.
	 */
	addNonTerminal(nt: string) {
		this.nonTerminals.add(nt);
	}

	/**
	 * Adds a rule to the grammar.
	 * @param lhs The non terminal on the left hand side.
	 * @param rhs The handles on the right hand side.
	 */
	addRule(lhs: string, rhs: string[][]) {
		if (this.rules.has(lhs)) {
			// Add the new rhs to the already existing rhs'.
			this.rules.set(lhs, this.rules.get(lhs)!.concat(rhs));
		} else {
			this.rules.set(lhs, rhs);
		}
	}

	/**
	 * Generates the terminals.
	 * Every symbol that is not a non terminal is a terminal.
	 */
	genTerminals() {
		this.symbols.forEach(sym => {
			if (!this.nonTerminals.has(sym)) {
				this.terminals.add(sym);
			}
		});
	}

	/**
	 * Checks if a symbol is a non terminal.
	 * @param symbol The symbol to check.
	 * @returns If a symbol is a non terminal.
	 */
	isNonTerminal = (symbol: string): boolean => this.nonTerminals.has(symbol);

	// NOTE: This has to be an arrow function because `this` is somehow undefined,
	//       when called from the `ShiftReduceModel`
	/**
	 * Checks if a symbol is a terminal.
	 * @param symbol The symbol to check.
	 * @returns If a symbol is a terminal.
	 */
	isTerminal = (symbol: string) => this.terminals.has(symbol);

	/**
	 * @returns An array of terminals without the epsilon.
	 */
	getTerminalsAsArray(): string[] {
		return foldLeft((acc: string[], val: string) => {
			if (val !== this.emptyWord) { return acc.concat(val); }
			else { return acc; }
		})([])(Array.from(this.terminals));
	}

	/**
	 * @returns An array of non terminals without the `S'`.
	 */
	getNonTerminalsAsArray(): string[] {
		return foldLeft((acc: string[], val: string) => {
			if (val !== this.extendedStartSymbol) { return acc.concat(val); }
			else { return acc; }
		})([])(Array.from(this.nonTerminals));
	}

	format(): string {
		let str = "";
		for (const rule of this.rules) {
			str += rule[0] + " -> " + mkString(rule[1], _ => mkString(_, _ => _ + " ", 1) + " | ", 3) + "\n";
		}
		return str;
	}

	clone(): Grammar {
		let grammar = new Grammar(this.startSymbol, this.emptyWord);
		grammar.symbols      = new Set(this.symbols);
		grammar.terminals    = new Set(this.terminals);
		grammar.nonTerminals = new Set(this.nonTerminals);
		grammar.rules        = new Map(this.rules);
		return grammar;
	}

	toString(): string {
		let str = "";
		str += "Symbols: " + mkString(Array.from(this.symbols), _ => _ + ", ") + "\n";
		str += "Terminals: " + mkString(Array.from(this.terminals), _ => _ + ", ") + "\n";
		str += "Non Terminals: " + mkString(Array.from(this.nonTerminals), _ => _ + ", ") + "\n";
		str += "Rules:\n";
		this.rules.forEach((rhs, lhs) => {
			const rhsStr = mkString(rhs, _ => mkString(_, _ => _ + " ", 1) + " | ", 3);
			str += "    " + lhs + " -> " + rhsStr + "\n";
		});
		return str;
	}
}

export class InputError extends Error {
	private readonly msg: string;
	constructor(msg: string) {
		super(msg);
		this.msg = msg;
		Object.setPrototypeOf(this, InputError.prototype);
	}
	get = (): string => this.msg;
}

export class Token implements Comparable<Token> {
	type  : TokenType;
	value : string;

	constructor(type: TokenType, value: string) {
		this.type  = type;
		this.value = value;
	}

	equals(other: Token): boolean {
		if (this.type !== other.type) { return false; }
		if (this.type === TokenType.Symbol && this.value !== other.value) { return false; }
		return true;
	}

	toString(): string {
		if (this.type === TokenType.Symbol) { return "Sym: " + this.value; }
		return this.type === TokenType.Arrow ? "Arrow" : "Separator";
	}
}

const tokenize = (text: string): Token[] => {
	let result: Token[] = [];

	const isWhitespace = (c: string): boolean => exists(c) && c.match(/[ \t\n\r]/)       !== null;
	const isSymbol     = (c: string): boolean => exists(c) && c.match(/[^ \t\n\r]/) !== null;
	const isArrowBegin = (c: string): boolean => exists(c) && c.match(/[-=]/)            !== null;
	const isArrowEnd   = (c: string): boolean => exists(c) && c === ">";
	const isColon      = (c: string): boolean => exists(c) && c === ":";
	const isEqualsSym  = (c: string): boolean => exists(c) && c === "=";
	const isSeparator  = (c: string): boolean => exists(c) && c === "|";

	const handleRuleDefineSymbol = (text: string, i: number): number => {
		if (isArrowBegin(text[i]) && isArrowEnd(text[i + 1])) {
			result.push(new Token(TokenType.Arrow, text.substring(i, i + 2)));
			return 2;
		} else if (isColon(text[i])) {
			if (isColon(text[i + 1]) && isEqualsSym(text[i + 2])) {
				result.push(new Token(TokenType.Arrow, "::="));
				return 3;
			} else {
				result.push(new Token(TokenType.Arrow, ":"));
				return 1;
			}
		}
		return 0;
	}

	for (let i = 0; i < text.length;) {
		let cc = text[i];

		while (isWhitespace(cc)) {
			cc = text[++i];
		}

		const offset = handleRuleDefineSymbol(text, i);
		if (offset > 0) {
			i += offset;
			continue;
		}

		if (isSeparator(cc)) {
			result.push(new Token(TokenType.Separator, "|"));
			i += 1;
			continue;
		}

		let symValue = "";
		while (isSymbol(cc)) {
			symValue += cc;
			cc = text[++i];
		}

		if (symValue.trim().length === 0) { continue; }

		result.push(new Token(TokenType.Symbol, symValue));
	}

	return result;
}

const parseTokens = (tokens: Token[]) => {
	let lastType: TokenType | undefined = undefined;

	for (let idx = 0; idx < tokens.length; ++idx) {
		if (!exists(tokens[idx])) { break; }

		const ct = tokens[idx]!;

		if (lastType === TokenType.Arrow && ct.type !== TokenType.Symbol) {
			throw new InputError("A symbol must follow an arrow.");
		}

		if (lastType === TokenType.Separator && ct.type !== TokenType.Symbol) {
			throw new InputError("A symbol must follow a separator.");
		}

		lastType = ct.type;
	}
}

const checkGrammar = (grammar: Grammar) => {
	if (!grammar.symbols.has(grammar.startSymbol)) {
		throw new InputError("The start symbol is not defined.");
	}

	if (!grammar.nonTerminals.has(grammar.startSymbol)) {
		throw new InputError("The start symbol is not a non terminal.");
	}

	if (grammar.terminals.size === 0) {
		throw new InputError("The grammar does not have any terminals.");
	}

	grammar.getNonTerminalsAsArray().forEach(nt => {
		let hasAppeared = false;
		grammar.rules.forEach(_ => _.forEach(rule => {
			if (rule.includes(nt)) { hasAppeared = true; }
		}));
		if (!hasAppeared) {
			throw new InputError("Non Terminal '" + nt + "' cannot be reached.");
		}
	});
}

export const testCall_tokenize     = (text: string)     => tokenize(text);
export const testCall_parseTokens  = (tokens: Token[])  => parseTokens(tokens);
// export const testCall_checkGrammar = (grammar: Grammar) => checkGrammar(grammar);
