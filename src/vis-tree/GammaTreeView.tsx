import {GammaTreeNode, TreeDirection, TreeInstructionError} from './GammaTree';
import {useEffect, useRef, useState} from 'react';
import {assert, exists, mkString, noop} from '../util';
import {Automaton, UnificationError} from './Automaton';

interface GammaTreeViewProps {
	automaton: Automaton;
}

type Vec2 = {x: number, y: number};

const format = {
	nodeRadius: 15,
	horizontalDistance: 50,
	verticalDistance: 50
};

let lastRefresh = new Date().getTime();

const shouldRefresh = (delta: number): boolean => {
	const now = new Date().getTime();
	if (now - lastRefresh > delta) {
		lastRefresh += delta;
		return true;
	}
	return false;
}

class TreeScaffoldNode {
	readonly pos: Vec2;
	readonly parent: TreeScaffoldNode | null;
	leftDesc: TreeScaffoldNode | null = null;
	rightDesc: TreeScaffoldNode | null = null;

	constructor(x: number, y: number, parent: TreeScaffoldNode | null = null) {
		this.pos = {x: x, y: y};
		this.parent = parent;
	}

	toString = (): string => `{x: ${this.pos.x}, y: ${this.pos.y}}`;
}

export const GammaTreeView = (props: GammaTreeViewProps) => {
	assert(exists(props.automaton));

	const tree = props.automaton.history.top()!.tree;

	const canvasRef = useRef<HTMLCanvasElement | null>(null);

	const [mousePosition, setMousePosition] = useState<Vec2 | null>(null);

	const [cameraPosition, setCameraPosition] = useState<Vec2>({x: 0, y: 0});
	const [cameraZoom,     setCameraZoom]     = useState<number>(1);

	// This is a bad solution but it works for now.
	const [, update] = useState<{}>({});

	noop(cameraZoom, setCameraZoom, TreeScaffoldNode);

	useEffect(() => {
		const ref = canvasRef.current!;
		const canvas = ref.getContext("2d")!;
		ref.width  = ref.clientWidth;
		ref.height = ref.clientHeight;
		canvas.clearRect(0, 0, ref.width, ref.height);
		const scaffold = new TreeScaffoldNode(ref.width / 2, 50);
		buildScaffoldTree([scaffold], tree.depth, ref.width);
		drawTreeNode(canvas, tree.root, scaffold, tree.treePath);
	});

	const buildScaffoldTree = (lastLevel: TreeScaffoldNode[], depth: number, canvasWidth: number) => {
		if (depth <= 0) {
			return;
		}

		const nextLevel: TreeScaffoldNode[] = [];

		const childrenCount = 2 * lastLevel.length;
		const space = canvasWidth / childrenCount;
		let xOffset = 0;

		for (const node of lastLevel) {
			node.leftDesc = new TreeScaffoldNode(xOffset + space / 2, node.pos.y + format.verticalDistance, node);
			node.rightDesc = new TreeScaffoldNode(xOffset + space + space / 2, node.pos.y + format.verticalDistance, node);
			xOffset += 2 * space;
			nextLevel.push(node.leftDesc, node.rightDesc);
		}

		buildScaffoldTree(nextLevel, depth - 1, canvasWidth);
	}

	const onMousePress = (event: any) => {
		const rect = canvasRef.current!.getBoundingClientRect();
		setMousePosition({
			x: event.clientX - rect.left,
			y: event.clientY - rect.top
		});
	}

	// TODO: Add `TouchEvent`.
	const onDragMouse = (event: any) => {
		if (exists(mousePosition) && shouldRefresh(16)) {
			const rect = canvasRef.current!.getBoundingClientRect();
			// console.log(`pageX = ${event.clientX}, clientY = ${event.clientY}`);
			const newMousePosition = {x: event.clientX - rect.left, y: event.clientY - rect.top};
			setCameraPosition({
				x: cameraPosition.x + (newMousePosition.x - mousePosition!.x),
				y: cameraPosition.y + (newMousePosition.y - mousePosition!.y)
			});
			setMousePosition({
				x: event.clientX - rect.left,
				y: event.clientY - rect.top
			});
		}
	}

	const onMouseRelease = (_: any) => {
		setMousePosition(null);
	}

	const drawLine = (canvas: CanvasRenderingContext2D, from: Vec2, to: Vec2) => {
		// console.log(`Draw from (${from.x}, ${from.y}) to (${to.x}, ${to.y})`);
		canvas.beginPath();
		canvas.moveTo(from.x + cameraPosition.x, from.y + cameraPosition.y);
		canvas.lineWidth = 1;
		canvas.lineTo(to.x + cameraPosition.x, to.y + cameraPosition.y);
		canvas.stroke();
	}

	const checkPath = (path: TreeDirection[] | null, expected: TreeDirection): TreeDirection[] | null =>
		exists(path) && path!.length > 0 && path![0] === expected ? path!.slice(1) : null;

	const drawPointerArrow = (canvas: CanvasRenderingContext2D, x: number, y: number) => {
		canvas.moveTo(x + cameraPosition.x, y + cameraPosition.y);
		canvas.lineTo(x + cameraPosition.x + 25, y + cameraPosition.y);
		canvas.moveTo(x + cameraPosition.x + 15, y + 5 + cameraPosition.y);
		canvas.lineTo(x + cameraPosition.x + 25, y + cameraPosition.y);
		canvas.moveTo(x + cameraPosition.x + 15, y - 5 + cameraPosition.y);
		canvas.lineTo(x + cameraPosition.x + 25, y + cameraPosition.y);
		canvas.stroke();
	}

	const drawTreeNode = (canvas: CanvasRenderingContext2D, node: GammaTreeNode, scaffoldNode: TreeScaffoldNode, path: TreeDirection[] | null) => {
		if (exists(node.leftDesc)) {
			drawLine(canvas, scaffoldNode.pos, scaffoldNode.leftDesc!.pos);
			const restPath = checkPath(path, TreeDirection.LEFT);
			drawTreeNode(canvas, node.leftDesc!, scaffoldNode.leftDesc!, restPath);
		}

		if (exists(node.rightDesc)) {
			drawLine(canvas, scaffoldNode.pos, scaffoldNode.rightDesc!.pos);
			const restPath = checkPath(path, TreeDirection.RIGHT);
			drawTreeNode(canvas, node.rightDesc!, scaffoldNode.rightDesc!, restPath);
		}

		const x = scaffoldNode.pos.x;
		const y = scaffoldNode.pos.y;

		canvas.moveTo(x, y);
		canvas.beginPath();
		canvas.arc(x + cameraPosition.x, y + cameraPosition.y, format.nodeRadius, 0, 2 * Math.PI);

		if (path?.length === 0) {
			drawPointerArrow(canvas, x - 50, y);
		}

		canvas.fillStyle = "lightgrey";
		canvas.fill();

		canvas.lineWidth = 1;
		canvas.strokeStyle = "black";
		canvas.stroke();

		canvas.font = "20px Computer Modern";
		canvas.textAlign = "center";
		canvas.fillStyle = "black";
		canvas.fillText(node.symbol, x + cameraPosition.x, y + cameraPosition.y + 7);
	}

	const updateAutomaton = (f: () => Automaton) => {
		try {
			f();
		} catch (e: unknown) {
			if (e instanceof UnificationError || e instanceof TreeInstructionError) {
				alert(e.message);
			} else {
				alert(`Unexpected Error: ${(e as Error).message}`);
			}
		}
		update({});
	}

	return <div>
		<div>
			<div>
				<button
					onClick={() => updateAutomaton(props.automaton!.step)}
				>Step</button>
				<button
					onClick={() => updateAutomaton(props.automaton!.unstep)}
				>Unstep</button>
			</div>
		</div>
		<div>
			Input: {props.automaton!.history.top()!.input}
			<br/>
			State: {props.automaton!.history.top()!.state}
			<br/>
			Pointer: {props.automaton!.getTreePointerKind(props.automaton!.history.top()).toString()}
			<br/>
			End States: {mkString(props.automaton!.endStates)}
		</div>
		<div style={{height: "80vh"}}>
			<div style={{height: "80vh"}}>
				<canvas
					ref={canvasRef}
					style={{width: "100%", height: "100%"}}
					onMouseDown={onMousePress}
					onMouseMove={onDragMouse}
					onMouseUp={onMouseRelease}
				/>
			</div>
		</div>
	</div>;
}
