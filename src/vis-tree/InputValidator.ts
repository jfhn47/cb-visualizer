import {Action, Direction, MappingLeft, MappingRight, NodeKind, NodeType, popInstruction, PushInstruction, HasDesc, TreeLabel} from "./InputModel";
import * as E from "fp-ts/Either";
import {throwExpr, assert} from "../util";

// Input example: (q0, some text $, (-, -, +))
export const validateLeftMapping = (input: string): E.Either<Error, MappingLeft> => {
	try {
		const matches = leftMappingRegex.exec(input) ?? throwExpr(new InputError("Invalid left side syntax."));
		assert(matches.length === 5, `Actual length: ${matches.length}`);

		const state = stateRegex.exec(matches[1]) ?? throwExpr(new InputError("Invalid state name."));
		const word  = inputRegex.exec(matches[2]) ?? throwExpr(new InputError("Invalid input sequence."));
		const typ   = parseType(matches[3])
		const label = treeLabelRegex.exec(matches[4]) ?? throwExpr(new InputError("Invalid tree symbol."));

		return E.right(new MappingLeft(state[1], word[1], typ, TreeLabel.of(label[1])));
	} catch (e: unknown) {
		if (e instanceof InputError) {
			return E.left(new Error(`Input error: ${e.get()}`));
		} else {
			return E.left(new Error(`Unexpected error: ${e}`));
		}
	}
}

// Input example: (q1, dl) = move left.
// Definition: (state, (direction | pop | push(label, [dl])))
export const validateRightMapping = (input: string): E.Either<Error, MappingRight> => {
	try {
		if (input.charAt(0) === "(") {
			if (input.charAt(input.length - 1) === ")") {
				input = input.substring(1, input.length - 1);
			} else {
				throw new InputError("Invalid right side syntax.");
			}
		}

		const sepIndex = input.indexOf(",");
		const matches  = [input.slice(0, sepIndex).trim(), input.slice(sepIndex + 1).trim()];

		const state = stateRegex.exec(matches[0]) ?? throwExpr(new InputError("Invalid state name."));
		const action = parseAction(matches[1]);

		return E.right(new MappingRight(state[1], action));
	} catch (e: unknown) {
		if (e instanceof InputError) {
			return E.left(new Error(`Input error: ${e.get()}`));
		} else {
			return E.left(new Error(`Unexpected error: ${e}`));
		}
	}
}

// Abstract: (state, input $, ([-lr], [*+-], [*+-]))
const leftMappingRegex  = /\(?\s*(.*)\s*,\s*(.*)\s*,\s*(\(.*\))\s*,\s*(.*)\s*\)?$/;
const stateRegex        = /([^,]+)/;
const inputRegex        = /([^,]+)/;
const treeLabelRegex    = /([^,]+)/;
const typeRegex         = /\(\s*(.*)\s*,\s*(.*)\s*,\s*(.*)\s*\)/;
const t1Regex           = /\s*([-lr*])\s*/;
const t2t3Regex         = /\s*([*+-])\s*/;
const pushRegex         = /^push\s*\(\s*(\s*[^,]+)\s*,\s*([lr])\s*\)\)*$/;
const directionRegex    = /^[surl]\)*$/;
const popRegex          = /^pop\)*$/;

// ([-lr], [*+-], [*+-])
const parseType = (input: string): NodeType => {
	const matches = typeRegex.exec(input) ?? throwExpr(new InputError("Invalid type syntax. Use ([-lr], [*+-], [*+-])."));

	const kindMatch  = t1Regex.exec(matches[1])   ?? throwExpr(new InputError(`Invalid t1 with '${matches[1]}'`));
	const leftMatch  = t2t3Regex.exec(matches[2]) ?? throwExpr(new InputError(`Invalid t2 with '${matches[2]}'`));
	const rightMatch = t2t3Regex.exec(matches[3]) ?? throwExpr(new InputError(`Invalid t2 with '${matches[3]}'`));

	const kind     = NodeKind.of(kindMatch[1]);
	const hasLeft  = HasDesc.of(leftMatch[1]);
	const hasRight = HasDesc.of(rightMatch[1]);

	return new NodeType(kind, hasLeft, hasRight);
}

// direction | pop | push(label, [dl])
const parseAction = (input: string): Action => {
	if (directionRegex.test(input)) {
		return Direction.of(input.charAt(0));
	} else if (popRegex.test(input)) {
		return popInstruction;
	}

	const match = pushRegex.exec(input) ?? throwExpr(new InputError(`Invalid push syntax: '${input}'`));

	// The match cannot be `null` because it was matched in `validateRightMapping`.
	assert(match!.length === 3, `Actual length = ${match!.length}`);

	// Capture groups begin at 1.
	const label = match![1];

	// When the string is not "l", it has to be "r" because the regex matched.
	const direction = Direction.of(match![2]); // match[2] === "l" ? Direction.Kind.LEFT : Direction.Kind.RIGHT;

	return new PushInstruction(label, direction);
}

class InputError extends Error {
	private readonly msg: string;

	constructor(msg: string) {
		super(msg);
		this.msg = msg;

		Object.setPrototypeOf(this, InputError.prototype);
	}

	get = (): string => this.msg;
}
