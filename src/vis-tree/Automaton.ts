import {InputModel, State, NodeKind, NodeType, Symbol, HasDesc, MappingTuple, PushInstruction, popInstruction, Direction, TreeLabel} from "./InputModel";
import {assert, mkString, Stack, exists, anyMatch} from "../util";
import {GammaTree, TreeDirection} from "./GammaTree";
import {MarkedRules} from "./VisTree";

class InternalState {
	state    : State;
	input    : Symbol[];
	tree     : GammaTree;
	lastRule : MappingTuple | null;

	constructor(state: State, input: Symbol[], tree: GammaTree, lastRule: MappingTuple | null) {
		this.state    = state;
		this.input    = input;
		this.tree     = tree;
		this.lastRule = lastRule;
	}

	toString = (): string => `Internal State: State = ${this.state}, Input = ${mkString(this.input, _ => _ + " ", 1)}, Rule = ${this.lastRule![0] + " -> " + this.lastRule![1]}`;
}

type RowHighlightFunc = (rules: MarkedRules) => void;

export class Automaton {
	private readonly startState: State;
	readonly endStates: State[];
	private readonly input: InputModel;
	readonly history: Stack<InternalState> = new Stack();
	private readonly addNextAndLastRowHighlight: RowHighlightFunc;
	private nextRule: MappingTuple | null = null;

	constructor(startState: State, endStates: State[], input: InputModel, addNextAndLastRowHighlight: RowHighlightFunc, viewOnly: boolean = false) {
		this.startState = startState;
		this.endStates = endStates;
		this.input = input;
		this.addNextAndLastRowHighlight = addNextAndLastRowHighlight;

		if (viewOnly) {
			return;
		}

		const initialState = new InternalState(this.startState, this.input.input, GammaTree.of(input.treeAlphabet), null);
		this.getNextRule(initialState);
		initialState.lastRule = this.nextRule;
		this.history.push(initialState);

		console.assert(exists(this.nextRule), "No matching rule found at start.");

		// TODO: This leads to an infinite rerender at the moment.
		// this.addRowAsNextMatch(this.nextRule![2]);
	}

	toString = () => `Automaton:\nStart state: ${this.startState}\nEnd States: { ${mkString(this.endStates)} }\n${this.input.toString()}`;

	/**
	 * @mutate `this.nextRule`
	 */
	getNextRule = (state: InternalState = this.history.top()): MappingTuple | null => {
		try {
			this.nextRule = this._getNextRule(state.state, state.input[0], this.getTreePointerKind(state), this.getTreePointerSymbol(state));
		} catch (e: unknown) {
			if (e instanceof UnificationError) {
				console.log(e.msg);
			}
			throw e;
		}
		return this.nextRule;
	}

	step = (): Automaton => {
		// console.log(this.history);
		assert(this.history.size() > 0, "State history is empty.");
		const state = this.history.top();
		// console.log(`state = ${state.state.toString()}`);
		// console.log(`this.history.size() = ${this.history.size()}`);
		// console.log(`Remaining input: ${state.input}`);

		this.simulate(state, this.nextRule!, this.nextRule![0].input !== InputModel.emptyWord);
		const lastRule = this.nextRule;

		const newState = this.history.top().state;

		if (anyMatch(endState => endState === newState)(this.endStates) && state.input[0] === InputModel.endMarker) {
			alert("Finished");
			return this;
		}

		this.getNextRule(this.history.top());
		this.addNextAndLastRowHighlight({last: lastRule![2], next: this.nextRule![2]});

		return this;
	}

	unstep = (): Automaton => {
		if (this.history.size() > 1) {
			this.history.pop();
		}
		const lastRule = this.history.top().lastRule;
		this.getNextRule();
		this.addNextAndLastRowHighlight({last: lastRule![2], next: this.nextRule![2]});
		return this;
	}

	private _getNextRule = (state: State, input: Symbol, ptr: NodeType, treeSymbol: Symbol) => {
		let rule  : MappingTuple     | null = null;
		let error : UnificationError | null = null;

		// console.log(`State = ${state}, input = ${input}, ptr = ${ptr}`);

		for (const mapping of this.input.mappings) {
			// console.log(mapping);

			const stateKind     = this.freshNodeKind(ptr.kind);
			const stateHasLeft  = this.freshHasDesc(ptr.hasLeft);
			const stateHasRight = this.freshHasDesc(ptr.hasRight);
			const stateLabel    = this.freshLabel(new TreeLabel.Literal(treeSymbol));

			const ruleKind      = this.freshNodeKind(mapping[0].typ.kind);
			const ruleHasLeft   = this.freshHasDesc(mapping[0].typ.hasLeft);
			const ruleHasRight  = this.freshHasDesc(mapping[0].typ.hasRight);
			const ruleLabel     = this.freshLabel(mapping[0].label);

			try {
				this.unifyState(mapping[0].state, state);
				if (mapping[0].input !== InputModel.emptyWord) {
					this.unifyInputWord(mapping[0].input, input);
				}
				this.unifyNodeKind(ruleKind, stateKind);
				this.unifyHasDesc(ruleHasLeft, stateHasLeft);
				this.unifyHasDesc(ruleHasRight, stateHasRight);
				this.unifyTreeSymbol(ruleLabel, stateLabel);
				rule = mapping;
				error = null;
				break;
			} catch (e: unknown) {
				if (e instanceof UnificationError) {
					error = e;
				} else {
					assert(false, `Unexpected error: ${JSON.stringify(e)}`);
				}
			}
		}

		if (exists(error)) {
			throw error;
		}

		if (!exists(rule)) {
			throw new UnificationError("No rule matched the input.");
		}

		return rule;
	}

	private simulate = (state: InternalState, rule: MappingTuple, dropFirst: boolean = true) => {
		// console.log(`Simulate. Rule = ${rule.toString()}`);
		const rhs = rule[1];
		const nextTree = state.tree.clone();
		// console.log(nextTree);

		if (rhs.action instanceof PushInstruction) {
			const inst = rhs.action as PushInstruction;
			nextTree.pushNode(TreeDirection.of(inst.direction.dir), inst.label);
		} else if (rhs.action === popInstruction) {
			nextTree.popNode();
		} else if (rhs.action instanceof Direction) {
			switch ((rhs.action as Direction).dir) {
				case "u":
					nextTree.moveUp();
					break;
				case "l":
					nextTree.moveLeft();
					break;
				case "r":
					nextTree.moveRight();
					break;
			}
		}

		const nextInput = dropFirst ? state.input.slice(1) : [...state.input];
		this.history.push(new InternalState(rhs.state, nextInput, nextTree, rule));
	}

	getTreePointerKind = (state: InternalState): NodeType => {
		const node = state.tree.walkTree();
		return new NodeType(
			new NodeKind.Literal(node.kind),
			new HasDesc.Literal(exists(node.leftDesc)),
			new HasDesc.Literal(exists(node.rightDesc))
		);
	}

	getTreePointerSymbol = (state: InternalState): Symbol => state.tree.walkTree().symbol;

	private unifyState = (a: State, b: State) => {
		// console.log(`a = ${a}, b = ${b}`);
		if (a !== b) {
			throw new UnificationError(`State mismatch: a = ${a}, b = ${b}`);
		}
	}

	private unifyInputWord = (a: Symbol, b: Symbol) => {
		// console.log(`a = ${a}, b = ${b}`);
		if (a !== b) {
			throw new UnificationError(`Input Symbol mismatch: a = ${a}, b = ${b}`);
		}
	}

	private unifyTreeSymbol = (a: TreeLabel.Base, b: TreeLabel.Base) => {
		if (a instanceof TreeLabel.Literal && b instanceof TreeLabel.Literal) {
			const ak = (a as TreeLabel.Literal).value;
			const bk = (b as TreeLabel.Literal).value;
			if (ak !== bk) {
				// Cannot be unified. How to communicate?
				throw new UnificationError(`Literals do not match: a = '${a}', b = '${b}'`);
			}
		} else if (a instanceof TreeLabel.Mvar) {
			const mvar = (a as TreeLabel.Mvar);
			if (exists(mvar.instance)) {
				if (this.occurs(b, mvar)) {
					throw new UnificationError("Recursive unification in rules.");
				}
			} else {
				// Unification succeeded?
				mvar.instance = b; // Mutate a
			}
		} else if (b instanceof TreeLabel.Mvar) {
			this.unifyNodeKind(b, a);
		} else {
			throw new UnificationError("Unification error.");
		}
	}

	private unifyNodeKind = (a: NodeKind.Base, b: NodeKind.Base) => {
		// console.log(`a = ${a}, b = ${b}`);
		if (a instanceof NodeKind.Literal && b instanceof NodeKind.Literal) {
			const ak = (a as NodeKind.Literal).kind;
			const bk = (b as NodeKind.Literal).kind;
			if (ak !== bk) {
				// Cannot be unified. How to communicate?
				throw new UnificationError(`Literals do not match: a = '${a}', b = '${b}'`);
			}
		} else if (a instanceof NodeKind.Mvar) {
			const mvar = (a as NodeKind.Mvar);
			if (exists(mvar.instance)) {
				if (this.occurs(b, mvar)) {
					throw new UnificationError("Recursive unification in rules.");
				}
			} else {
				// Unification succeeded?
				mvar.instance = b; // Mutate a
			}
		} else if (b instanceof NodeKind.Mvar) {
			this.unifyNodeKind(b, a);
		} else {
			throw new UnificationError("Unification error.");
		}
	}

	private unifyHasDesc = (a: HasDesc.Base, b: HasDesc.Base) => {
		// console.log(`a = ${a}, b = ${b}`);
		if (a instanceof HasDesc.Literal && b instanceof HasDesc.Literal) {
			const ad = (a as HasDesc.Literal).hasDesc;
			const bd = (b as HasDesc.Literal).hasDesc;
			if (ad !== bd) {
				throw new UnificationError(`Literals do not match: a = '${a}', b = '${b}'`);
			}
		} else if (a instanceof HasDesc.Mvar) {
			const mvar = a as NodeKind.Mvar;
			if (exists(mvar.instance)) {
				if (this.occurs(b, mvar)) {
					throw new UnificationError("Recursive unification in rules.");
				}
			} else {
				mvar.instance = b; // Mutate a
			}
		} else if (b instanceof HasDesc.Mvar) {
			this.unifyHasDesc(b, a);
		} else {
			throw new UnificationError("Unification error.");
		}
	}

	/*

Referenz:
https://gist.github.com/louthy/bafb2b8b5701c0842ca405c638b58e80

unifyNodeKind = (a: NodeKind, b: NodeKind) => {
	if (a instanceof NodeKindLiteral && a instanceof NodeKindLiteral) {
		val ak = (a as NodeKindLiteral).kind;
		val bk = (b as NodeKindLiteral).kind;
		if (ak == bk) // good
		else // Error
	} else if (a instanceof MVar_NodeKind) {
		const mvar = (a as NodeKind.MVar);
		if (exists(mvar.instance)) {
			if (occurs(b, mvar)) {
				// Recursive meta var error.
			}
			unifyNodeKind(mvar.instance, b);
		} else {
			mvar.instance = b;
		}
	} else if (b instanceof MVar_NodeKind) {
		unifyNodeKind(b, a);
	} else {
		// Error
	}
}

occurs = (a: NodeKind, b: NodeKind.MVar) => a match {
	case _: Literal => false
	case a: NodeKind.MVar if a == b => true
	case a: NodeKind.MVar if exists(a.instance) => occurs(a.instance, b)
	case _ => false
}

fresh = (kind: NodeKind, mapping = Map<MVar, MVar>) => {
	if (kind instanceof NodeKind.Mvar) {
		const mvar = (a as NodeKind.MVar);
		if (mvar.
	} else {

	}
}

	 */

	/**
	 * Checks for cycles in meta variable substitutions.
	 */
	private occurs = (base: NodeKind.Base, mvar: NodeKind.Mvar): boolean => {
		if (base instanceof NodeKind.Literal) {
			return false;
		} else if (base instanceof NodeKind.Mvar) {
			if (base === mvar) {
				return true;
			}
			if (exists(base.instance)) {
				return this.occurs(base.instance!, mvar);
			}
		}
		return false;
	}

	/**
	 * Copies the mapping and creates a new version with meta variables.
	 */
	private freshNodeKind = (kind: NodeKind.Base): NodeKind.Base => {
		if (kind instanceof NodeKind.Literal) {
			return new NodeKind.Literal(kind.toString());
		}
		const mvar = (kind as NodeKind.Mvar);
		const result = new NodeKind.Mvar(mvar.id + 1);
		result.instance = mvar.instance;
		return result;
	}

	private freshHasDesc = (hasDesc: HasDesc.Base): HasDesc.Base => {
		if (hasDesc instanceof HasDesc.Literal) {
			return new HasDesc.Literal((hasDesc as HasDesc.Literal).hasDesc);
		}
		const mvar = (hasDesc as HasDesc.Mvar);
		const result = new HasDesc.Mvar(mvar.id + 1);
		result.instance = mvar.instance;
		return result;
	}

	private freshLabel = (label: TreeLabel.Base): TreeLabel.Base => {
		if (label instanceof TreeLabel.Literal) {
			return new TreeLabel.Literal((label as TreeLabel.Literal).value);
		}
		const mvar = label as TreeLabel.Mvar;
		const result = new TreeLabel.Mvar(mvar.id + 1);
		result.instance = mvar.instance;
		return result;
	}

	testCall_unifyNodeKind = this.unifyNodeKind;
	testCall_unifyHasDesc  = this.unifyHasDesc;
	testCall_getNextRule   = this._getNextRule;
}

export class UnificationError extends Error {
	readonly msg: string;
	constructor(msg: string) {
		super(msg);
		this.msg = msg;
		Object.setPrototypeOf(this, UnificationError.prototype);
	}
	get = (): string => this.msg;
}
