import {Automaton} from './Automaton';

interface InfoProps {
	automaton: Automaton
}

export const AutomatonInfoView = (props: InfoProps) => {
	return (
		<div>
			<div>Automaton info</div>
			<br/>
			<div>
				{props.automaton.toString()}
			</div>
		</div>
	);
}
