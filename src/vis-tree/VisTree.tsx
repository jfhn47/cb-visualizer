import {ChangeEvent, useState} from 'react';
import './VisTree.css';
import {HasDesc, InputModel, MappingLeft, MappingRight, Mappings, NodeKind, State} from "./InputModel";
import {exists, stableSort} from "../util";
import MultiSelect from 'multiselect-react-dropdown';
import './bootstrap-multiselect.css';
import 'bootstrap'
import 'react-bootstrap'
import {Automaton} from './Automaton';
import {AutomatonInfoView} from './AutomatonInfoView';
import {GammaTreeView} from './GammaTreeView';
import {parseInputGrammar, errorToString} from './InputParser';
import {ParseError} from 'parzec';
import Swal from "sweetalert2";
import withReactContent from 'sweetalert2-react-content';
import Popup from 'reactjs-popup';

const SweetAlert = withReactContent(Swal);

type MultiSelectOptionType = {
	name : string,
	id   : number
};

export type TableRowType    = [MappingLeft | null, MappingRight | null];
export type InputTable      = TableRowType[];
export type LeftColumnType  = MappingLeft    | string;
export type RightColumnType = MappingRight   | string;
export type ColumnType      = LeftColumnType | RightColumnType;

const Colors = {
	normal: "#FFFFFF",
	last: "#FFFFBB",
	next: "#BBFFBB",
} as const;

type NewTableRowProps = {
	left   : MappingLeft,
	right  : MappingRight,
	isLast : boolean,
	isNext : boolean,
};

const NewTableRow = (props: NewTableRowProps) => {
	return <tr style={{
		backgroundColor: props.isNext
			? Colors.next
			: props.isLast
				? Colors.last
				: Colors.normal
	}}>
		<td><input
			type="text"
			value={props.left.toString()}
			readOnly={true}
			style={{
				width: "98%", border: "0px",
				fontStyle: "italic", fontSize: "14pt",
				backgroundColor: props.isNext
					? Colors.next
					: props.isLast
						? Colors.last
						: Colors.normal
			}}
		/></td>
		<td><input
			type="text"
			value={props.right.toString()}
			readOnly={true}
			style={{
				width: "98%", border: "0px",
				fontStyle: "italic", fontSize: "14pt",
				backgroundColor: props.isNext
					? Colors.next
					: props.isLast
						? Colors.last
						: Colors.normal
			}}
		/></td>
	</tr>;
}

/* Simple example
(qa, a, (*, -, *), *) -> (qa, push(A, l))
(qa, b, (*, -, -), *) -> (qb, pop)
(qb, b, (*, -, -), *) -> (qb, pop)
(qb, $, (-, *, *), *) -> (accept, pop)
 */

/* Complex example
(q0, a,      (-,-,-), -) -> (q1, push(o, l))
(q1, a,      (l,-,-), o) -> (q1, push(o, l))
(q1, b,      (l,*,-), o) -> (p1, push(x, r))
(p1, lambda, (r,-,-), x) -> (p2, u)
(p2, lambda, (l,*,+), o) -> (p3, u)
(p3, b,      (l,*,-), o) -> (p4, s)
(p4, b,      (l,*,-), o) -> (p3, u)
(p3, lambda, (-,+,-), -) -> (s1, l)
(s1, lambda, (l,+,-), o) -> (s1, l)
(s1, lambda, (l,*,+), o) -> (q1, u)
(q1, $,      (-,*,-), -) -> (q+, s)
(q1, b,      (-,+,-), -) -> (r1, l)
(r1, lambda, (l,*,+), o) -> (r2, r)
(r2, lambda, (r,-,-), x) -> (r1, pop)
(r1, lambda, (l,+,-), o) -> (r1, l)
(r1, lambda, (l,-,-), o) -> (-p1, push(x, r))
 */

interface VisTreeProps {
	onBackToToolSelection: () => void;
}

export type MarkedRules = {last: number, next: number};

const VisTree = (props: VisTreeProps) => {
	const [states,             setStates]             = useState<Set<State>>(new Set());
	const [startState,         setStartState]         = useState<State | null>();
	const [endStates,          setEndStates]          = useState<MultiSelectOptionType[]>([]);
	const [input,              setInput]              = useState<string>("");
	const [automaton,          setAutomaton]          = useState<Automaton | null>(null);
	const [markedRules,        setMarkedRules]        = useState<MarkedRules>({last: -1, next: -1});
	const [grammarInput,       setGrammarInput]       = useState<string>("");
	const [parsedSuccessfully, setParsedSuccessfully] = useState<boolean>(false);
	const [mappings,           setMappings]           = useState<Mappings | null>(null);

	const [showHelp,           setShowHelp]           = useState<boolean>(false);

	const onSelectStartState = (e: ChangeEvent<HTMLSelectElement>) => {
		setStartState(e.target.value);
	}

	const sortMappings = (): Mappings => {
		if (!exists(mappings)) {
			return [];
		}

		return stableSort(mappings!, ([l1], [l2]) => {
			let l1Score = 0, l2Score = 0;

			if (l1.typ.kind     instanceof NodeKind.Mvar) l1Score++;
			if (l1.typ.hasLeft  instanceof HasDesc.Mvar)  l1Score++;
			if (l1.typ.hasRight instanceof HasDesc.Mvar)  l1Score++;

			if (l2.typ.kind     instanceof NodeKind.Mvar) l2Score++;
			if (l2.typ.hasLeft  instanceof HasDesc.Mvar)  l2Score++;
			if (l2.typ.hasRight instanceof HasDesc.Mvar)  l2Score++;

			return l1Score <= l2Score;
		});
	}

	const getStates = (mappings: Mappings): Set<State> => {
		const result = new Set<State>();
		for (const mapping of mappings) {
			result.add(mapping[0].state); // LHS
			result.add(mapping[1].state); // RHS
		}
		if (!exists(startState)) {
			setStartState(Array.from(result)[0]);
		}
		return result;
	}

	const drawRuleInputField = () => {
		return <div>
			<textarea
				style={{
					height: "100%",
					width: "100%",
					fontSize: "16pt",
					fontStyle: "italic",
				}}
				rows={15}
				onChange={(e) => setGrammarInput(e.target.value!)}
				value={grammarInput}
				placeholder="Enter input here"
			/>
			<button
			onClick={() => {
				try {
					const mappings = parseInputGrammar(grammarInput);
					setMappings(mappings);
					setStates(getStates(mappings));
					setParsedSuccessfully(true);
				} catch (e: unknown) {
					if (e instanceof ParseError) {
						console.log(e);
						const parserError = e as ParseError;
						const errorMessage = errorToString(parserError, grammarInput);
						console.log(errorMessage);
						const swalText = errorMessage.split("\n").map(line => `<p>${line}<p>`).join("");
						SweetAlert.fire({
							width: "80%",
							title: "Parser error",
							html: `<div style="text-align: left">` + swalText + "</div>",
							icon: 'error',
						});
					}
					setParsedSuccessfully(false);
				}
			}}
		>Parse</button></div>
	}

	const drawRuleTable = () => {
		return <table style={{width: "100%"}}>
			<thead style={{
				fontFamily: "",
				fontStyle: "italic", fontSize: "14pt",
			}}>
				<tr>
					<th>Input states</th>
					<th>Output states</th>
				</tr>
			</thead>
			<tbody>
				{mappings!.map(([lhs, rhs, i]) => <NewTableRow
					key={i}
					isLast={markedRules.last === i}
					isNext={markedRules.next === i}
					left={lhs}
					right={rhs}
				/>)}
			</tbody>
		</table>
	}

	const canSubmit = (): boolean => {
		return input.length > 0
		    && exists(startState)
		    && endStates.length > 0
		    && parsedSuccessfully
		    && !exists(automaton);
	}

	const addNextAndLastRowHighlight = (rows: MarkedRules) => {
		// console.log(`Next match is in row ${row}`);
		setMarkedRules(rows);
	}

	const drawAutomatonInfo = () => {
		return (
			<AutomatonInfoView automaton={new Automaton(
				startState!, endStates as any as string[],
				new InputModel(input.split(" "), sortMappings()),
				addNextAndLastRowHighlight, true
			)} />
		);
	}

	const drawAutomaton = () => {
		return <div>
			<div>
				<button
					onClick={() => {
						setAutomaton(null);
						setInput("");
						console.log("Reset");
					}}
				>Reset</button>
			</div>
			<GammaTreeView automaton={automaton!}/>
		</div>;
	}

	return <div style={{overflowX: "hidden", overflowY: "hidden"}}>
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/dreampulse/computer-modern-web-font@master/fonts.css" />
		<link rel="stylesheet" href="../bootstrap-multiselect.css" type="text/css" />
		<div id="all_content" style={{marginLeft: '3%', marginRight: '3%'}}>
			<h1 className="header_light"><i>&Gamma;-Tree Parsing visualized</i>
				<button
					className="help_button"
					style={{marginLeft: "1%"}}
					disabled={showHelp}
					onClick={() => setShowHelp(true)}
				>?</button>
				<button
					style={{marginLeft: "1%"}}
					className="feedback_button"
					onClick={() => {
						props.onBackToToolSelection();
						window.location.reload();
					}} // TODO: There should be a better solution.
				>
					Back to tool selection
				</button>
			</h1>
			<Popup
				open={showHelp}
				closeOnDocumentClick
				closeOnEscape
				onClose={() => setShowHelp(false)}
				modal
				position="top center"
				contentStyle={{ background: "#FCFAED", borderRadius: "5px", border: "2px solid black" }}
			>
				{(close: () => void) => (
					<div className="modal">
						<div className="header">Help</div>
						<div className="content">
							Rough Syntax:<br/>
							Rule ::= LHS -&#62; RHS<br/>
							LHS ::= (&lt;state&gt;, &lt;input&gt; (&lt;node-kind&gt;, &lt;has-left-descendant&gt;, &lt;has-right-descendant&gt;), &lt;tree-label&gt;)<br/>
							RHS ::= (&lt;state&gt;, &lt;Action&gt;)<br/>
							Action ::= &lt;move&gt; | pop | push(&lt;tree-label&gt;, &lt;left-or-right&gt;)<br/><br/>
							Important:<br/>
							&#8901; Root tree label is <i>not</i> "&#8869;" but "-" because it is easier to write.<br/>
							&#8901; The "*" character is a wildcard that matches every input.<br/><br/>

							Examples: (copy and paste)<br/><br/>
							Simple:<br/>
							(qa, a, (*, -, *), *) -&#62; (qa, push(A, l))<br/>
							(qa, b, (*, -, -), *) -&#62; (qb, pop)<br/>
							(qb, b, (*, -, -), *) -&#62; (qb, pop)<br/>
							(qb, $, (-, *, *), *) -&#62; (accept, pop)<br/><br/>

							Complex:<br/>
							(q0, a,      (-,-,-), -) -&#62; (q1, push(o, l))<br/>
							(q1, a,      (l,-,-), o) -&#62; (q1, push(o, l))<br/>
							(q1, b,      (l,*,-), o) -&#62; (p1, push(x, r))<br/>
							(p1, lambda, (r,-,-), x) -&#62; (p2, u)<br/>
							(p2, lambda, (l,*,+), o) -&#62; (p3, u)<br/>
							(p3, b,      (l,*,-), o) -&#62; (p4, s)<br/>
							(p4, b,      (l,*,-), o) -&#62; (p3, u)<br/>
							(p3, lambda, (-,+,-), -) -&#62; (s1, l)<br/>
							(s1, lambda, (l,+,-), o) -&#62; (s1, l)<br/>
							(s1, lambda, (l,*,+), o) -&#62; (q1, u)<br/>
							(q1, $,      (-,*,-), -) -&#62; (q+, s)<br/>
							(q1, b,      (-,+,-), -) -&#62; (r1, l)<br/>
							(r1, lambda, (l,*,+), o) -&#62; (r2, r)<br/>
							(r2, lambda, (r,-,-), x) -&#62; (r1, pop)<br/>
							(r1, lambda, (l,+,-), o) -&#62; (r1, l)<br/>
							(r1, lambda, (l,-,-), o) -&#62; (-p1, push(x, r))<br/>
						</div>
						<div>
								<button className="button" onClick={() => close()}>Close help</button>
						</div>
					</div>
				)}
			</Popup>
		</div>

		<div style={{width: "100%", paddingLeft: "1%", paddingRight: "1%"}}>
			<div style={{float: "left", width: "50%", height: "100%", paddingRight: "1%"}}>
				{/* Input field */}
				<div style={{height: "40%", width: "98%"}}>
					{
						!exists(automaton)
							? drawRuleInputField()
							: drawRuleTable()
					}
				</div>

				{/* Dropdown select for start state. */}
				Start state:
				<select
					onChange={onSelectStartState}
					disabled={exists(automaton)}
					style={{
						marginLeft: "2%", marginTop: "3%", marginBottom: "2%",
						border: "none",
						borderBottom: "1px solid black"
					}}
				>
					{states.size === 0 ? <option>Undefined</option> :
					Array.from(states).map((state, i) => <option key={i}>{state.toString()}</option>)}
				</select>
				<br/>

				{/* Dropdown checkbox select for end states. */}
				<MultiSelect
					isObject={false}
					options={Array.from(states)}
					selectedValues={endStates}
					onSelect={setEndStates}
					onRemove={setEndStates}
					// hideSelectedList
					showCheckbox
					displayValue={"Select end states"}
					placeholder={"Select end states..."}
					emptyRecordMsg={"No states defined"}
					disable={exists(automaton)}
					style={{
						searchBox: {
							border: "none",
							borderRadius: "0px",
							borderBottom: "1px solid black"
						},
						multiselectContainer: {
							width: "49.2%",
							fontSize: "16px",
							display: "flex",
						},
						optionContainer: {
							backgroundColor: "#80ba24"
						},
						option: {
							backgroundColor: "#80ba24"
						},
					}}
				/>

				<br/>
				<br/>

				{/* Input for the automaton. */}
				<div>
					<div style={{float: "left"}}>Input:</div>
					<input
						onChange = {(e: ChangeEvent<HTMLInputElement>) => setInput(e.target.value)}
						value={input}
						disabled={exists(automaton)}
						style={{
							marginLeft: "2%",
							paddingTop: "1%",
							float: "left",
							border: "none",
							borderBottom: "1px solid black"
						}}
					/>
				</div>

				<br/>
				<br/>

				{/* Submit button. */}
				<div>
					<button
						onClick={() => {
							// The `endStates` are somehow not of type MultiSelectOptionType[] but of type string[].
							setAutomaton(new Automaton(startState!, endStates as any as string[], new InputModel(input.split(" "), mappings!), addNextAndLastRowHighlight));
						}}
						disabled={!canSubmit()}
					>Start</button>
				</div>
			</div>

			{/* Info and automaton views */}
			<div style={{float: "left", width: "49%", height: "100%", whiteSpace: "pre-line"}}>
			{
				!exists(automaton)
					? drawAutomatonInfo()
					: drawAutomaton()
			}
			</div>
		</div>
	</div>
}

export default VisTree;
