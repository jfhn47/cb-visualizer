import {NodeKind, Alphabet, Symbol, Direction} from "./InputModel";
import {exists, mkString, noop} from "../util";

export enum TreeDirection {
	LEFT,
	RIGHT
}

export namespace TreeDirection {
	export const of = (dir: Direction.Kind) => dir === Direction.Kind.LEFT ? TreeDirection.LEFT : TreeDirection.RIGHT;

	export const toString = (dir: TreeDirection) => dir === TreeDirection.LEFT ? "l" : "r"
}

export class GammaTreeNode {
	parent    : GammaTreeNode | null;
	symbol    : Symbol;
	kind      : NodeKind.Kind;
	leftDesc  : GammaTreeNode | null = null;
	rightDesc : GammaTreeNode | null = null;

	constructor(symbol: Symbol, kind: NodeKind.Kind, parent: GammaTreeNode | null = null) {
		this.symbol = symbol;
		this.kind = kind;
		this.parent = parent;
	}

	clone = (parent: GammaTreeNode | null = null): GammaTreeNode => {
		const result = new GammaTreeNode(this.symbol, this.kind, parent);
		if (this.leftDesc) {
			result.leftDesc = this.leftDesc.clone(this);
		}
		if (this.rightDesc) {
			result.rightDesc = this.rightDesc.clone(this);
		}
		return result;
	}

	toString = () => `${this.symbol}\n${this.symbol}.left = ${this.leftDesc?.toString() ?? "null"}\n${this.symbol}.right = ${this.rightDesc?.toString() ?? "null"}`;
}

export class TreeInstructionError extends Error {
	private readonly msg: string;

	constructor(msg: string) {
		super(msg);
		this.msg = msg;

		Object.setPrototypeOf(this, TreeInstructionError.prototype);
	}

	get = (): string => this.msg;
}

export class GammaTree {
	readonly treePath: TreeDirection[];
	private readonly alphabet: Alphabet;
	readonly root: GammaTreeNode;
	depth: number;

	private constructor(alphabet: Alphabet, internalTree: GammaTreeNode = new GammaTreeNode("-", NodeKind.Kind.ROOT), path: TreeDirection[] = [], depth: number = 0) {
		this.alphabet = alphabet;
		this.root = internalTree;
		this.treePath = path;
		this.depth = depth;
		noop(this.alphabet);
	}

	static of = (alphabet: Alphabet): GammaTree => new GammaTree(alphabet);

	walkTree = (): GammaTreeNode => {
		return this.walkTree_(0, this.root);
	}

	private walkTree_ = (idx: number, node: GammaTreeNode | null): GammaTreeNode => {
		// console.log(`Path = ${mkString(this.treePath, _ => TreeDirection.toString(_), 0).substring(idx)}`);
		const dir = this.treePath[idx];
		if (idx >= this.treePath.length) {
			console.assert(node !== null, "Node is null");
			return node!;
		}
		if (dir === TreeDirection.LEFT) {
			return this.walkTree_(idx + 1, node!.leftDesc!);
		}
		return this.walkTree_(idx + 1, node!.rightDesc!);
	}

	moveUp = () => {
		this.treePath.pop();
	}

	moveLeft = () => {
		const node = this.walkTree();
		if (!exists(node.leftDesc)) {
			throw new TreeInstructionError("Cannot move left because the current node has no left descendant.");
		}
		this.treePath.push(TreeDirection.LEFT);
	}

	moveRight = () => {
		const node = this.walkTree();
		if (!exists(node.rightDesc)) {
			throw new TreeInstructionError("Cannot move right because the current node has no right descendant.");
		}
		this.treePath.push(TreeDirection.RIGHT);
	}

	pushNode = (dir: TreeDirection, symbol: Symbol) => {
		const parent = this.walkTree();

		if (dir === TreeDirection.LEFT) {
			if (exists(parent.leftDesc)) {
				throw new TreeInstructionError("Cannot push left because the current node already has a left descendant.");
			}
			parent.leftDesc = new GammaTreeNode(symbol, NodeKind.Kind.LEFT_DESC, parent);
		} else {
			if (exists(parent.rightDesc)) {
				throw new TreeInstructionError("Cannot push right because the current node already has a right descendant.");
			}
			parent.rightDesc = new GammaTreeNode(symbol, NodeKind.Kind.RIGHT_DESC, parent);
		}

		if (this.treePath.length >= this.depth) {
			++this.depth;
		}

		this.treePath.push(dir);
	}

	popNode = () => {
		const poppedChild = this.walkTree();
		if (exists(poppedChild.leftDesc) || exists(poppedChild.rightDesc)) {
			throw new TreeInstructionError("Cannot pop because the current node has descendants.");
		}

		this.treePath.pop();
		const parent = this.walkTree();
		if (poppedChild.kind === NodeKind.Kind.LEFT_DESC) {
			parent.leftDesc = null;
		} else if (poppedChild.kind === NodeKind.Kind.RIGHT_DESC) {
			parent.rightDesc = null;
		}
	}

	clone = (): GammaTree => new GammaTree(this.alphabet, this.root.clone(), [...this.treePath], this.depth);

	toString = () => this.root.toString() + `\nP = ${mkString(this.treePath, dir => TreeDirection.toString(dir) + "", 0)}`;
}
