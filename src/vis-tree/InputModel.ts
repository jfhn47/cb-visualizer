import {Comparable, ComparableSet, mkString, strEqualsFunc, StringBuilder} from '../util';

// Mapping   : State x Input x TYPE x Label -> State x Action.
// TYPE      : is root/left/right descendant, has left/right descendant.
// Action    : Direction/pop/push.
// Direction : up/stay/left/right.

export type Symbol        = string;
export type State         = string;
export type Label         = string;
export type MappingTuple  = [MappingLeft, MappingRight, number];
export type Mappings      = MappingTuple[];

export namespace NodeKind {
	// Type safe enumerations.
	export const Kind = {
		ROOT: "-",
		LEFT_DESC: "l",
		RIGHT_DESC: "r"
	} as const;
	export type Kind = (typeof Kind)[keyof typeof Kind];

	export interface Base extends Comparable<Base> {
		toString: () => string;
	}

	export class Literal implements NodeKind.Base {
		readonly kind: Kind;

		constructor(kind: Kind) {
			this.kind = kind;
		}

		toString = () => this.kind;

		equals = (other: Base): boolean => {
			if (other instanceof Mvar) {
				return false;
			}
			return this.kind === (other as Literal).kind;
		}
	}

	export class Mvar implements NodeKind.Base {
		readonly id: number;
		instance: NodeKind.Base | null = null;

		constructor(id: number = 0) {
			this.id = id;
		}

		toString = () => "*";

		equals = (other: Base): boolean => {
			if (other instanceof Literal) {
				return false;
			}
			return this.id === (other as Mvar).id;
		}
	}

	export const of = (input: string): Base => {
		return input === "*" ? new Mvar(0) : new Literal(input as Kind);
	}
}

export namespace HasDesc {
	export interface Base extends Comparable<Base> {
		toString: () => string;
	}

	export class Literal implements Base {
		readonly hasDesc: boolean

		constructor(hasDesc: boolean) {
			this.hasDesc = hasDesc;
		}

		toString = () => this.hasDesc ? "+" : "-";

		equals = (other: Base): boolean => {
			if (other instanceof Mvar) {
				return false;
			}
			return this.hasDesc === (other as Literal).hasDesc;
		}
	}

	export class Mvar implements Base {
		readonly id: number;
		instance: HasDesc.Base | null = null;

		constructor(id: number) {
			this.id = id;
		}

		toString = () => "*";

		equals = (other: Base): boolean => {
			if (other instanceof Literal) {
				return false;
			}
			return this.id === (other as Mvar).id;
		}
	}

	export const of = (input: string): Base => {
		return input === "*" ? new Mvar(0) : new Literal(input === "+");
	}
}

export class NodeType implements Comparable<NodeType> {
	readonly kind     : NodeKind.Base;
	readonly hasLeft  : HasDesc.Base;
	readonly hasRight : HasDesc.Base;

	constructor(kind: NodeKind.Base, hasLeft: HasDesc.Base, hasRight: HasDesc.Base) {
		this.kind     = kind;
		this.hasLeft  = hasLeft;
		this.hasRight = hasRight;
	}

	equals = (other: NodeType) => {
		return this.kind.equals(other.kind)
		    && this.hasLeft.equals(other.hasLeft)
		    && this.hasRight.equals(other.hasRight);
	}

	toString = (): string => `(${this.kind.toString()}, ${this.hasLeft.toString()}, ${this.hasRight.toString()})`;
}

export namespace TreeLabel {
	export interface Base extends Comparable<Base> {
		toString: () => string;
	}

	export class Literal implements Base {
		readonly value: string;

		constructor(value: string) {
			this.value = value;
		}

		toString = () => this.value;

		equals = (other: Base): boolean => {
			if (other instanceof Mvar) {
				return false;
			}
			return this.value === (other as Literal).value;
		}
	}

	export class Mvar implements Base {
		readonly id: number;
		instance: HasDesc.Base | null = null;

		constructor(id: number) {
			this.id = id;
		}

		toString = () => "*";

		equals = (other: Base): boolean => {
			if (other instanceof Literal) {
				return false;
			}
			return this.id === (other as Mvar).id;
		}
	}

	export const of = (input: string): Base => {
		return input === "*" ? new Mvar(0) : new Literal(input);
	}
}

export class Direction {
	readonly dir: Direction.Kind;

	constructor(dir: Direction.Kind) {
		this.dir = dir;
	}

	toString = () => this.dir;
}

export namespace Direction {
	// Type safe enumerations.
	export const Kind = {
		STAY: "s",
		UP: "u",
		LEFT: "l",
		RIGHT: "r"
	} as const;
	export type Kind = (typeof Kind)[keyof typeof Kind];

	export const of = (input: string): Direction => new Direction(input as Kind);
}

export interface Instruction {
	toString: () => string;
}

export const popInstruction: Instruction = {
	toString: () => "pop"
};

export class PushInstruction implements Instruction {
	readonly label: Label;
	readonly direction: Direction;

	constructor(label: Label, direction: Direction) {
		this.label     = label;
		this.direction = direction;
	}

	toString = (): string => `push(${this.label}, ${this.direction.toString()})`;
}

export type Action = Direction | Instruction;

export class MappingLeft implements Comparable<MappingLeft> {
	readonly state : State;
	readonly input : Symbol;
	readonly typ   : NodeType;
	readonly label : TreeLabel.Base;

	constructor(state: State, input: Symbol, typ: NodeType, label: TreeLabel.Base) {
		this.state = state;
		this.input = input;
		this.typ   = typ;
		this.label = label;
	}

	equals = (other: MappingLeft): boolean => {
		return this.state === other.state
		    && this.input === other.input
		    && this.typ.equals(other.typ)
		    && this.label.equals(other.label);
	}

	toString = (): string => `(${this.state}, ${this.input}, ${this.typ.toString()}, ${this.label})`;
}

export class MappingRight {
	readonly state  : State;
	readonly action : Action;

	constructor(state: State, action: Action) {
		this.state  = state;
		this.action = action;
	}

	toString = () => `(${this.state}, ${this.action.toString()})`;
}

export type Mapping = (MappingLeft | MappingRight) & { state: State };

export class Alphabet extends ComparableSet<Symbol> {
	constructor(data?: Iterable<Symbol>) {
		super(strEqualsFunc, data);
	}
}

export class InputModel {
	readonly        inputAlphabet : Alphabet;
	readonly        treeAlphabet  : Alphabet;
	readonly        input         : Symbol[];
	static readonly emptyWord     : Symbol   = "lambda"; // TODO: Make editable.
	static readonly endMarker     : Symbol   = "$"       // TODO: Make editable.
	readonly        mappings      : Mappings;

	constructor(input: Symbol[], mappings: Mappings) {
		this.input = input;
		this.inputAlphabet = InputModel.findInputAlphabet(mappings);
		this.treeAlphabet = InputModel.findTreeAlphabet(mappings);
		this.mappings = mappings;
		this.input.push(InputModel.endMarker);
	}

	private static findInputAlphabet = (mappings: Mappings): Alphabet => {
		const result = new Alphabet();
		for (const [lhs] of mappings) {
			result.add(lhs.input);
		}
		return result;
	}

	private static findTreeAlphabet = (mappings: Mappings): Alphabet => {
		const result = new Alphabet();
		for (const [lhs, rhs] of mappings) {
			if (lhs.label instanceof TreeLabel.Literal) {
				result.add((lhs.label as TreeLabel.Literal).value);
			}
			if (rhs.action instanceof PushInstruction) {
				result.add(rhs.action.label);
			}
		}
		return result;
	}

	toString = (): string => {
		const sb = new StringBuilder("Input alphabet  = { ");
		sb.append(mkString(this.inputAlphabet) + " }\n");
		sb.append("Tree alphabet = { ")
		sb.append(mkString(this.treeAlphabet) + " }\n");
		sb.append(`emptyWord = ${InputModel.emptyWord}\n`);
		sb.append(`endMarker = ${InputModel.endMarker}\n`);
		sb.append("Sorted rules:\n");
		for (const [lhs, rhs] of this.mappings) {
			sb.append(`${lhs.toString()} -> ${rhs.toString()}\n`);
			// sb.append(`${lhs.toString()} \u2192 ${rhs.toString()}\n`);
		}
		return sb.toString();
	}
}
