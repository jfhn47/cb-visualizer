/*
 * Grammar definition in EBNF:
 *
 * input       ::= { rule } ;
 * rule        ::= lhs "->" rhs ;
 * lhs         ::= "(" state "," word "," "(" node-kind "," has-desc "," has-desc "," ")" "," tree-label ")" ;
 * state       ::= (regex: [^,]+) ;
 * word        ::= state ;
 * tree-label  ::= state ;
 * node-kind   ::= meta | "-" | "l" | "r" ;
 * has-desc    ::= meta | "+" | "-" ;
 * rhs         ::= "(" state "," instruction ")" ;
 * instruction ::= move | pop | push ;
 * move        ::= "s" | "u" | "l" | "r" ;
 * pop         ::= "pop" ;
 * push        ::+ "push" "(" tree-label "," ("l" | "r") ")" ;
 * meta        ::= "*" ;
 */

import * as pz from 'parzec';
import {ParseError, Token} from 'parzec';
import {Direction, HasDesc, MappingLeft, MappingRight, MappingTuple, NodeKind, NodeType, popInstruction, PushInstruction, TreeLabel} from './InputModel';

enum GToken {
	// Pure syntax symbols.
	LeftParen, RightParen, Arrow, Comma,

	// Tokens with meaning.
	Text, LeftRight, Minus, Plus, HasDesc, ExtraMove, Push, Pop, Meta,

	// Ignored tokens.
	Whitespace, Comment, EOF,
}

// Note:
// We need to overwrite the original lexer to make it useful.
// The problem with the lexer is that it returns the first token that matches.
// This leeds to problems when we want to match words like "lambda" but have
// a token that matches with "[lr]". Since words are defined as anything but
// syntax symbols to provide maximum flexibility ("[^,() ]+"), the word token
// be checked at the end. This makes getting the tokens "lambda","rx" and so
// on impossible.
// This custom lexer extends the original lexer and overrides the match token
// function. We now save all matching tokens and return the first longest
// matching one. It uses some hacks to bypass the private modifier but this
// is the most simple solution apart from a real fork.

type TokenMatcher<S> = {
	regex: RegExp,
	token: S,
};

type LexMatch = {token: GToken, match: RegExpExecArray};

class CustomLexer extends pz.Lexer<GToken> {
	override matchToken(input: string, pos: number): Token<GToken> | null {
		let matches: LexMatch[] = [];

		for (let i = 0; i < (this as any).matchers.length; i++) {
			let matcher = (this as any).matchers[i] as TokenMatcher<GToken>;
			matcher.regex.lastIndex = pos;
			let match = matcher.regex.exec(input);
			if (match != null) {
				matches.push({token: matcher.token, match});
			}
		}

		if (matches.length === 0) {
			return null;
		}

		let longestMatch = 0;
		let resultMatchIndex = 0;
		for (let i = 0; i < matches.length; i++) {
			let match = matches[i];
			if (match.match[0].length > longestMatch) {
				longestMatch = match.match[0].length;
				resultMatchIndex = i;
			}
		}

		return new Token(matches[resultMatchIndex].token, matches[resultMatchIndex].match[0]);
	}
}

const lexer = new CustomLexer(
	[/\(/           , GToken.LeftParen],
	[/\)/           , GToken.RightParen],
	[/->/           , GToken.Arrow],
	[/,/            , GToken.Comma],
	[/[lr]/         , GToken.LeftRight],
	[/-/            , GToken.Minus],
	[/\+/           , GToken.Plus],
	[/[su]/         , GToken.ExtraMove],
	[/pop/          , GToken.Pop],
	[/push/         , GToken.Push],
	[/\*/           , GToken.Meta],
	[/[^,() ]+/     , GToken.Text],
	[/[\t\n\r ]+/   , GToken.Whitespace],
	[/\/\/[^\n\r]*/ , GToken.Comment],
);

/* eslint-disable no-whitespace-before-property */
const parseOptws      = pz.terminal(GToken.Whitespace, "whitespace") .optionalRef();
// const parseComment    = pz.terminal(GToken.Comment,    "comment")    .optionalRef();

const parseLeftParen  = pz.terminal(GToken.LeftParen,  "(")          .followedBy(parseOptws);
const parseRightParen = pz.terminal(GToken.RightParen, ")")          .followedBy(parseOptws);
const parseArrow      = pz.terminal(GToken.Arrow,      "->")         .followedBy(parseOptws);
const parseComma      = pz.terminal(GToken.Comma,      ",")          .followedBy(parseOptws);

const parseLeftRight  = pz.terminal(GToken.LeftRight,  "[lr]")       .followedBy(parseOptws);
const parseMinus      = pz.terminal(GToken.Minus,      "-")          .followedBy(parseOptws);
const parsePlus       = pz.terminal(GToken.Plus,       "+")          .followedBy(parseOptws);
const parseExtraMove  = pz.terminal(GToken.ExtraMove,  "[su]")       .followedBy(parseOptws);
const parsePop        = pz.terminal(GToken.Pop,        "pop")        .followedBy(parseOptws);
const parsePush       = pz.terminal(GToken.Push,       "push")       .followedBy(parseOptws);
const parseMeta       = pz.terminal(GToken.Meta,       "*")          .followedBy(parseOptws);

const parseText       = pz.terminal(GToken.Text,       "text")       .followedBy(parseOptws);

// export const parseEOF = pz.terminal(GToken.EOF,        "<eof>");

const parseState = parseText;
const parseInput = parseText;
const parseLabel = parseText.or(parseMinus).or(parseMeta);

const parseNodeKind = parseLeftRight.or(parseMinus).or(parseMeta);
const parseHasDesc  = parsePlus.or(parseMinus).or(parseMeta);

const parseNodeType = parseLeftParen.seq(parseNodeKind).followedBy(parseComma)
	.bind(nodeKind => parseHasDesc.followedBy(parseComma)
	.bind(hasLeft => parseHasDesc.followedBy(parseRightParen)
	.map(hasRight => new NodeType(NodeKind.of(nodeKind.text), HasDesc.of(hasLeft.text), HasDesc.of(hasRight.text)))));

const parseLHS = parseLeftParen.seq(parseState).followedBy(parseComma)
	.bind(state => parseInput.followedBy(parseComma)
	.bind(input => parseNodeType.followedBy(parseComma)
	.bind(nodeKind => parseLabel
	.map(label => new MappingLeft(state.text, input.text, nodeKind, TreeLabel.of(label.text)))))).followedBy(parseRightParen);

const parseMove = parseLeftRight.or(parseExtraMove);

const parsePushInstruction = parsePush.seq(parseLeftParen).seq(parseLabel).followedBy(parseComma)
	.bind(label => parseLeftRight.followedBy(parseRightParen)
	.map(dir => new PushInstruction(label.text, Direction.of(dir.text))));

const parseInstruction = parsePop.map(_ => popInstruction).or(parsePushInstruction);

const parseAction = parseMove.map(dir => Direction.of(dir.text)).or(parseInstruction);

const parseRHS = parseLeftParen.seq(parseState.or(parseMeta)).followedBy(parseComma)
	.bind(state => parseAction.followedBy(parseRightParen)
	.map(action => new MappingRight(state.text, action)));

let ruleNumber = 0;
const parseRule = parseOptws.seq(parseLHS)
	.bind(lhs => parseArrow.seq(parseRHS)
	.map(rhs => {
		let num = ruleNumber++;
		return [lhs, rhs, num] as MappingTuple;
	}));

const parseAll = parseRule.oneOrMore();

const lex = (text: string): pz.ParserInput<pz.Token<GToken>> => pz.lexerInput<GToken>(text, lexer, new pz.Token(GToken.EOF, "<eof>"));

export const parseInputGrammar = (input: string): MappingTuple[] => {
	ruleNumber = 0;
	return pz.parse(parseAll, lex(input.replace(/\n/g, " "))); // The parser cannot handle real newlines -> replace them.
}

const getErrorLocation = (error: ParseError, input: string): [number, number] => {
	let line = 1;
	let column = 1;
	for (let i = 0; i < error.position; i++) {
		if (input[i] === "\n") {
			line++;
			column = 1;
		} else {
			column++;
		}
	}
	return [line, column];
}

const getRelevantErrorInput = (line: number, column: number, input: string): string => {
	const lines = input.split("\n");
	const errorPointerString = "-".repeat(column + line.toString().length + 1) + "^";
	if (line === 1) {
		return [`1. ${lines[0]}`, errorPointerString, `2. ${lines[1]}`].join("\n");
	} else if (line === lines.length) {
		return [`${line - 1}. ${lines[line - 2]}`, errorPointerString, `${line}. ${lines[line - 1]}`].join("\n");
	} else {
		return [`${line - 1}. ${lines[line - 2]}`, `${line}. ${lines[line - 1]}`, errorPointerString, `${line + 1}. ${lines[line]}`].join("\n");
	}
}

export const errorToString = (error: ParseError, input: string): string => {
	const [line, column] = getErrorLocation(error, input);
	return `Parsing error in line ${line}, column ${column}:\n` +
		`Expected: ${error.expected.map(e => e.toString()).join(" or ")}\n` +
		`Actual: "${error.found}"\n` +
		`Relevant Input:\n${getRelevantErrorInput(line, column, input)}`;
}
