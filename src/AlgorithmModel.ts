import {
	assert,
	ComparableSet,
	exists,
	filter2,
	foldLeft,
	map,
	max,
	mkString,
	Nothing,
	strEqualsFunc,
	StringBuilder,
	sum,
	UniqueQueue
} from "./util";
import {Grammar} from "./Grammar"
import {LR0Element, lr0EqualsFunc, LR1Element, lr1EqualsFunc} from "./LR0Element";
import {Reduction, ReductionLR1} from "./Reduction";

export enum ParserAlgorithm {
	SLR,
	LR1
}

/**
 * Maps the 'from state' and the symbol to the 'to state'.
 */
export type Jumps = Map<[number, string], number>;

/**
 * A wrapper class for a collection entry that is a comparable set
 * with the `lr0EqualsFunc` function to test equality.
 */
export class LR0CollectionEntry extends ComparableSet<LR0Element> {
	constructor(data?: Iterable<LR0Element>) {
		super(lr0EqualsFunc, data);
	}
}

export class LR1CollectionEntry extends ComparableSet<LR1Element> {
	constructor(data?: Iterable<LR1Element>) {
		super(lr1EqualsFunc, data);
	}
}

/**
 * A list of collection entries.
 */
export type CollectionSLR = LR0CollectionEntry[];

export type CollectionLR1 = LR1CollectionEntry[];

/**
 * This class encapsulates all the data necessary for the computation of algorithms.
 * It also contains the results that can be accessed by their getter methods.
 */
export class AlgorithmModel {
	private readonly grammar         : Grammar;
	private readonly parserAlgorithm : ParserAlgorithm;

	private firstSets  = new Map<string, Set<string>>();
	private followSets = new Map<string, Set<string>>();
	private firstLogs  = "";
	private followLogs = "";

	// The index is also the state.
	// Index 0 means that the state for the set is I_0.
	private collectionSLR: CollectionSLR = [];
	private reductionsSLR: Reduction[]   = [];

	private collectionLogs : CollectionLog[] = [];
	private calculations   : string          = "";

	private currentState = 0;

	private lr0queue = new UniqueQueue<[number, string]>(([state, sym], [newState, newSym]) => state === newState && sym === newSym);

	private collectionLR1: CollectionLR1  = [];
	private reductionsLR1: ReductionLR1[] = [];

	private jumps: Jumps = new Map();

	constructor(grammar: Grammar, parserAlgorithm: ParserAlgorithm = ParserAlgorithm.SLR) {
		this.grammar         = grammar;
		this.parserAlgorithm = parserAlgorithm;
	}

	// Lookup function
	first = (symbol: string): Set<string> => this.firstSets.get(symbol)!;

	calculateFirstSets() {
		// Initialization
		this.grammar.nonTerminals.forEach(nt => this.firstSets.set(nt, new Set()));
		this.grammar.terminals.forEach(t => this.firstSets.set(t, new Set([t])));

		// Loops
		let endlessLoopSafety = 0;
		let outerIterations = 0;
		let allIterations = 0;
		let hasChanged = false;
		do {
			++endlessLoopSafety;
			++outerIterations;
			let innerIterations = 0;
			hasChanged = false;
			for (const nt of this.grammar.nonTerminals) {
				const rules = this.grammar.rules.get(nt)!;
				const oldSize = this.first(nt).size;
				for (const rule of rules) {
					if (rule.length === 1 && rule[0] === this.grammar.emptyWord) {
						this.first(nt).add(this.grammar.emptyWord);
						this.firstLogs += `First(${nt}) := First(${nt}) ∪ { ${this.grammar.emptyWord} } = { ${mkString(this.first(nt))} }\n`
					} else if (this.grammar.isTerminal(rule[0])) {
						this.first(nt).add(rule[0]);
						this.firstLogs += `First(${nt}) := First(${nt}) ∪ { ${rule[0]} } = { ${mkString(this.first(nt))} }\n`
					} else {
						const logSB = new StringBuilder();
						const newSet = this.firstOfRange(rule, logSB);
						const logStr = logSB.toString();
						this.firstLogs += logStr.charAt(logStr.length - 1) === "}" // A little hack for formatting
							? `First(${nt}) := (First(${nt})`
							: `First(${nt}) := First(${nt})`;
						this.firstLogs += logStr;
						this.first(nt).addAll(newSet);
						this.firstLogs += ` = First(${nt}) ∪ { ${mkString(newSet, _ => _ + ", ")} } = { ${mkString(this.first(nt))} }\n`
					}
					++innerIterations;
				}
				if (!hasChanged) {
					hasChanged = oldSize < this.first(nt).size;
				}
			}
			this.firstLogs += `Inner Iterations: ${innerIterations}\n`;
			allIterations += innerIterations;

			if (endlessLoopSafety > 5000) {
				throw Error("Probably Internal Error: Too many iterations (> 5000) for First Algorithm");
			}
		} while (hasChanged);
		this.firstLogs += `Outer Iterations: ${outerIterations}\nAll Iterations: ${allIterations}\n`;

		const rules = new Map(this.grammar.rules);
		const N = this.grammar.nonTerminals.size;
		const P = sum(rules.mapValues(_ => _.length));
		const p = foldLeft((m: number, v: string[][]) => max(m, foldLeft((m: number, v: string[]) => max(m, v.length))(0)(v)))(0)(Array.from(rules.values()));
		this.firstLogs += `|N| = ${N}, |P| = ${P}, p = ${p}\n`;
	}

	follow = (symbol: string): Set<string> => this.followSets.get(symbol)!;

	private firstOfRange(symbols: string[], sb: StringBuilder | Nothing = null): Set<string> {
		let result = new Set<string>();
		for (const symbol of symbols) {
			if (exists(sb)) { sb!.append(` ∪ First(${symbol})`); }
			const set = this.first(symbol);
			result.addAll(set);
			if (!set.has(this.grammar.emptyWord)) {
				result.delete(this.grammar.emptyWord);
				if (exists(sb)) { sb!.append(`) \\ { ${this.grammar.emptyWord} }`) }
				break;
			}
		}
		return result;
	}

	calculateFollowSets() {
		// Initialization
		// Follow(S') := { $ }.
		this.followSets.set(this.grammar.extendedStartSymbol, new Set("$"));

		// For all A ∈ N and A != S: Follow(A) := {}.
		Array.from(this.grammar.nonTerminals)
			.filter(nt => nt !== this.grammar.extendedStartSymbol)
			.forEach(nt => this.followSets.set(nt, new Set()));

		let firstIterations = 0;
		// First iteration, for all A ∈ N:
		for (const nt of this.grammar.nonTerminals) {
			for (const [, rhs] of this.grammar.rules) {
				for (const rule of rhs) {
					// For all B -> xAy ∈ P
					rule.forEach((symbol, index) => {
						if (symbol === nt) {
							// Follow(A) := Follow(A) ∪ (First(y) \ { e }).
							const symbolsAfterNT = rule.slice(index + 1);
							let followSet = this.firstOfRange(symbolsAfterNT);
							followSet.delete(this.grammar.emptyWord);
							let logStr = symbolsAfterNT.length === 0
								? this.grammar.emptyWord
								: mkString(symbolsAfterNT, _ => _ + " ", 1);
							this.followLogs += `Follow(${nt}) := Follow(${nt}) ∪ (First(${logStr}) \\ { ${this.grammar.emptyWord} }) = { ${mkString(this.follow(nt))} } ∪ { ${mkString(this.firstOfRange(symbolsAfterNT))} } = `;
							this.follow(nt).addAll(followSet);
							this.followLogs += `{ ${mkString(this.follow(nt))} }\n`;
						}
					});
					++firstIterations;
				}
			}
		}
		this.followLogs += `First Iterations: ${firstIterations}\n`;

		let endlessLoopSafety = 0;
		// Second iteration, for all A ∈ N:
		let outerIterations = 0;
		let allIterations = 0;
		let hasChanged = false;
		do {
			++endlessLoopSafety;
			++outerIterations;
			let innerIterations = 0;
			hasChanged = false;
			for (const nt of this.grammar.nonTerminals) {
				const oldSize = this.follow(nt).size;
				for (const [lhs, rhs] of this.grammar.rules) {
					for (const rule of rhs) {
						// For all B -> xAy ∈ P mit ε ∈ First(y)
						rule.forEach((symbol, index) => {
							if (symbol === nt) {
								const handlesAfterSymbol = rule.slice(index + 1);
								const firstOfFollow = this.firstOfRange(handlesAfterSymbol);
								if (firstOfFollow.has(this.grammar.emptyWord) || firstOfFollow.size === 0) {
									// Follow(A) := Follow(A) ∪ Follow(B)
									this.followLogs += `Follow(${nt}) := Follow(${nt}) ∪ Follow(${lhs}) = { ${mkString(this.follow(nt))} } ∪ { ${mkString(this.follow(lhs))} } = `;
									this.follow(nt).addAll(this.follow(lhs));
									this.followLogs += `{ ${mkString(this.follow(nt))} }\n`
								}
							}
						});
						++innerIterations;
					}
				}
				if (!hasChanged) {
					hasChanged = oldSize < this.follow(nt).size;
				}
			}
			this.followLogs += `Inner Iterations: ${innerIterations}\n`;
			allIterations += innerIterations;

			if (endlessLoopSafety > 5000) {
				throw Error("Probably Internal Error: Too many iterations (> 5000) for Follow Algorithm");
			}
		} while (hasChanged);
		this.followLogs += `Outer Iterations: ${outerIterations}\nAll Iterations: ${allIterations}\n`;
	}

	generateCollection() {
		switch (this.parserAlgorithm) {
			case ParserAlgorithm.SLR:
				this.genCollectionSLR();
				break;
			case ParserAlgorithm.LR1:
				this.genCollectionLR1();
				break;
			// NOTE: New algorithms can be inserted before the default case.
			default:
				alert("Invalid parser algorithm");
				break;
		}
	}

	private closure1(element: LR0Element): LR0CollectionEntry {
		return this.closure(new LR0CollectionEntry([element]));
	}

	// NOTE: Closure = Hülle
	private closure(set: LR0CollectionEntry): LR0CollectionEntry {
		let result = new LR0CollectionEntry(set);
		let hasChanged = false;

		let endlessLoopSafety = 0;
		do {
			hasChanged = false;
			let oldSize = result.getSize();

			for (const element of set) {
				const nt = element.getSymbolAfterPoint();
				// If C ∈ N, A -> a.Cb ∈ Closure(I) and C -> y ∈ P: C -> .y ∈ Closure(I).
				if (nt !== null && this.grammar.isNonTerminal(nt)) {
					const rules = this.grammar.rules.get(nt)!;
					const newElements = new LR0CollectionEntry(rules.map(rhs => new LR0Element(this.grammar, nt, rhs)));
					result.addAll(newElements);
					set.addAll(newElements); // REVIEW: Is it okay to mutate the set in a for each loop?
				}
			}

			hasChanged = result.getSize() > oldSize;

			++endlessLoopSafety;
			if (endlessLoopSafety > 5000) {
				throw Error("Probably Internal Error: Too many iterations (> 5000) for Closure");
			}
		} while (hasChanged);

		return result;
	}

	private jump(state: number, symbol: string): LR0Element[] {
		// REVIEW: Are there no nulls after the call? The symbol should not be enqueued, if the element can be reduced.
		const elements = this.collectionSLR[state].toArray().filter(e => e.getSymbolAfterPoint() === symbol);
		elements.forEach(e => assert(e !== null && !e.canReduce()));

		return elements.map(e => e.jump()![1]);
	}

	private hasClosureOccurred(closure: LR0CollectionEntry): number {
		let result = -1;
		this.collectionSLR.forEach((entry, index) => {
			if (entry.equals(closure)) { result = index; }
		});
		return result;
	}

	/**
	 * Calculates the canonical collection (the states for the state machine) for the grammar.
	 * It mutates the variables `collectionSLR`, `collectionLogs`, `reductionsSLR` and `jumps` that can be accessed with
	 * their getter methods.
	 */
	genCollectionSLR() {
		this.grammar.addSymbol("$");
		this.grammar.terminals.add("$");
		this.firstSets.set("$", new Set("$"));
		this.currentState = 0;

		// NOTE:I_0 ... I_n as array (push new closures/sets).

		const firstElement = new LR0Element(this.grammar, this.grammar.extendedStartSymbol, [this.grammar.startSymbol]);
		const firstClosure = this.closure1(firstElement);
		this.collectionSLR.push(firstClosure);
		const [elementsToEnqueue, reduceElements] = filter2(firstClosure, (e => !e.canReduce()));
		this.lr0queue.enqueueAll(elementsToEnqueue.map(e => [this.currentState, e.getSymbolAfterPoint()] as [number, string]));

		this.addCalculation("I0 = Hülle({ " + firstElement.toString() + " }) = { " + mkString(firstClosure, _ => _ + ", ") + " }");
		this.collectionLogs.push(new CollectionLog(0, [firstElement], this.collectionSLR[0].toArray()));

		reduceElements.forEach(e => {
			this.addCalculation("Reduce: " + e.toString());
			this.reductionsSLR.push(new Reduction(0, e.lhs, e.rhs));
		});

		let safety = 0;
		while (this.lr0queue.getSize() > 0 && safety < 100000) {
			++safety;

			// console.log("Queue: " + concat(this.lr0queue, ([st, sym]) => "(" + st + ", " + sym + ") ", 1));

			const [state, nextSym] = this.lr0queue.dequeue()!;
			const elementsAfterJump = this.jump(state, nextSym);

			const closureAfterJump = this.closure(new LR0CollectionEntry(elementsAfterJump));

			const occurrence = this.hasClosureOccurred(closureAfterJump);
			if (occurrence !== -1) {
				this.addCalculation("Sprung(" + state + ", '" + nextSym + "') = " + occurrence);
				this.jumps.set([state, nextSym], occurrence);
				this.collectionLogs.push(new CollectionLog(occurrence, elementsAfterJump, closureAfterJump.toArray(), state, nextSym));
				continue;
			}

			++this.currentState;
			this.collectionSLR.push(new LR0CollectionEntry());
			this.addCalculation("I" + this.currentState + " = Sprung(" + state + ", '" + nextSym + "') = Hülle({ " + mkString(elementsAfterJump, e => e.toString() + ", ") + " }) = { " + mkString(closureAfterJump, _ => _ + ", ") + " }");
			this.jumps.set([state, nextSym], this.currentState);
			this.collectionLogs.push(new CollectionLog(this.currentState, elementsAfterJump, closureAfterJump.toArray(), state, nextSym));

			const newElements = closureAfterJump.toArray().map(e => {
				this.collectionSLR[this.currentState].add(e);
				if (e.canReduce()) {
					this.addCalculation("Reduce: " + e.toString());
					this.reductionsSLR.push(new Reduction(this.currentState, e.lhs, e.rhs));
					return undefined;
				} else {
					return [this.currentState, e.getSymbolAfterPoint()] as [number, string];
				}
			}).filter(_ => _ !== undefined) as Array<[number, string]>;

			this.lr0queue.enqueueAll(newElements);
		}
	}

	private calculateLookahead(rest: string[], oldLookahead: ComparableSet<string>): ComparableSet<string> {
		let result = new ComparableSet(strEqualsFunc, map(oldLookahead, _ => rest.concat(_))
			.map(s => Array.from(this.firstOfRange(s)))
			.flat()
		);

		assert(result.getSize() < 1000, "Result (" + result.getSize() + ") = " + mkString(result, _ => _ + ", "));
		return result;
	}

	private lr1Closure1 = (element: LR1Element): LR1CollectionEntry => this.lr1Closure(new LR1CollectionEntry([element]));

	private lr1Closure(set: LR1CollectionEntry): LR1CollectionEntry {
		let result = new LR1CollectionEntry(set);
		let hasChanged = false;

		let safety = 0;

		do {
			++safety;
			hasChanged = false;
			let oldSize = result.getSize();

			for (const element of set) {
				const nt = element.getSymbolAfterPoint();
				if (nt !== null && this.grammar.isNonTerminal(nt)) {
					const rules = this.grammar.rules.get(nt)!;
					const lookahead = this.calculateLookahead(element.getRest(), element.lookahead);
					const newElements = new LR1CollectionEntry(rules.map(rhs => new LR1Element(this.grammar, nt, rhs, lookahead)));
					result.addAll(newElements);
					set.addAll(newElements);
				}
			}

			hasChanged = result.getSize() > oldSize;
		} while (hasChanged && safety < 2);

		return result;
	}

	private jumpLR1(state, symbol): LR1Element[] {
		const elements = this.collectionLR1[state].toArray().filter(e => e.getSymbolAfterPoint() === symbol);
		elements.forEach(e => assert(e !== null && !e.canReduce()));

		return elements.map(e => e.jump()![1]);
	}

	private lr1ClosureOccurrence(closure: LR1CollectionEntry): number {
		let result = -1;
		this.collectionLR1.forEach((entry, index) => {
			if (entry.equals(closure)) { result = index; }
		});
		return result;
	}

	/*
	 * elem: [ A -> a .B b, M ]

	 * [B -> α.Aβ, {x,y}]
	 * M = First(βx) ∪ First(βy)
	 * map(lookahead, _ => β + _).flatMap() // https://stackoverflow.com/questions/53556409/typescript-flatmap-flat-flatten-doesnt-exist-on-type-any
	 * [A -> .γ, M]
	 */
	genCollectionLR1() {
		this.grammar.addSymbol("$");
		this.grammar.terminals.add("$");
		this.firstSets.set("$", new Set("$"));

		const firstElement = new LR1Element(this.grammar, this.grammar.extendedStartSymbol, [this.grammar.startSymbol], ["$"]);
		const firstClosure = this.lr1Closure1(firstElement);
		this.collectionLR1.push(firstClosure);
		// this.lr0queue.enqueueAll(firstClosure.toArray().filter(e => !e.canReduce()).map(e => [this.currentState, e.getSymbolAfterPoint()] as [number, string]));

		const [elementsToEnqueue, reduceElements] = filter2(firstClosure, (e => !e.canReduce()));
		this.lr0queue.enqueueAll(elementsToEnqueue.map(e => [this.currentState, e.getSymbolAfterPoint()] as [number, string]));

		this.addCalculation("I0 = Hülle({ " + firstElement.toString() + " }) = { " + mkString(firstClosure, _ => _ + ", ") + " }");
		this.collectionLogs.push(new CollectionLog(0, [firstElement], this.collectionLR1[0].toArray()));

		reduceElements.forEach(e => {
			this.addCalculation("Reduce: " + e.toString());
			this.reductionsLR1.push(new ReductionLR1(0, e.lhs, e.rhs, e.lookahead));
		});

		let safety = 0;
		while (this.lr0queue.getSize() > 0 && safety < 100000) {
			++safety;

			console.log("Queue (size = " + this.lr0queue.getSize() + "): " + mkString(this.lr0queue, ([st, sym]) => "(" + st + ", " + sym + ") ", 1));

			const [state, nextSym] = this.lr0queue.dequeue()!;
			const elementsAfterJump = this.jumpLR1(state, nextSym);

			const closureAfterJump = this.lr1Closure(new LR1CollectionEntry(elementsAfterJump));

			const occurrence = this.lr1ClosureOccurrence(closureAfterJump);
			if (occurrence !== -1) {
				this.addCalculation("Sprung(" + state + ", '" + nextSym + "') = " + occurrence);
				this.jumps.set([state, nextSym], occurrence);
				this.collectionLogs.push(new CollectionLog(occurrence, elementsAfterJump, closureAfterJump.toArray(), state, nextSym));
				continue;
			}

			++this.currentState;
			this.collectionLR1.push(new LR1CollectionEntry());
			this.addCalculation("I" + this.currentState + " = Sprung(" + state + ", '" + nextSym + "') = Hülle({ " + mkString(elementsAfterJump, e => e.toString() + ", ") + " }) = { " + mkString(closureAfterJump, _ => _ + ", ") + " }");
			this.jumps.set([state, nextSym], this.currentState);
			this.collectionLogs.push(new CollectionLog(this.currentState, elementsAfterJump, closureAfterJump.toArray(), state, nextSym));

			const newElements = map(closureAfterJump, e => {
				this.collectionLR1[this.currentState].add(e);
				if (e.canReduce()) {
					this.addCalculation("Reduce: " + e.toString());
					this.reductionsLR1.push(new ReductionLR1(this.currentState, e.lhs, e.rhs, e.lookahead));
					return undefined;
				} else {
					return [this.currentState, e.getSymbolAfterPoint()] as [number, string];
				}
			}).filter(_ => _ !== undefined) as Array<[number, string]>;

			this.lr0queue.enqueueAll(newElements);
		}
	}

	private addCalculation(calculation: string) {
		this.calculations += calculation + "\n";
	}

	getSelectedParserAlgorithm = (): ParserAlgorithm => this.parserAlgorithm;

	/**
	 * @returns A map with the symbols whose first sets have been calculated so far.
	 */
	getFirstSets = (): Map<string, Set<string>> => this.firstSets;

	/**
	 * @returns A map with the symbols whose follow sets have been calculated so far.
	 */
	getFollowSets = (): Map<string, Set<string>> => this.followSets;

	getFirstLogs = (): string => this.firstLogs;

	getFollowLogs = (): string => this.followLogs;

	getCollection(): CollectionSLR | CollectionLR1 {
		if (this.parserAlgorithm === ParserAlgorithm.SLR) {
			return this.collectionSLR;
		} else if (this.parserAlgorithm === ParserAlgorithm.LR1) {
			return this.collectionLR1;
		} else {
			throw new Error("Invalid parser option in `getCollection()`. This should never happen");
		}
	}

	/**
	 * @returns The reductions for the parser table.
	 */
	getReductions(): Reduction[] | ReductionLR1[] {
		if (this.parserAlgorithm === ParserAlgorithm.SLR) {
			return this.reductionsSLR;
		} else if (this.parserAlgorithm === ParserAlgorithm.LR1) {
			return this.reductionsLR1;
		} else {
			throw new Error("Invalid parser option in `getReductions()`. This should never happen");
		}
	}

	/**
	 * @returns The canonical collection set with the states and their LR(0) elements.
	 */
	getCollectionSLR = (): CollectionSLR => this.collectionSLR;

	/**
	 * @returns The jumps that are used to create the table and state machine.
	 */
	getJumps = (): Jumps => this.jumps;

	/**
	 * @returns The logs of the collection creation.
	 */
	getCollectionLogs = (): CollectionLog[] => this.collectionLogs;

	/**
	 * @returns The calculations as if they were written down.
	 */
	getCalculations = (): string => this.calculations;


	// Methods for tests.

	testCall_closure1    = (element: LR0Element)           => this.closure1(element);
	testCall_closure     = (set: LR0CollectionEntry)       => this.closure(set);
	testCall_lr1Closure1 = (element: LR1Element)           => this.lr1Closure1(element);
	testCall_jump        = (state: number, symbol: string) => this.jump(state, symbol);

	test_setCollectionSLR(data: CollectionSLR) {
		this.collectionSLR = data;
	}
}

/**
 * This class defines a collection log.
 * It is used to collect the data that will be displayed in `CollectionView`.
 */
export class CollectionLog {
	private toState            : number;
	private readonly fromState : number | null;
	private readonly symbol    : string | null;
	private readonly elements  : LR0Element[] | LR1Element[];
	private readonly closure   : LR0Element[] | LR1Element[];

	constructor(toState: number, elements: LR0Element[] | LR1Element[], closure: LR0Element[] | LR1Element[], fromState: number | null = null, symbol: string | null = null) {
		this.toState   = toState;
		this.elements  = elements;
		this.closure   = closure;
		this.fromState = fromState;
		this.symbol    = symbol;
	}

	private hasJump = (): boolean => this.fromState !== null && this.symbol !== null;

	/**
	 * @returns An array with four entries that represent the content of a row in the `CollectionView`.
	 */
	formatToArray(): string[] {
		return [
			this.hasJump()
				? "Goto(" + this.fromState + ", " + this.symbol + ")"
				: "",
			mkString(this.elements, _ => _ + ", "),
			this.toState.toString(),
			mkString(this.closure, _ => _ + ", ")
		];
	}
}
