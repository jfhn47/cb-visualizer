import './App.css';
import VisLR from './VisLR';
import VisTree from './vis-tree/VisTree';
import {useCookies} from 'react-cookie';
import {exists} from './util';

const Tools = {
	LR_PARSING   : "lr-parsing",
	TREE_PARSING : "tree-parsing",
};

const App = () => {
	const [cookies, setCookie, removeCookie] = useCookies(['selected-tool']);

	console.log(cookies['selected-tool']);

	const selectedTool = (): string => {
		return cookies['selected-tool'];
	}

	const setSelectedTool = (tool: string) => {
		setCookie('selected-tool', tool, {path: "/"});
	}

	const removeSelectedTool = () => {
		removeCookie('selected-tool', {path: "/"});
	}

	return <div> {
		!exists(selectedTool()) &&
		<div>
			<div style={{ alignContent: "center", textAlign: "center", fontSize: "36pt" }}>
				<h2>Visualization Tools</h2>
				<button
					style={{ fontSize: "20pt" }}
					onClick={() => {
						setSelectedTool(Tools.LR_PARSING);
					}}
				>
					LR Parsing
				</button>
				<br />
				<button
					style={{ fontSize: "20pt" }}
					onClick={() => {
						setSelectedTool(Tools.TREE_PARSING);
					}}
				>
					&#120548;-Tree Parsing
				</button>
			</div>
			<footer>
				<div style={{ alignContent: "center", textAlign: "center", fontSize: "36pt", position: "fixed", bottom: "2%", width: "100%"}}>
					<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
					<button
						style={{ fontSize: "14pt" }}
						onClick={() => window.open("https://git.thm.de/jfhn47/cb-visualizer")}
					>
						<i className="fa fa-gitlab" style={{fontSize: "28px", color: "#FC6D26"}}/> Gitlab-Repository
					</button>
				</div>
			</footer>
		</div>
	} {
		selectedTool() === Tools.LR_PARSING &&
		<VisLR onBackToToolSelection={removeSelectedTool}/>
	} {
		selectedTool() === Tools.TREE_PARSING &&
		<VisTree onBackToToolSelection={removeSelectedTool}/>
	}
	</div>;
}

export default App;
