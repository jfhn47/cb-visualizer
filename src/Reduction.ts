import { Comparable, ComparableSet, mkString, zip } from "./util";

export class Reduction implements Comparable<Reduction> {
	state : number;
	lhs   : string;
	rhs   : string[];

	constructor(state: number, lhs: string, rhs: string[]) {
		this.state = state;
		this.lhs   = lhs;
		this.rhs   = rhs;
	}

	// NOTE: The state does not matter.
	equals(other: Reduction): boolean {
		if (this.rhs.length !== other.rhs.length) {
			return false;
		}
		for (const [l, r] of zip(this.rhs, other.rhs)) {
			if (l !== r) { return false; }
		}
		return this.lhs === other.lhs;
	}

	formatForTable(): string {
		if (this.lhs === "S'") { return "accept"; }
		return "reduce " + this.lhs + " -> " + mkString(this.rhs, _ => _ + " ", 1);
	}

	formatForLogs(): string {
		return "Reduce " + this.lhs + " -> " + mkString(this.rhs, _ => _ + " ", 1);
	}

	toString(): string {
		return "(I_" + this.state + ", " + this.lhs + " -> " + mkString(this.rhs, _ => _ + " ", 1) + ")";
	}
}

export class ReductionLR1 extends Reduction {
	lookahead: ComparableSet<string>;

	constructor(state: number, lhs: string, rhs: string[], lookahead: ComparableSet<string>) {
		super(state, lhs, rhs);
		this.lookahead = lookahead;
	}
}
