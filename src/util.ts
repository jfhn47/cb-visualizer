export const mkString = <T>(
	it: Iterable<T>,
	f: (_: T) => string = _ => _ + ", ",
	sepLen: number = 2
): string => {
	let res = "";
	for (const e of it) {
		res += f(e);
	}
	return res.substring(0, res.length - sepLen);
}

export const assert = (condition: any, msg: string = "Assertion failed") /* : asserts condition */ => {
	if (!condition) throw new Error(msg);
}

export const throwExpr = <T>(error: T): never => {
	throw error;
}

export const zip = <A, B>(a1: A[], a2: B[]): [A, B][] => {
	let result: [A, B][] = [];
	const len = a1.length < a2.length ? a1.length : a2.length;
	for (let i = 0; i < len; ++i) {
		result.push([a1[i], a2[i]]);
	}
	return result;
}

export const foldLeft = <A, B>(f: (acc: A, v: B) => A) => {
	const iterator = (acc: A, it: Iterator<B>): A => {
		const next = it.next();
		return next.done ? acc : iterator(f(acc, next.value), it);
	}
	return (start: A) => (it: Iterable<B>) => iterator(start, it[Symbol.iterator]());
}

export const sum = foldLeft<number, number>((acc, v) => acc + v)(0)

const _max = foldLeft<number, number>((m, v) => m >= v ? m : v)(Number.MIN_SAFE_INTEGER);
export const max = (...args: number[]): number => _max(args);

export const all = <T>(pred: (_: T) => boolean) => foldLeft<boolean, T>((acc, t) => acc && pred(t))(true);
export const anyMatch = <T>(pred: (_: T) => boolean) => foldLeft<boolean, T>((acc, t) => acc || pred(t))(false);

export const map = <A, B>(it: Iterable<A>, f: (_: A) => B): B[] => {
	let result: B[] = [];
	for (const v of it) {
		result.push(f(v));
	}
	return result;
}

export const filter2 = <T>(list: Iterable<T>, f: (_: T) => boolean): [T[], T[]] => {
	let wanted   : T[] = [];
	let unwanted : T[] = [];
	for (const v of list) {
		if (f(v)) wanted.push(v);
		else      unwanted.push(v);
	}
	return [wanted, unwanted];
}

export const range = (start: number, end: number): number[] => {
	let result: number[] = [];
	for (let i = start; i < end; ++i) {
		result.push(i);
	}
	return result;
}

export const split1 = (str: string, sep: string): [string, string] => {
	assert(sep.length === 1, "split1() can only be used with a separator of length 1");

	let idx = 0;
	for (; idx < str.length; ++idx) {
		if (sep === str[idx]) { break; }
	}

	return [str.substring(0, idx - 1), str.substring(idx + 1)];
}

export const noop = (...args: any[]) => {
	[].forEach(_ => args);
}

export const exists = (v: any): boolean => v !== undefined && v !== null;

export const isIterable = <T>(obj: T): boolean => obj !== null && typeof obj![Symbol.iterator] === "function";

/*
export const concatArrays = <T>(...arrays: T[][]): T[] => {
	let result = [] as T[];
	for (const array of arrays) {
		result.push(array);
	}
	return result;
}
*/

// Is this possible?
/*
export const deepCopy = <T>(it: Iterable<T>): Iterable<T> => {
	let result: any[] = [];
	for (const v of it) {
		result.push(isIterable(v) ? deepCopy(<Iterable<any>>v) : v);
	}
	return result;
}
*/

const stableMerge = <T>(left: T[], right: T[], cmpFunc: (a: T, b: T) => boolean): T[] => {
	const result = [] as T[];

	while (left.length > 0 && right.length > 0) {
		if (cmpFunc(left[0], right[0])) {
			result.push(left.shift()!);
		} else {
			result.push(right.shift()!);
		}
	}

	result.push(...left);
	result.push(...right);

	return result;
}

const defaultStableMergeSortCompareFunc = <T>(a: T, b: T): boolean => a <= b;

export const stableSort = <T>(items: T[], cmpFunc: (a: T, b: T) => boolean = defaultStableMergeSortCompareFunc): T[] => {
	if (items.length <= 1) {
		return items;
	}
	const mid   = Math.floor(items.length / 2);
	const left  = stableSort(items.slice(0, mid), cmpFunc);
	const right = stableSort(items.slice(mid, items.length), cmpFunc);
	return stableMerge(left, right, cmpFunc);
}

export class Stack<T> {
	private readonly items: T[];

	constructor(init?: T[]) {
		this.items = init ?? [];
	}

	push = (e: T): this => {
		this.items.push(e);
		return this;
	}

	pop = (): T | undefined => {
		const oldSize = this.items.length;
		const result = this.items.pop();
		console.assert(this.items.length === oldSize - 1, "Pop is wrong");
		return result;
	}

	size = () => this.items.length;

	top = () => this.items[this.items.length - 1];

	[Symbol.iterator](): Iterator<T> {
		let counter = 0;
		return {
			next: () => {
				return {
					done  : counter >= this.items.length,
					value : this.items[counter++]
				}
			}
		}
	}
}

export type Nothing = null | undefined;

export interface Comparable<T> {
	equals: (other: T) => boolean;
}

export const strEqualsFunc = (s: string, o: string) => s === o;

export class UniqueQueue<T> {
	private data: T[] = [];
	private compareFunc: (elem: T, newElem: T) => boolean;

	constructor(compareFunc: (elem: T, newElem: T) => boolean) {
		this.compareFunc = compareFunc;
	}

	[Symbol.iterator](): Iterator<T> {
		let counter = 0;
		return {
			next: () => {
				return {
					done  : counter >= this.data.length,
					value : this.data[counter++]
				}
			}
		}
	}

	enqueue(v: T): boolean {
		let vIsUnique = true;
		this.data.forEach(_ => {
			if (this.compareFunc(_, v)) { vIsUnique = false; }
		});
		if (!vIsUnique) { return false; }
		this.data.push(v);
		return true;
	}

	enqueueAll(vs: Iterable<T>): number {
		let counter = 0;
		for (const v of vs) {
			counter += this.enqueue(v) ? 1 : 0;
		}
		return counter;
	}

	dequeue = (): T | undefined => this.data.shift();

	getSize = (): number => this.data.length;

	clear() {
		this.data = [];
	}

	toArray = (): T[] => this.data;

	toString = (): string => mkString(this.data, _ => _ + ", ");
}

// TODO: @speed Implement a hash set.
export class ComparableSet<T> implements Comparable<ComparableSet<T>>, Iterable<T> {
	private data: T[] = [];
	private compareFunc: (elem: T, newElem: T) => boolean;

	constructor(compareFunc: (elem: T, newElem: T) => boolean, data?: Iterable<T>) {
		this.compareFunc = compareFunc;
		if (data !== undefined) {
			this.addAll(data);
		}
	}

	[Symbol.iterator](): Iterator<T> {
		let counter = 0;
		return {
			next: () => {
				return {
					done  : counter >= this.data.length,
					value : this.data[counter++]
				}
			}
		}
	}

	add(v: T): boolean {
		let vIsUnique = true;
		this.data.forEach(_ => {
			if (this.compareFunc(_, v)) { vIsUnique = false; }
		});
		if (!vIsUnique) { return false; }
		this.data.push(v);
		return true;
	}

	addAll(set: Iterable<T>): number {
		let counter = 0;
		for (const e of set) {
			counter += this.add(e) ? 1 : 0;
		}
		return counter;
	}

	has(v: T): boolean {
		for (const e of this.data) {
			if (this.compareFunc(e, v)) { return true; }
		}
		return false;
	}

	equals(other: ComparableSet<T>): boolean {
		if (this.getSize() !== other.getSize()) { return false; }

		for (const v of this) {
			if (!other.has(v)) { return false; }
		}
		for (const v of other) {
			if (!this.has(v)) { return false; }
		}

		return true;
	}

	getSize = (): number => this.data.length;

	toArray = (): T[] => this.data;
}

export class ComparableMap<K extends Comparable<K>, V> implements Comparable<ComparableMap<K, V>>, Iterable<[K, V]> {
	private innerMap: Map<K, V> = new Map();

	set = (key: K, value: V): boolean => {
		for (const k of this.innerMap.keys()) {
			if (k.equals(key)) {
				return false;
			}
		}
		this.innerMap.set(key, value);
		return true;
	}

	*[Symbol.iterator]() {
		yield* [...this.innerMap.entries()];
	}

	// TODO
	equals = (_: ComparableMap<K, V>): boolean => false;
}

export class StringBuilder {
	private bucket: string[] = [];

	constructor(...init: string[]) {
		for (const str of init) {
			this.bucket.push(str);
		}
	}

	append(str: string) {
		if (exists(str)) { this.bucket.push(str); }
	}

	toString = () => this.bucket.join("");
}

declare global {
	interface Set<T> {
		addAll(values: Iterable<T>): void;
	}

	interface Map<K, V> {
		filterByKey(predicate: (_: K) => boolean): Map<K, V>;
		mapValues<T>(f: (_: V) => T): T[];
	}
}

// eslint-disable-next-line no-extend-native
Set.prototype.addAll = function<T>(this: Set<T>, values: Iterable<T>) {
	for (const v of values) {
		this.add(v);
	}
}

// eslint-disable-next-line no-extend-native
Map.prototype.filterByKey = function<K, V>(this: Map<K, V>, predicate: (_: K) => boolean) {
	let result = new Map<K, V>();
	for (const [key, val] of this) {
		if (predicate(key)) {
			result.set(key, val);
		}
	}
	return result;
}

// eslint-disable-next-line no-extend-native
Map.prototype.mapValues = function<K, V, T>(this: Map<K, V>, f: (_: V) => T): T[] {
	let result: T[] = [];
	for (const [, value] of this) {
		result.push(f(value));
	}
	return result;
}
