import { AlgorithmModel } from "./AlgorithmModel";
import { Grammar } from "./Grammar";
import "./ParserTableSLR.css"
import React from 'react';
import { ConflictTableEntry, ParserTableModel } from "./ParserTableModel";
import Popup from "reactjs-popup";

interface Props {
	grammar    : Grammar;
	algorithms : AlgorithmModel;
}

interface State {
	row               : number;
	column            : number;
	table             : ParserTableModel | undefined;
	activeCellColours : [string, string],
}

export const lightCellColours : [string, string] = ["#FCFAED", "#007E8A"];
export const darkCellColours  : [string, string] = ["#72737A", "#FFC66D"];

export class ParserTableView extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);
		this.state = {
			row               : 0,
			column            : 0,
			table             : undefined,
			activeCellColours : lightCellColours
		};
	}

	private setTable(tab: ParserTableModel) {
		this.setState({
			table : tab
		});
	}

	private createTable() {
		this.setTable(new ParserTableModel(this.props.grammar, this.props.algorithms));
	}

	setActiveCellColours(colours: [string, string]) {
		this.setState({
			activeCellColours : colours
		});
	}

	setActiveCell(state: number, symbol: string) {
		this.setState({
			row    : state + 1,
			column : this.state.table!.getColumn(symbol)
		});
	}

	override componentDidMount() {
		this.createTable();
	}

	// TODO: Find out how to update the table when the grammar was updated and the OK button pressed.

	renderTable() {
		const actionCellLength = this.props.grammar.getTerminalsAsArray().length;
		const jumpCellLength   = this.props.grammar.getNonTerminalsAsArray().length;

		return <tbody>
			<tr><td></td><td colSpan={actionCellLength}>Action</td><td colSpan={jumpCellLength}>Jump</td></tr>
			{
				this.state.table!.getEntries().map((_, row) => <tr>{_.map((entry, col) => {
					if (entry instanceof ConflictTableEntry) {
						return (
							<Popup
								trigger={<td style={{borderColor: entry.bgColour}} >{entry.toString()}</td>}
								position="top center"
								mouseLeaveDelay={300}
								contentStyle={{ padding: "0px", border: "1px solid black", backgroundColor: "#ffffff" }}
							>
								{ entry.entries.map((inner, index) => <div><button onClick={() => { entry.setActiveEntry(index); this.forceUpdate() }}>{inner.toString()}</button></div>) }
							</Popup>
						)
					}

					if (row === this.state.row && col === this.state.column) {
						return <td style={{backgroundColor: this.state.activeCellColours[0], color: this.state.activeCellColours[1], fontWeight: 900}}>{entry.toString()}</td>
					}
					return <td>{entry.toString()}</td>
				})}</tr>)
			}
		</tbody>
	}

	override render() {
		return (
			<div>
				<table id="parser_table_slr" className="parser_table">
					{(this.state.table !== undefined && this.renderTable()) || "Internal Error: Table is undefined"}
				</table>
			</div>
		);
	}
}
