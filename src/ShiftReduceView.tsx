import React, { createRef, RefObject } from "react";
import { ShiftReduceModel } from "./ShiftReduceModel";
import { range } from "./util";

interface Props {
	shiftReduceModel : ShiftReduceModel;
}

interface State {
	logs  : string[][];
	model : ShiftReduceModel | null;
}

export default class ShiftReduceView extends React.Component<Props, State> {
	private endRef ?: RefObject<HTMLDivElement>;

	constructor(props: Props) {
		super(props);
		this.state = {
			logs  : [], // Array.from({length: 1}, () => Array.from({length: 4})),
			model : null
		};
	}

	private internalStackLogs  : string[] = [];
	private internalStateLogs  : string[] = [];
	private internalInputLogs  : string[] = [];
	private internalActionLogs : string[] = [];

	private index = -1;

	override componentDidMount() {
		this.props.shiftReduceModel.setView(this);
		this.endRef = createRef<HTMLDivElement>();
	}

	override componentDidUpdate() {
		this.endRef?.current?.scrollIntoView({ behavior: "smooth" }); // Auto scrolling
	}

	newRow() {
		this.internalStackLogs.push();
		this.internalStateLogs.push();
		this.internalInputLogs.push();
		this.internalActionLogs.push();
		++this.index;
	}

	setStack(newStack: string) {
		this.internalStackLogs[this.index] = newStack;
		this.updateLogs();
	}

	setStates(newStates: string) {
		this.internalStateLogs[this.index] = newStates;
		this.updateLogs();
	}

	setInput(newInput: string) {
		this.internalInputLogs[this.index] = newInput;
		this.updateLogs();
	}

	setAction(action: string, finished: boolean = false) {
		const index = finished ? this.index : this.index - 1;
		this.internalActionLogs[index] = action;
		this.updateLogs();
	}

	private updateLogs() {
		this.setState({
			logs : range(0, this.index + 1).map(row => [
				this.internalStackLogs[row]  ?? "",
				this.internalStateLogs[row]  ?? "",
				this.internalInputLogs[row]  ?? "",
				this.internalActionLogs[row] ?? ""
			])
		});
	}

	override render() {
		return (
			<table style={{width: '100%'}}>
				<tbody>
					<tr>
						<th>Symbols</th>
						<th>States</th>
						<th>Word</th>
						<th>Action</th>
					</tr>
					{
						this.state.logs.map(row => <tr>{row.map(col => <td>{col}</td>)}</tr>)
					}
					<div ref={this.endRef}></div>
				</tbody>
			</table>
		);
	}
}
