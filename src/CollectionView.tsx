import React from "react";
import { CollectionLog } from "./AlgorithmModel";

interface Props {
	logs: CollectionLog[];
}

export class CollectionView extends React.Component<Props> {
	override render() {
		return (
			<div style={{whiteSpace: "pre-line", float: "left", textAlign: "center", width: "100%"}}>
				<table style={{width: "100%"}}>
					<tr>
						<th>Goto</th>
						<th>Elements</th>
						<th>State</th>
						<th>Closure</th>
					</tr>
					<tbody>
						{
							this.props.logs.map(log => {
								const format = log.formatToArray();
								return <tr>
									<td>{format[0]}</td>
									<td>{format[1]}</td>
									<td>{format[2]}</td>
									<td>{format[3]}</td>
								</tr>;
							})
						}
					</tbody>
				</table>
			</div>
		);
	}
}
