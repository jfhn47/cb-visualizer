import { Grammar } from "./Grammar";
import { zip, Comparable, mkString, ComparableSet, strEqualsFunc } from "./util";

/**
 * Represents a LR(0) element.
 */
export class LR0Element implements Comparable<LR0Element> {
	grammar : Grammar;
	lhs     : string;
	rhs     : string[];
	index   : number;

	constructor(grammar: Grammar, lhs: string, rhs: string[], index: number = 0) {
		this.grammar = grammar;
		this.lhs     = lhs;
		this.rhs     = rhs;
		this.index   = index;
	}

	/**
	 * @returns The symbol after the point or null.
	 */
	getSymbolAfterPoint(): string | null {
		for (let i = this.index; i < this.rhs.length; ++i) {
			if (this.rhs[i] === this.grammar.emptyWord) { continue; }
			return this.rhs[i];
		}
		return null;
	}

	/**
	 * @returns The symbol that was jumped over and the new LR(0) element.
	 */
	jump(): [string, LR0Element] | null {
		for (let i = this.index; i < this.rhs.length; ++i) {
			if (this.rhs[i] === this.grammar.emptyWord) { continue; }
			return [this.rhs[i], new LR0Element(this.grammar, this.lhs, this.rhs, i + 1)];
		}
		return null;
	}

	/**
	 * @returns If the LR(0) element can be reduced (i.e. the dot is on the right).
	 */
	canReduce(): boolean {
		for (let i = this.index; i < this.rhs.length; ++i) {
			if (this.rhs[i] === this.grammar.emptyWord) { continue; }
			return false;
		}
		return true;
	}

	/**
	 * @param other The other LR(0) element.
	 * @returns If the LR(0) elements are equal.
	 */
	equals(other: LR0Element): boolean {
		if (this.rhs.length !== other.rhs.length) {
			return false;
		}
		for (const [l, r] of zip(this.rhs, other.rhs)) {
			if (l !== r) { return false; }
		}
		return this.lhs === other.lhs && this.index === other.index;
	}

	// TODO: @rework This is not a very clever solution, but it is just a toString() method.
	toString(): string {
		const showSym = (s: string): string => {
			if (s === this.grammar.emptyWord) { return ""; }
			return s;
		}

		let str = this.lhs + " -> ";

		for (let i = 0; i < this.index; ++i) {
			str += showSym(this.rhs[i]) + " ";
		}

		str += ".";

		for (let i = this.index; i < this.rhs.length; ++i) {
			str += showSym(this.rhs[i]) + " ";
		}

		if (this.index === this.rhs.length) {
			return str.substr(0, str.length - 2) + ".";
		}

		return str.substr(0, str.length - 1);
	}
}

export const lr0EqualsFunc = (elem: LR0Element, newElem: LR0Element) => elem.equals(newElem);

export class LR1Element extends LR0Element {
	lookahead : ComparableSet<string>;

	constructor(grammar: Grammar, lhs: string, rhs: string[], lookahead: Iterable<string>, index: number = 0) {
		super(grammar, lhs, rhs, index);
		this.lookahead = new ComparableSet<string>(strEqualsFunc, lookahead);
	}

	override equals(other: LR1Element): boolean {
		return super.equals(other as LR0Element) && this.lookahead.equals(other.lookahead);
	}

	override jump(): [string, LR1Element] | null {
		for (let i = this.index; i < this.rhs.length; ++i) {
			if (this.rhs[i] === this.grammar.emptyWord) { continue; }
			return [this.rhs[i], new LR1Element(this.grammar, this.lhs, this.rhs, this.lookahead, i + 1)];
		}
		return null;
	}

	getRest = (): string[] => this.rhs.slice(this.index + 1);

	override toString(): string {
		return "[" + super.toString() + ", { " + mkString(this.lookahead, _ => _ + ", ") + " }]";
	}
}

export const lr1EqualsFunc = (elem: LR1Element, newElem: LR1Element) => elem.equals(newElem);
