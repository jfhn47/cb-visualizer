import { SweetAlertIcon } from "sweetalert2";

export class ErrorMessage {
	icon  : SweetAlertIcon;
	title : string;
	text  : string;

	constructor(icon: SweetAlertIcon, title: string, text: string) {
		this.icon  = icon;
		this.title = title;
		this.text  = text;
	}
}
