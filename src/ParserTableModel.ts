import { AlgorithmModel, ParserAlgorithm } from "./AlgorithmModel";
import { Reduction, ReductionLR1 } from "./Reduction";
import { Grammar } from "./Grammar";
import { assert, map } from "./util";

// NOTE: The type of every table entry becomes 'Object' at runtime.
//       That means, a variable that defines the type is necessary.
export interface TableEntry {
	type       : string;
	toString() : string;
}

export class SymbolTableEntry implements TableEntry {
	type   : string = "SymbolTableEntry";
	symbol : string;

	constructor(symbol: string) {
		this.symbol = symbol;
	}

	toString = (): string => this.symbol;
}

export class StateTableEntry implements TableEntry {
	type  : string = "StateTableEntry";
	state : number;

	constructor(state: number) {
		this.state = state;
	}

	toString = (): string => "I" + this.state;
}

export class ShiftTableEntry implements TableEntry {
	type      : string = "ShiftTableEntry";
	nextState : number;

	constructor(nextState: number) {
		this.nextState = nextState;
	}

	toString = (): string => "s" + this.nextState;
}

export class JumpTableEntry implements TableEntry {
	type      : string = "JumpTableEntry";
	nextState : number;

	constructor(nextState: number) {
		this.nextState = nextState;
	}

	toString = (): string => "I" + this.nextState;
}

export class ReduceTableEntry implements TableEntry {
	type          : string = "ReduceTableEntry";
	reduction     : Reduction;
	isAcceptState : boolean;

	constructor(reduction: Reduction, isAcceptState: boolean) {
		this.reduction     = reduction;
		this.isAcceptState = isAcceptState;
	}

	toString = (): string => this.reduction.formatForTable();
}

export class ErrorTableEntry implements TableEntry {
	type: string = "ErrorTableEntry";

	toString = (): string => "";
}

export class ConflictTableEntry implements TableEntry {
	type     : string = "ConflictTableEntry";
	bgColour : string = "#CC5555";
	entries  : TableEntry[];
	selected : number = 0;

	constructor(entries: TableEntry[] = []) {
		this.entries = entries;
	}

	toString = (): string => this.entries[this.selected].toString();

	getActiveEntry = (): TableEntry =>this.entries[this.selected];

	setActiveEntry(index: number) {
		this.selected = index;
		// console.log("New selected = " + this.selected);
	}

	addEntry(e: TableEntry) {
		this.entries.push(e);
	}
}

export class ParserTableModel {
	private data: TableEntry[][];

	constructor(grammar: Grammar, algorithms: AlgorithmModel) {
		const configSet       = algorithms.getCollection();
		const jumps           = algorithms.getJumps();
		const reductions      = algorithms.getReductions();
		const parserAlgorithm = algorithms.getSelectedParserAlgorithm();

		const header = grammar.getTerminalsAsArray().concat(grammar.getNonTerminalsAsArray());

		this.data = Array.from({length: configSet.length + 1}, () => Array.from({length: header.length + 1}));

		// Set every entry to the error entry.
		for (let i = 0; i < this.data.length; ++i) {
			assert(this.data[0].length === this.data[i].length);
			for (let j = 0; j < this.data[0].length; ++j) {
				this.data[i][j] = new ErrorTableEntry();
			}
		}

		// Add all symbols to the table header.
		for (let col = 1; col < this.data[0].length; ++col) {
			this.data[0][col] = new SymbolTableEntry(header[col - 1]);
		}

		// Add the states to the left side of the table.
		for (let row = 1; row < this.data.length; ++row) {
			this.data[row][0] = new StateTableEntry(row - 1);
		}

		// Enter the jumps.
		for (const [[oldState, symbol], newState] of jumps) {
			const col = header.indexOf(symbol) + 1;
			if (grammar.nonTerminals.has(symbol)) {
				this.addEntry(oldState + 1, col, new JumpTableEntry(newState));
			} else {
				this.addEntry(oldState + 1, col, new ShiftTableEntry(newState));
			}
		}

		// Enter the reductions.
		for (const reduction of reductions) {
			let indices: number[] = [];
			if (parserAlgorithm === ParserAlgorithm.SLR) {
				indices = Array.from(algorithms.follow(reduction.lhs)).map(sym => header.indexOf(sym) + 1);
			} else if (parserAlgorithm === ParserAlgorithm.LR1) {
				indices = map((reduction as ReductionLR1).lookahead, sym => header.indexOf(sym) + 1);
			} else {
				console.log("Invalid parser algorithm in constructor of ParserTableModel");
			}

			indices.forEach(i => {
				this.addEntry(reduction.state + 1, i,
				              new ReduceTableEntry(reduction, reduction.lhs === grammar.extendedStartSymbol));
			});
		}
	}

	private addEntry(row: number, col: number, entry: TableEntry) {
		let cell = this.data[row][col];

		if (cell instanceof ErrorTableEntry) {
			this.data[row][col] = entry;
			return;
		}

		if (cell instanceof ConflictTableEntry) {
			cell.addEntry(entry);
			return;
		}

		const oldEntry = cell;
		this.data[row][col] = new ConflictTableEntry([oldEntry, entry]);
	}

	getEntry(state: number, symbol: string): TableEntry {
		if (state >= this.data.length || state < 0) {
			return new ErrorTableEntry();
		}

		const symIdx = this.data[0].findIndex((entry) => entry.toString() === symbol);
		if (symIdx === -1) {
			return new ErrorTableEntry();
		}

		const entry = this.data[state + 1][symIdx];
		// console.log(typeof this.data[state][symIdx]);

		if (entry instanceof ConflictTableEntry) {
			return entry.getActiveEntry();
		}

		return entry;
	}

	getColumn = (symbol: string): number => this.data[0].findIndex((entry) => entry.toString() === symbol);

	getEntries = (): TableEntry[][] => this.data;

	toStringArray = (): string[][] => this.data.map(row => row.map(col => col.toString()));

	toString(): string {
		let res = "";
		for (let i = 0; i < this.data.length; ++i) {
			for (let j = 0; j < this.data[0].length; ++j) {
				res += this.data[i][j].toString() + "\t";
			}
			res += "\n";
		}
		return res;

		// REVIEW: @optional Concat skips the last non terminal in the header.
		// return concat(this.data, row => concat(row, entry => this.formatTableEntry(entry) + "\t") + "\n");
	}
}
