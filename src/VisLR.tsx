import React, { createRef, RefObject } from 'react';
import { AlgorithmModel, ParserAlgorithm } from './AlgorithmModel';
import { ParserError, ShiftReduceModel } from './ShiftReduceModel';
import "./App.css";
import { Grammar, InputError } from './Grammar';
import { darkCellColours, lightCellColours, ParserTableView } from './ParserTableView';
import { ParserGraphModel } from './ParserGraphModel';
import ShiftReduceAlgorithmView from './ShiftReduceView';
import { FirstFollowView } from './FirstFollowView';
import { CollectionView } from './CollectionView';
import { GraphColours, ParserGraphView } from './ParserGraphView';
import { Tab, Tabs } from './Tabs';
import Swal from "sweetalert2";
import withReactContent from 'sweetalert2-react-content';
import { ErrorMessage } from './ErrorMessage';
import Popup from 'reactjs-popup';
import { noop } from './util';

const SweetAlert = withReactContent(Swal);

interface State {
	grammarInput            : string;
	grammar                 : Grammar          | null;
	algorithms              : AlgorithmModel   | null;
	graph                   : ParserGraphModel | null;
	shiftReduceAlgorithm    : ShiftReduceModel | null;
	errorMessage            : ErrorMessage     | null;
	shiftReduceFinished     : boolean;
	showHelp                : boolean;
	selectedParserAlgorithm : ParserAlgorithm;

	startSymbolRef  ?: RefObject<HTMLInputElement>;
	emptyWordRef    ?: RefObject<HTMLInputElement>;
	grammarInputRef ?: RefObject<HTMLTextAreaElement>;
	toggleRef       ?: RefObject<HTMLInputElement>;
	headerRef       ?: RefObject<HTMLHeadingElement>;
	tableRef        ?: RefObject<ParserTableView>;
	wordInputRef    ?: RefObject<HTMLInputElement>;
	shiftReduceLogs ?: RefObject<ShiftReduceAlgorithmView>;
	graphViewRef    ?: RefObject<ParserGraphView>;
}

interface VisLRProps {
	onBackToToolSelection: () => void;
}

class VisLR extends React.Component<VisLRProps, State> {
	constructor(props: VisLRProps) {
		super(props);
		this.state = {
			grammarInput            : "",
			grammar                 : null,
			algorithms              : null,
			graph                   : null,
			shiftReduceAlgorithm    : null,
			errorMessage            : null,
			shiftReduceFinished     : false,
			showHelp                : false,
			selectedParserAlgorithm : ParserAlgorithm.SLR
		}
	}

	hideErrorBox() {
		this.setState({
			errorMessage : null
		});
	}

	override componentDidMount() {
		this.setState({
			startSymbolRef  : createRef<HTMLInputElement>(),
			emptyWordRef    : createRef<HTMLInputElement>(),
			grammarInputRef : createRef<HTMLTextAreaElement>(),
			toggleRef       : createRef<HTMLInputElement>(),
			headerRef       : createRef<HTMLHeadingElement>(),
			tableRef        : createRef<ParserTableView>(),
			wordInputRef    : createRef<HTMLInputElement>(),
			shiftReduceLogs : createRef<ShiftReduceAlgorithmView>(),
			graphViewRef    : createRef<ParserGraphView>()
		});
	}

	private setGrammarInput(input: string) {
		this.setState({
			grammarInput: input
		});
	}

	private setGrammar(grammar: Grammar) {
		this.setState({
			grammar: grammar
		});
	}

	private setAlgorithms(algorithms: AlgorithmModel) {
		this.setState({
			algorithms : algorithms
		});
	}

	private setGraph(graph: ParserGraphModel) {
		this.setState({
			graph: graph
		});
	}

	private setSRAlgorithm(algorithm: ShiftReduceModel) {
		this.setState({
			shiftReduceAlgorithm: algorithm
		});
	}

	private showHelp() {
		console.log("Show help");
		this.setState({ showHelp : true });
	}

	private hideHelp() {
		console.log("Hide help");
		this.setState({ showHelp : false });
	}

	private setParserAlgorithm(value: ParserAlgorithm) {
		this.setState({ selectedParserAlgorithm : value });
	}

	// Unused at the moment because it does not work.
	private enterSimpleGrammar = () => {
		this.setState({ grammarInput : "S -> A B\nA -> a\nB -> b" });
		this.forceUpdate();
	}

	private parserGraphColours = GraphColours.LIGHT;

	override render() {
		return (
			<div style={{overflowX: "hidden"}}>
			<div id="all_content" style={{marginLeft: '3%', marginRight: '3%'}}>
				<h1 ref={this.state.headerRef} className="header_light">LR Parsing visualized
					<button
						className="help_button"
						disabled={this.state.showHelp}
						onClick={() => this.showHelp()}
					>?</button>
					<button
						className="parser_algorithm_buttons"
						disabled={this.state.selectedParserAlgorithm === ParserAlgorithm.SLR || this.state.grammar !== null}
						onClick={() => this.setParserAlgorithm(ParserAlgorithm.SLR)}
						style={(this.state.selectedParserAlgorithm === ParserAlgorithm.SLR && {color: this.parserGraphColours.lineText}) || {}}
					>
						SLR(1)
					</button>
					<button
						className="parser_algorithm_buttons"
						disabled={this.state.selectedParserAlgorithm === ParserAlgorithm.LR1 || this.state.grammar !== null}
						onClick={() => this.setParserAlgorithm(ParserAlgorithm.LR1)}
						style={(this.state.selectedParserAlgorithm === ParserAlgorithm.LR1 && {color: this.parserGraphColours.lineText}) || {}}
					>
						LR(1)
					</button>
					<button
						className="feedback_button"
						onClick={() => window.open("https://docs.google.com/forms/d/e/1FAIpQLSf3Y_kqdjio1ft3FL0BZpItx3VhmJPlBwDvZoC_xaexzHX2Sg/viewform?usp=sf_link")}
					>
						Feedback
					</button>
					<button
						className="feedback_button"
						onClick={() => {
							this.props.onBackToToolSelection();
							window.location.reload();
						}} // TODO: There should be a better solution.
					>
						Back to tool selection
					</button>
				</h1>
				<Popup
					open={this.state.showHelp}
					closeOnDocumentClick
					onClose={() => this.hideHelp()}
					modal
					position="top center"
					contentStyle={{ background: "#FCFAED", borderRadius: "5px", border: "2px solid black" }}
				>
					{(close: () => void) => (
						<div className="modal">
							<div className="header">Help</div>
							<div className="content">
								This tool can be used to get a better understanding of the algorithms used in LR parsing for context free grammars.<br/>
								The results of the first, follow and collection functions are listed in tables.<br/>
								To strengthen the understanding of the parser algorithm, the corresponding parser table and state machine are also created.<br/>
								Finally, there are the logs of the shift reduce algorithm and a 'step' key.<br/>
								The parser table and state machine are updated when the shift reduce algorithm does an action.<br/><br/>
								To start, just enter a context free grammar.<br/>
								The rules can be defined like this:<br/>
								1. LHS -&#62;&nbsp; s1 s2 s3 | s4 s5 | s6 s7 s8 s9 ...<br/>
								2. LHS ::= s1 s2 s3 | s4 s5 | s6 s7 s8 s9 ...<br/>
								3. LHS :&nbsp;&nbsp; s1 s2 s3 | s4 s5 | s6 s7 s8 s9 ...<br/><br/>
								Examples: (copy and paste)<br/>
								S -&#62; A B<br/>
								A -&#62; a A | epsilon<br/>
								B -&#62; b B | b<br/><br/>
								S &nbsp;&nbsp;&nbsp;-&#62; exp<br/>
								exp &nbsp;-&#62; exp cmp exp1 | exp1<br/>
								exp1 -&#62; exp1 pm exp2 | exp2<br/>
								exp2 -&#62; exp2 md exp3 | exp3<br/>
								exp3 -&#62; - exp4 | exp4<br/>
								exp4 -&#62; ( exp ) | x | 0<br/>
								cmp &nbsp;-&#62; = | # | &#62; | &#8804; | &#8805; | &#62;<br/>
								pm &nbsp;&nbsp;-&#62; + | -<br/>
								md &nbsp;&nbsp;-&#62; * | /<br/>
							</div>
							<div className="actions">
								{ /* TODO: Does not work. */ noop(this.enterSimpleGrammar) }
								{/* <Popup
									trigger={<button
										onClick={() => { console.log("Open grammars"); }} className="button"
									>Predefined Grammars</button>}
									position="top center"
									closeOnDocumentClick
									contentStyle={{ padding: "0px", border: "none" }}
								>
									<div className="menu">
										<button
											onClick={() => { console.log("Enter simple grammar"); this.enterSimpleGrammar() }}
										>Simple</button>
										<button className="menu-item" onClick={() => { console.log("Enter simple grammar with epsilon"); }}>Simple Epsilon</button>
										<button className="menu-item" onClick={() => { console.log("Enter simple expression grammar"); }}>Simple Expressions</button>
										<button className="menu-item" onClick={() => { console.log("Enter SPL expression grammar"); }}>SPL Expressions</button>
									</div>
								</Popup> */}
								<button className="button" onClick={() => { console.log("Close"); close(); }}>Close help</button>
							</div>
						</div>
					)}
				</Popup>
				<div id="top_content" style={{height: '25vh', marginBottom: '1%'}}>
					<div style={{marginBottom: '10px', width: "45vw", height: "100%", float: "left"}}>
						<div style={{marginBottom: "20px", marginTop: "14px"}}>
							Start symbol: <input ref={this.state.startSymbolRef} type="text" placeholder="S" style={{marginRight: '10px'}} disabled={this.state.grammar !== null} />
							Empty word: <input ref={this.state.emptyWordRef} type="text" placeholder="epsilon" disabled={this.state.grammar !== null} />
						</div>
						<textarea ref={this.state.grammarInputRef} onChange={(e) => this.setGrammarInput(e.target.value)} name="Grammar input" id="grammar_input" className="grammar_input" rows={5} placeholder="Enter grammar here" value={this.state.grammarInput} disabled={this.state.grammar !== null} />
					</div>
					{ (this.state.grammar !== null && ((!(this.state.grammar instanceof Error) && this.state.algorithms !== null) &&
					<div style={{float: "right", height: "21vh", width: "44.5vw"}}>
						<Tabs>
							<Tab label={"Table"}>
								<FirstFollowView grammar={this.state.grammar!} algorithms={this.state.algorithms!} />
							</Tab>
							<Tab label={"Calculation"}>
								<div style={{whiteSpace: "pre-line", height: "21vh", width: "44.5vw", overflow: "scroll"}}>
									{ this.state.algorithms.getFirstLogs() + "\n" + this.state.algorithms.getFollowLogs() }
								</div>
							</Tab>
						</Tabs>
					</div>))
					}
				</div>
				<div id="ok_btn" style={{width: "100vw", marginTop: "20px"}}>
					<button disabled={this.state.grammar !== null} onClick={() => {
						try {
							const startSymbol = this.state.startSymbolRef!.current!.value;
							const emptyWord   = this.state.emptyWordRef!.current!.value;
							const _grammar = Grammar.read(this.state.grammarInput, startSymbol.length !== 0 ? startSymbol : "S", emptyWord.length !== 0 ? emptyWord : "epsilon");

							this.setGrammar(_grammar);
							const _algorithms = new AlgorithmModel(_grammar, this.state.selectedParserAlgorithm);
							this.setAlgorithms(_algorithms);

							const startTime = performance.now();
							_algorithms.calculateFirstSets(); // genFirstSets();
							_algorithms.calculateFollowSets(); // genFollowSets();
							_algorithms.generateCollection();
							const endTime = performance.now();

							console.log(`Calculations took ${endTime - startTime} milliseconds`);

							const _parserStateMachine = new ParserGraphModel(_algorithms.getCollection(), _algorithms.getJumps());
							this.setGraph(_parserStateMachine);
						} catch (e: unknown) {
							if (!(e instanceof InputError)) { throw e; }

							SweetAlert.fire({
								icon  : 'error',
								title : "Input error",
								text  : (e as InputError).get()
							});
						}
					}}>OK</button>
				</div>
				{ (this.state.grammar !== null && this.state.algorithms !== null) &&
					<div>
						<div id="mid_content" style={{width: '100%', marginTop: "1vh"}}>
							<Tabs>
								<Tab label={"Table"}>
									<div style={{float: "left", maxHeight: "50vmin", overflow: "scroll", width: "48%"}}>
										<CollectionView logs={this.state.algorithms.getCollectionLogs()}/>
									</div>
								</Tab>
								<Tab label={"Calculation"}>
									<div style={{float: "left", whiteSpace: "pre-line", width: "48%", maxHeight: "50vmin", overflow: "scroll"}}>{this.state.algorithms.getCalculations()}</div>
								</Tab>
							</Tabs>
							<div style={{width: '48%', float: 'right', maxHeight: "50vmin", overflow: "scroll"}}><ParserTableView ref={this.state.tableRef} grammar={this.state.grammar! as Grammar} algorithms={this.state.algorithms!}/></div>
						</div>
						<div id="word_input" style={{float: 'left', width: '100%', textAlign: 'center', marginTop: '2%'}}>
							<input ref={this.state.wordInputRef} placeholder="Eingabewort" disabled={this.state.shiftReduceAlgorithm !== null} />
							<button disabled={this.state.shiftReduceAlgorithm !== null} onClick={() => {
								const input = this.state.wordInputRef!.current!.value.split(/[ \t]+/);
								const shiftReduceAlgorithm = new ShiftReduceModel(input, this.state.tableRef!.current!, this.state.graph!, this.state.grammar!);
								this.setSRAlgorithm(shiftReduceAlgorithm);
							}}>Start</button>
						</div>
						{ (this.state.shiftReduceAlgorithm !== null) &&
							<div id="step_button" style={{float: "right", width: "48%", marginTop: "5px", marginBottom: "5px", textAlign: "left"}}>
								<button disabled={this.state.shiftReduceFinished} style={{width: "20%"}} onClick={() => {
									try {
										if (this.state.shiftReduceAlgorithm!.nextAction()) {
											this.setState({ shiftReduceFinished : true });
											SweetAlert.fire({
												icon  : 'success',
												title : "Parsed",
												text  : "Input parsed successfully"
											});
										}
									} catch(e: unknown) {
										if (e instanceof ParserError) {
											SweetAlert.fire({
												icon  : 'error',
												title : "Parser Error",
												text  : (e as ParserError).get()
											});
											this.setState({ shiftReduceFinished : true });
											this.state.shiftReduceLogs?.current?.setAction((e as ParserError).get(), true);
										} else {
											throw e;
										}
									}
								}}>Step</button>
								</div>
						}
						<div id="low_content" style={{marginTop: '5%', width: '100%', maxHeight: '500px'}}>
							<div style={{width: '48%', float: 'left'}}>
								{ this.state.graph !== null && <ParserGraphView ref={this.state.graphViewRef} model={this.state.graph!} colours={this.parserGraphColours}/> }
							</div>
							{ (this.state.shiftReduceAlgorithm !== null) &&
								// TODO: Auto scroll
								<div style={{width: '48%', float: 'right', marginBottom: "2%", overflow: "scroll", maxHeight: '500px'}}>
									<ShiftReduceAlgorithmView ref={this.state.shiftReduceLogs} shiftReduceModel={this.state.shiftReduceAlgorithm} />
								</div>
							}
						</div>
					</div>
				}


				<div className="switch_container">
					<label id="label" className="switch toggle">
						<input ref={this.state.toggleRef} id="toggle" type="checkbox" onClick={(_) => {
							if (this.state.toggleRef!.current?.checked) {
								document.body.classList.add("dark");
								this.state.grammarInputRef!.current?.classList.add("dark");
								this.state.headerRef!.current?.classList.add("header_dark");

								const buttons = document.getElementsByTagName("button");
								for (const button of buttons) {
									button.classList.add("dark");
								}

								const inputs = document.getElementsByTagName("input");
								for (const input of inputs) {
									input.classList.add("dark");
								}

								if (this.state.tableRef!.current !== null) {
									this.state.tableRef!.current.setActiveCellColours(darkCellColours);
								}

								this.parserGraphColours = GraphColours.DARK;
								this.state.graphViewRef?.current?.setNodeColours(this.parserGraphColours);
							} else {
								document.body.classList.remove("dark");
								this.state.grammarInputRef!.current?.classList.remove("dark")
								this.state.headerRef!.current?.classList.remove("header_dark");

								const buttons = document.getElementsByTagName("button");
								for (const button of buttons) {
									button.classList.remove("dark");
								}

								const inputs = document.getElementsByTagName("input");
								for (const input of inputs) {
									input.classList.remove("dark");
								}

								if (this.state.tableRef!.current !== null) {
									this.state.tableRef!.current.setActiveCellColours(lightCellColours);
								}

								this.parserGraphColours = GraphColours.LIGHT;
								this.state.graphViewRef?.current?.setNodeColours(this.parserGraphColours);
							}
							this.forceUpdate();
						}} />
						<span className="slider round"></span>
					</label>
				</div>
			</div>
			</div>
		);
	}
};

export default VisLR;
