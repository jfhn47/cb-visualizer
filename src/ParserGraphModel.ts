import { CollectionLR1, CollectionSLR, Jumps } from "./AlgorithmModel";
import { LR0Element } from "./LR0Element";
import { ParserGraphView } from "./ParserGraphView";
import { Comparable, mkString, foldLeft, zip } from "./util";

export class GraphNode implements Comparable<GraphNode> {
	state         : number;
	content       : LR0Element[];
	neighbours    : Map<string, GraphNode>;
	isAcceptState : boolean;

	constructor(state: number, content: LR0Element[], isAcceptState: boolean = false, neighbours: Map<string, GraphNode> = new Map()) {
		this.state         = state;
		this.content       = content;
		this.neighbours    = neighbours;
		this.isAcceptState = isAcceptState;
	}

	addNeighbour(symbol: string, neighbour: GraphNode) {
		this.neighbours.set(symbol, neighbour);
	}

	transitWith(symbol: string): GraphNode | undefined {
		return this.neighbours.get(symbol);
	}

	getName = (): string => "I" + this.state;

	formatContent(): string {
		return mkString(this.content, el => el.toString() + '\n', 1);
	}

	// NOTE: The name is not important.
	equals(other: GraphNode): boolean {
		return zip(this.content, other.content).filter(([l, r]) => !l.equals(r)).length === 0;
	}

	toString(): string {
		const neighboursAsString = foldLeft((acc: string, [symbol, neighbour]: [string, GraphNode]) => acc +  "I" + this.state + " --" + symbol + "-> I" + neighbour.state + "\n")("")(zip(Array.from(this.neighbours.keys()), Array.from(this.neighbours.values())));
		return "I" + this.state + ": {\ncontent: { " + this.formatContent() + " }\nneighbours (" + this.neighbours.size + "): { " + neighboursAsString + " }\n}";
	}
}

export class ParserGraphModel {
	currentState : GraphNode;
	graphNodes   : GraphNode[] = [];
	graphView    : ParserGraphView | undefined = undefined;

	constructor(configSet: CollectionSLR | CollectionLR1, jumps: Jumps) {
		for (let i = 0; i < configSet.length; ++i) {
			// REVIEW: Is I1 always the accept state?
			this.graphNodes.push(new GraphNode(i, configSet[i].toArray(), i === 1));
		}
		this.currentState = this.graphNodes[0];

		for (const [[stateOfOccurrence, symbol], newState] of jumps) {
			this.graphNodes[stateOfOccurrence].addNeighbour(symbol, this.graphNodes[newState]);
		}
	}

	setNewState(newState: number) {
		const nextState = this.graphNodes.find((node) => node.state === newState);
		if (nextState === undefined) {
			throw Error("Next state in graph is undefined");
		}
		this.currentState = nextState;
		this.graphView?.updateGraph(nextState.state);
	}

	setView(view: ParserGraphView) {
		this.graphView = view;
	}
}
