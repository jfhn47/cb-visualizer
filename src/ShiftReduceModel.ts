import { Grammar } from "./Grammar";
import { ParserGraphModel } from "./ParserGraphModel";
import { JumpTableEntry, ParserTableModel, ReduceTableEntry, ShiftTableEntry, TableEntry } from "./ParserTableModel";
import { ParserTableView } from "./ParserTableView";
import ShiftReduceView from "./ShiftReduceView";
import { Stack, range, mkString } from "./util";

export class ShiftReduceModel {
	input     : string[];
	tableView : ParserTableView;
	tableData : ParserTableModel;
	graphData : ParserGraphModel;
	// graphView : ParserGraphView | null = null;
	view      : ShiftReduceView | null = null;
	grammar   : Grammar;

	stack  : Stack<string> = new Stack();
	states : Stack<number> = new Stack([0]);

	private forceJump : string | null = null;

	constructor(input: string[], table: ParserTableView, graph: ParserGraphModel, grammar: Grammar) {
		this.input     = input.length === 1 && input[0] === '' ? [] : input;
		this.tableView = table;
		this.tableData = table.state.table!;
		this.graphData = graph;
		// this.graphView = graphView;
		this.grammar   = grammar;

		this.tableView.setActiveCell(this.currentState(), this.currentSymbol());
	}

	setView(view: ShiftReduceView) {
		this.view = view;
		this.updateViews();
	}

	nextAction(): boolean {
		// console.log("TAB(" + this.currentState() + ", " + this.currentSymbol() + ")");
		const currentEntry = this.getEntry();
		// console.log(currentEntry.type);

		// NOTE: The type of every table entry becomes 'Object' at runtime.
		//       That means, a variable that defines the type is necessary.

		if (currentEntry.type === "ReduceTableEntry" && (currentEntry as ReduceTableEntry).isAcceptState) {
			this.stack.pop();
			this.stack.push("S'");
			this.states.pop();
			this.updateViews("Reduce S' -> " + this.grammar.startSymbol);
			this.view?.setAction("accept", true);
			return true;
		}

		if (currentEntry.type === "ShiftTableEntry") {
			const e = currentEntry as ShiftTableEntry;

			// console.log("Shift " + this.currentSymbol());
			// console.log("this.view = " + this.view);
			this.stack.push(this.currentSymbol());
			this.input.shift();
			this.states.push(e.nextState);
			this.graphData.setNewState(e.nextState);
			this.tableView.setActiveCell(this.currentState(), this.currentSymbol());

			this.updateViews("Shift " + e.nextState);
		} else if (currentEntry.type === "JumpTableEntry") {
			const e = currentEntry as JumpTableEntry;
			// console.log("Jump " + e.nextState);
			this.states.push(e.nextState);
			this.graphData.setNewState(e.nextState);
			this.tableView.setActiveCell(this.currentState(), this.currentSymbol());
			this.forceJump = null;

			this.updateViews("Jump " + e.nextState);
		} else if (currentEntry.type === "ReduceTableEntry") {
			const e = currentEntry as ReduceTableEntry;
			// console.log(e.reduction.toString());
			const times = e.reduction.rhs.filter(sym => sym !== this.grammar.emptyWord).length;
			range(0, times).forEach(_ => { this.stack.pop(); this.states.pop(); });
			this.stack.push(e.reduction.lhs);
			this.graphData.setNewState(this.currentState());

			this.updateViews(e.reduction.formatForLogs());

			this.tableView.setActiveCell(this.currentState(), e.reduction.lhs);
			this.forceJump = e.reduction.lhs;
		} else {
			const neighbours = Array.from(this.graphData.currentState.neighbours.keys());
			if (neighbours.length === 0) {
				throw new ParserError("Unexpected symbol '" + this.currentSymbol() + "'");
			}
			const expectedSymbols = mkString(neighbours.filter(this.grammar.isTerminal), k => "'" + k + "', ");
			throw new ParserError("Expected " + expectedSymbols + " but got '" + this.currentSymbol() + "'");
		}

		return false;
	}

	private currentState = (): number => this.states.top();

	private currentSymbol = (): string => this.input.length > 0 ? this.input[0] : "$";

	private getEntry(): TableEntry {
		if (this.forceJump !== null) {
			return this.tableData.getEntry(this.currentState(), this.forceJump!);
		} else {
			return this.tableData.getEntry(this.currentState(), this.currentSymbol());
		}
	}

	private updateViews(action: string = "") {
		this.view?.newRow();
		this.view?.setStack(mkString(this.stack, _ => _ + " ", 1));
		this.view?.setStates(mkString(this.states, _ => _ + " ", 1));
		const newInput = mkString(this.input, _ => _ + " ", 1);
		this.view?.setInput(newInput.length === 0 ? "$" : newInput);
		this.view?.setAction(action);
	}
}

export class ParserError extends Error {
	private msg: string;

	constructor(msg: string) {
		super(msg);
		this.msg = msg;

		Object.setPrototypeOf(this, ParserError.prototype);
	}

	get = (): string => this.msg;
}
