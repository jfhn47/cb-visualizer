import { LR0CollectionEntry } from "./AlgorithmModel";
import { Comparable, assert, mkString } from "./util";

export const compareMaps = (expected: Map<string, Set<string>>, actual: Map<string, Set<string>>): boolean => {
	let result = true;

	expected.forEach((v, k) => {
		const res = actual.get(k);
		assert(res !== undefined);
		const diff = Array.from(v).filter(_ => !res!.has(_)).length;
		if (diff > 0) { result = false; }
	});

	actual.forEach((v, k) => {
		const exp = expected.get(k);
		assert(exp !== undefined);
		const diff = Array.from(v).filter(_ => !exp!.has(_)).length;
		if (diff > 0) { result = false; }
	});

	return result;
}

export const compareCollections = (expected: LR0CollectionEntry[], actual: LR0CollectionEntry[]): number => {
	let diff = 0;

	const sizeDiff = Math.abs(expected.length - actual.length);
	if (sizeDiff > 0) { return sizeDiff; }

	actual.forEach(resultSet => {
		for (const expectedSet of expected) {
			if (resultSet.equals(expectedSet)) { return; }
		}
		console.log("Diff in " + mkString(resultSet, _ => _ + ", "));
		++diff;
	});

	expected.forEach(expectedSet => {
		for (const resultSet of actual) {
			if (expectedSet.equals(resultSet)) { return; }
		}
		console.log("Diff in " + mkString(expectedSet, _ => _ + ", "));
		++diff;
	});

	return diff;
}

export const compareArrays = <T extends Comparable<T>>(expected: T[], actual: T[]): number => {
	let diff = 0;

	actual.forEach(av => {
		for (const ev of expected) {
			if (av.equals(ev)) { return; }
		}
		console.log("Diff in " + mkString(actual, _ => _ + ", "));
		++diff;
	});

	expected.forEach(ev => {
		for (const av of actual) {
			if (ev.equals(av)) { return; }
		}
		console.log("Diff in " + mkString(expected, _ => _ + ", "));
		++diff;
	});

	return diff;
}
