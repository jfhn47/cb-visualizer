import React, { ReactElement, ReactNode, useState } from "react";
import "./Tabs.css";

// Everything from: https://codepen.io/piotr-modes/pen/ErqdxE
export const Tabs: React.FC = ({ children }) => {
	const [activeTab, setActiveTab] = useState<string>((children![0].props as TabProps).label);

	const changeTab = (tab: string) => {
		setActiveTab(tab);
	}

	let content: React.ReactNode;
	let buttons: string[] = [];

	return (
		<div>
			{React.Children.map(children, _child => {
				const childProps = (_child as ReactElement<TabProps>);
				buttons.push(childProps.props.label);
				if (childProps.props.label === activeTab) {
					content = childProps.props.children;
				}
			})}

			<TabButtons activeTab={activeTab} buttons={buttons} changeTab={changeTab}/>
			<div className="tab-content">{content}</div>
		</div>
	);
}

export const TabButtons = ({ buttons, changeTab, activeTab }) => {
	return (
		<div className="tab-buttons">
			{buttons.map(button => {
				return <button className={button === activeTab ? "tab-button active-tab" : "tab-button"} onClick={() => changeTab(button)}>{button}</button>
			})}
		</div>
	);
}

interface TabProps {
	label    : string;
	children : ReactNode;
}

export const Tab: React.FC<TabProps> = ({ children }) => {
	return (
		<React.Fragment>
			{children}
		</React.Fragment>
	);
}
