import React from "react";
import { AlgorithmModel } from "./AlgorithmModel";
import { Grammar } from "./Grammar";
import { mkString } from "./util";

interface Props {
	grammar    : Grammar;
	algorithms : AlgorithmModel;
}

export class FirstFollowView extends React.Component<Props> {
	override render() {
		return (
			<div style={{whiteSpace: "pre-line", width: "45vw", height: "21vh", textAlign: "center", overflow: "scroll"}}>
				<table style={{width: "100%"}}>
					<tr>
						<th>Symbol</th>
						<th>First</th>
						<th>Follow</th>
					</tr>
					<tbody>
						{
							// REVIEW: @speed This is not very efficient.
							this.props.grammar.getNonTerminalsAsArray().map(nt => {
								const firstSet  = Array.from(this.props.algorithms.getFirstSets().get(nt)!);
								const followSet = Array.from(this.props.algorithms.getFollowSets().get(nt)!);
								return <tr>
									<td>{nt}</td>
									<td>{mkString(firstSet, _ => _ + ", ")}</td>
									<td>{mkString(followSet, _ => _ + ", ")}</td>
								</tr>;
							})
						}
					</tbody>
				</table>
			</div>
		)
	}
}
