import { AlgorithmModel } from "../src/AlgorithmModel";
import { Grammar } from "../src/Grammar";
import { compareMaps } from "../src/testUtilFunctions"

describe("Tests for first set function", () => {
	it("Simple test for first sets", () => {
		/*
		 * S -> A B
		 * A -> a B | epsilon
		 * B -> b c
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S", "A", "B", "a", "b", "c", "epsilon"]);
		grammar.nonTerminals = new Set(["S", "A", "B"]);
		grammar.terminals    = new Set(["a", "b", "c", "epsilon"]);
		grammar.rules        = new Map([
			["S", [["A", "B"]]],
			["A", [["a", "B"], ["epsilon"]]],
			["B", [["b", "c"]]]
		]);

		/*
		 * First(S) := (First(A) u First(B)) \ { epsilon } = { a, b }
		 * First(A) := { a, epsilon }
		 * First(B) := { b }
		 */
		const expected = new Map<string, Set<string>>([
			["S", new Set(["a", "b"])],
			["A", new Set(["a", "epsilon"])],
			["B", new Set(["b"])]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Medium test for first sets", () => {
		/*
		 * S -> A B C
		 * A -> B a | epsilon x | b
		 * B -> b | y
		 * C -> c a A b
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S", "A", "B", "C", "a", "epsilon", "x", "b", "y", "c"]);
		grammar.nonTerminals = new Set(["S", "A", "B", "C"]);
		grammar.terminals    = new Set(["a", "epsilon", "x", "b", "y", "c"]);
		grammar.rules        = new Map([
			["S", [["A", "B", "C"]]],
			["A", [["B", "a"], ["epsilon", "x"], ["b"]]],
			["B", [["b"], ["y"]]],
			["C", [["c", "a", "A", "b"]]]
		]);

		/*
		 * First(S) := (First(A) u First(B)) \ { epsilon } = { b, y }
		 * First(A) := First(B) u { epsilon } u { b } = { b, y, epsilon }
		 * First(B) := { b, y }
		 * First(C) := { c }
		 */
		const expected = new Map([
			["S", new Set(["b", "y"])],
			["A", new Set(["b", "y", "epsilon"])],
			["B", new Set(["b", "y"])],
			["C", new Set(["c"])]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);

		algorithms.calculateFirstSets();

		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Recursion test for first sets 1", () => {
		/*
		 * S -> S a | a
		 */

		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S", "a"]);
		grammar.nonTerminals = new Set("S");
		grammar.terminals    = new Set("a");
		grammar.rules        = new Map([
			["S", [["S", "a"], ["a"]]]
		]);

		/*
		 * First(S) := First(S) u { a } = { a }
		 */
		const expected = new Map([
			["S", new Set("a")],
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Recursion test for first sets 2", () => {
		/*
		 * S -> A
		 * A -> A
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S", "A"]);
		grammar.nonTerminals = new Set(["S", "A"]);
		grammar.terminals    = new Set();
		grammar.rules        = new Map([
			["S", [["A"]]],
			["A", [["A"]]]
		]);

		const expected = new Map([
			["S", new Set<string>()],
			["A", new Set<string>()]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Recursion test for first sets 3", () => {
		/*
		 * S -> A
		 * A -> B
		 * B -> A | b
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S", "A", "B", "b"]);
		grammar.nonTerminals = new Set(["S", "A", "B"]);
		grammar.terminals    = new Set("b");
		grammar.rules        = new Map([
			["S", [["A"]]],
			["A", [["B"]]],
			["B", [["A"], ["b"]]]
		]);

		const expected = new Map([
			["S", new Set("b")],
			["A", new Set("b")],
			["B", new Set("b")]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("List test for first sets", () => {
		/*
		 * S    -> List
		 * List -> List Pair | Pair
		 * Pair -> ( Pair ) | ( )
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S", "List", "Pair", "(", ")"]);
		grammar.nonTerminals = new Set(["S", "List", "Pair"]);
		grammar.terminals    = new Set(["(", ")"]);
		grammar.rules        = new Map([
			["S", [["List"]]],
			["List", [["List", "Pair"], ["Pair"]]],
			["Pair", [["(", "Pair", ")"], ["(", ")"]]]
		]);

		/*
		 * First(S)    := First(List) = { '(' }
		 * First(List) := First(List) u First(Pair) = { '(' }
		 * First(Pair) := { '(' }
		 */
		const expected = new Map([
			["S", new Set("(")],
			["List", new Set("(")],
			["Pair", new Set("(")]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Recursion test for first sets 4", () => {
		/*
		 * S -> X | b
		 * X -> a | Y
		 * Y -> S
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S", "X", "Y", "a", "b"]);
		grammar.nonTerminals = new Set(["S", "X", "Y"]);
		grammar.terminals    = new Set(["a", "b"]);
		grammar.rules        = new Map([
			["S", [["X"], ["b"]]],
			["X", [["a"], ["Y"]]],
			["Y", [["S"]]],
		]);

		/*
		 * First(S) := First(X) u First(b) = {a} u {b} = {a, b}
		 * First(X) := First(a) u First(Y) = {a} u First(S) = {a, b}
		 * First(Y) := First(S) = {a, b}
		 */
		const expected = new Map([
			["S", new Set<string>(["a", "b"])],
			["X", new Set<string>(["a", "b"])],
			["Y", new Set<string>(["a", "b"])]
		]);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Recursion test for first sets 5", () => {
		/*
		 * S -> A
		 * A -> S
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S", "A"]);
		grammar.nonTerminals = new Set(["S", "A"]);
		grammar.terminals    = new Set();
		grammar.rules        = new Map([
			["S", [["A"]]],
			["A", [["S"]]]
		]);

		const expected = new Map([
			["S", new Set<string>()],
			["A", new Set<string>()]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Mathematical Expression test", () => {
		/*
		 * E -> E + T | T
		 * T -> T * F | F
		 * F -> - A | A
		 * A -> ( E ) | x
		 */
		let grammar = new Grammar("E");
		grammar.symbols = new Set(["E", "T", "F", "A", "+", "*", "-", "(", ")", "x"]);
		grammar.nonTerminals = new Set(["E", "T", "F", "A"]);
		grammar.terminals = new Set(["+", "*", "-", "(", ")", "x"]);
		grammar.rules = new Map([
			["E", [["E", "+", "T"], ["T"]]],
			["T", [["T", "*", "F"], ["F"]]],
			["F", [["-", "A"], ["A"]]],
			["A", [["(", "E", ")"], ["x"]]]
		]);

		const expected = new Map([
			["E", new Set(["-", "(", "x"])],
			["T", new Set(["-", "(", "x"])],
			["F", new Set(["-", "(", "x"])],
			["A", new Set(["(", "x"])],
		]);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		const result = algorithms.getFirstSets().filterByKey(key => grammar.isNonTerminal(key));
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});
});
