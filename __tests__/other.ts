import "jest"
import { Grammar } from "../src/Grammar";
import { LR0Element } from "../src/LR0Element";
import { Reduction } from "../src/Reduction";

describe("Tests for the LR0Element class", () => {
	it("Tests the equals() method", () => {
		const grammar = new Grammar();

		const e1 = new LR0Element(grammar, "S'", ["S"]);
		const e2 = new LR0Element(grammar, "S'", ["S"]);

		expect(e1.equals(e2)).toBeTruthy();

		const e3 = new LR0Element(grammar, "A", ["a"]);

		expect(e1.equals(e3)).toBeFalsy();

		const e4 = new LR0Element(grammar, "S'", ["S", "A"]);

		expect(e1.equals(e4)).toBeFalsy();
	});
});

describe("Tests for the Reduction class", () => {
	it("Tests the equals() method", () => {
		const red1 = new Reduction(0, "S'", ["S"]);
		const red2 = new Reduction(0, "S'", ["S"]);

		expect(red1.equals(red2)).toBeTruthy();

		const red3 = new Reduction(0, "A", ["a"]);

		expect(red1.equals(red3)).toBeFalsy();

		const red4= new Reduction(0, "S'", ["S", "A"]);

		expect(red1.equals(red4)).toBeFalsy();
	});
});
