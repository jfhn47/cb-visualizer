import { AlgorithmModel, LR0CollectionEntry } from "../src/AlgorithmModel";
import { Grammar } from "../src/Grammar";
import { LR0Element } from "../src/LR0Element";
import { Reduction } from "../src/Reduction";
import { compareArrays, compareCollections } from "../src/testUtilFunctions"

// NOTE: Closure = Hülle
describe("Tests for the SLR closure construction", () => {
	it("Simple test for SLR closures", () => {
		/*
		 * S' -> S
		 * S  -> A B
		 * A  -> a
		 * B  -> b
		 */
		let grammar = new Grammar();
		grammar.symbols = new Set(["S'", "S", "A", "B", "a", "b"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B"]);
		grammar.terminals = new Set(["a", "b"]);
		grammar.rules = new Map([
			["S'", [["S"]]],
			["S",  [["A", "B"]]],
			["A",  [["a"]]],
			["B",  [["b"]]]
		]);

		/*
		 * C  = { Closure({ S' -> .S }) }
		 * I0 = Closure({ S' -> .S }) = { S' -> .S, S -> .A B, A -> .a }
		 * I1 = Jump(I0, S) = Closure({ S' -> S. })  = { S' -> S. } // accept
		 * I2 = Jump(I0, a) = Closure({ A -> a. })   = { A -> a. } // reduce
		 * I3 = Jump(I0, A) = Closure({ S -> A .B }) = { S -> A .B, B -> .b }
		 * I4 = Jump(I2, b) = Closure({ B -> b. })   = { B -> b. } // reduce
		 * I5 = Jump(I2, B) = Closure({ S -> A B. }) = { S -> A B. } // reduce
		 *
		 * Reductions: { (I1, S' -> S), (I2, A -> a), (I4, B -> b), (I5, S -> A B) }
		 */
		const expected = [
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"], 0), new LR0Element(grammar, "S", ["A", "B"], 0), new LR0Element(grammar, "A", ["a"], 0)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "A", ["a"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S", ["A", "B"], 1), new LR0Element(grammar, "B", ["b"], 0)]),
			new LR0CollectionEntry([new LR0Element(grammar, "B", ["b"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S", ["A", "B"], 2)])
		];
		const expectedReductions = [
			new Reduction(1, "S'", ["S"]),
			new Reduction(2, "A", ["a"]),
			new Reduction(4, "B", ["b"]),
			new Reduction(5, "S", ["A", "B"])
		];

		// TODO: Test jumps for equality.

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		algorithms.genCollectionSLR();

		const result = algorithms.getCollectionSLR();
		const reductions = algorithms.getReductions();

		expect(compareCollections(expected, result)).toStrictEqual(0);
		expect(compareArrays(expectedReductions, reductions)).toStrictEqual(0);
	});

	it("Medium test for closure construction", () => {
		/*
		 * S' -> S
		 * S  -> A B
		 * A  -> a
		 * B  -> b B
		 * B  -> epsilon
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A", "B", "a", "b", "epsilon"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B"]);
		grammar.terminals    = new Set(["a", "b", "epsilon"]);
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S",  [["A", "B"]]],
			["A",  [["a"]]],
			["B",  [["b", "B"], ["epsilon"]]]
		]);

		/*
		 * C = { Closure({ S' -> .S }) }
		 * I0 = Closure({ S' -> .S }) = { S' -> .S, S -> .A B, A -> .a }
		 * I1 = Jump(I0, S) = Closure({ S' -> S. }) = { S' -> S. } // reduce
		 * I2 = Jump(I0, A) = Closure({ S -> A .B }) = { S -> A .B, B -> .b B, B -> . } // reduce
		 * I3 = Jump(I0, a) = Closure({ A -> a. }) = { A -> a. } // reduce
		 * I4 = Jump(I2, B) = Closure({ S -> A B. }) = { S -> A B. } // reduce
		 * I5 = Jump(I2, b) = Closure({ B -> b .B }) = { B -> b .B, B -> .b B, B -> . } // reduce
		 * I6 = Jump(I5, B) = Closure({ B -> b B. }) = { B -> b B. } // reduce
		 * Jump(I5, b) = I5
		 *
		 * Reductions: { (I1, S' -> S), (I2, B -> epsilon), (I3, A -> a), (I4, S -> A B), (I5, B -> epsilon), (I6, B -> b B) }
		 */
		const expected = [
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"], 0), new LR0Element(grammar, "S", ["A", "B"], 0), new LR0Element(grammar, "A", ["a"], 0)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S", ["A", "B"], 1), new LR0Element(grammar, "B", ["b", "B"], 0), new LR0Element(grammar, "B", ["epsilon"], 0)]),
			new LR0CollectionEntry([new LR0Element(grammar, "A", ["a"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S", ["A", "B"], 2)]),
			new LR0CollectionEntry([new LR0Element(grammar, "B", ["b", "B"], 1), new LR0Element(grammar, "B", ["b", "B"], 0), new LR0Element(grammar, "B", ["epsilon"], 0)]),
			new LR0CollectionEntry([new LR0Element(grammar, "B", ["b", "B"], 2)])
		];
		const expectedReductions = [
			new Reduction(1, "S'", ["S"]),
			new Reduction(2, "B", ["epsilon"]),
			new Reduction(3, "A", ["a"]),
			new Reduction(4, "S", ["A", "B"]),
			new Reduction(5, "B", ["epsilon"]),
			new Reduction(6, "B", ["b", "B"]),
		];

		// TODO: Test jumps for equality.

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		algorithms.genCollectionSLR();

		const result = algorithms.getCollectionSLR();
		const reductions = algorithms.getReductions();

		expect(compareCollections(expected, result)).toStrictEqual(0);
		expect(compareArrays(expectedReductions, reductions)).toStrictEqual(0);
	});

	it("Recursion test for closure construction", () => {
		/*
		 * S' -> S
		 * S  -> A
		 * A  -> B A | B
		 * B  -> a
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A", "B", "a"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B"]);
		grammar.terminals    = new Set(["a"]);
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S",  [["A"]]],
			["A",  [["B", "A"], ["B"]]],
			["B",  [["a"]]]
		]);

		/*
		 * C = { Closure({ S' -> .S }) }
		 * I0 = Closure({ S' -> .S }) = { S' -> .S, S -> .A, A -> .B A, A -> .B, B -> .a }
		 * I1 = Jump(I0, S) = Closure({ S' -> S. }) = { S' -> S. } // reduce
		 * I2 = Jump(I0, A) = Closure({ S -> A. }) = { S -> A. } // reduce
		 * I3 = Jump(I0, B) = Closure({ A -> B .A, A -> B. }) = { A -> B .A, A -> B., A -> .B A, A -> .B, B -> .a } // reduce
		 * I4 = Jump(I0, a) = Closure({ B -> a. }) = { B -> a. } // reduce
		 * I5 = Jump(I3, A) = Closure({ A -> B A. }) = { A -> B A. } // reduce
		 */
		const expectedConfig = [
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"]), new LR0Element(grammar, "S", ["A"]), new LR0Element(grammar, "A", ["B", "A"]), new LR0Element(grammar, "A", ["B"]), new LR0Element(grammar, "B", ["a"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S", ["A"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "A", ["B", "A"], 1), new LR0Element(grammar, "A", ["B"], 1), new LR0Element(grammar, "A", ["B", "A"]), new LR0Element(grammar, "A", ["B"]), new LR0Element(grammar, "B", ["a"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "B", ["a"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "A", ["B", "A"], 2)]),
		];

		const expectedReductions = [
			new Reduction(1, "S'", ["S"]),
			new Reduction(2, "S", ["A"]),
			new Reduction(3, "A", ["B"]),
			new Reduction(4, "B", ["a"]),
			new Reduction(5, "A", ["B", "A"]),
		];

		// TODO: Test jumps for equality.

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		algorithms.genCollectionSLR();

		const resultConfig     = algorithms.getCollectionSLR();
		const resultReductions = algorithms.getReductions();

		expect(compareCollections(expectedConfig, resultConfig)).toStrictEqual(0);
		expect(compareArrays(expectedReductions, resultReductions)).toStrictEqual(0);
	});

	it("Test for arithmetic expressions", () => {
		/*
		 * S' -> S
		 * S  -> E
		 * E  -> E + T | T
		 * T  -> T * F | F
		 * F  -> ( E ) | x
		 */

		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "E", "+", "T", "*", "F", "(", ")", "x"]);
		grammar.nonTerminals = new Set(["S'", "S", "E", "T", "F"]);
		grammar.terminals    = new Set(["+", "*", "x", "(", ")"]);
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S",  [["E"]]],
			["E",  [["E", "+", "T"], ["T"]]],
			["T",  [["T", "*", "F"], ["F"]]],
			["F",  [["(", "E", ")"], ["x"]]]
		]);

		/*
		 * I0 = Hülle({S' -> .S}) = { S' -> .S, S -> .E, E -> .E + T, E -> .T, T -> .T * F, T -> .F, F -> .( E ), F -> .x }
		 * I1 = Sprung(0, 'S') = Hülle({ S' -> S. }) = { S' -> S. } // Reduce
		 * I2 = Sprung(0, 'E') = Hülle({ S -> E., E -> E .+ T }) = { S -> E., E -> E .+ T } // Reduce
		 * I3 = Sprung(0, 'T') = Hülle({ E -> T., T -> T .* F }) = { E -> T., T -> T .* F } // Reduce
		 * I4 = Sprung(0, 'F') = Hülle({ T -> F. }) = { T -> F. } // Reduce
		 * I5 = Sprung(0, '(') = Hülle({ F -> ( .E ) }) = { F -> ( .E ), E -> .E + T, E -> .T, T -> .T * F, T -> .F, F -> .( E ), F -> .x }
		 * I6 = Sprung(0, 'x') = Hülle({ F -> x. }) = { F -> x. } // Reduce
		 * I7 = Sprung(2, '+') = Hülle({ E -> E + .T }) = { E -> E + .T, T -> .T * F, T -> .F, F -> .( E ), F -> .x }
		 * I8 = Sprung(3, '*') = Hülle({ T -> T * .F }) = { T -> T * .F, F -> .( E ), F -> .x }
		 * I9 = Sprung(5, 'E') = Hülle({ F -> ( E .), E -> E .+ T }) = { F -> ( E .), E -> E .+ T }
		 * Sprung(5, 'T') = I3
		 * Sprung(5, 'F') = I4
		 * Sprung(5, '(') = I5
		 * Sprung(5, 'x') = I6
		 * I10 = Sprung(7, 'T') = Hülle({ E -> E + T., T -> T .* F }) = { E -> E + T., T -> T .* F } // Reduce
		 * Sprung(7, 'F') = I4
		 * Sprung(7, '(') = I5
		 * Sprung(7, 'x') = I6
		 * I11 = Sprung(8, 'F') = Hülle({ T -> T * F. }) = { T -> T * F. } // Reduce
		 * Sprung(8, '(') = I5
		 * Sprung(8, 'x') = I6
		 * I12 = Sprung(9, ')') = Hülle({ F -> ( E ). }) = { F -> ( E ). } // Reduce
		 * Sprung(9, '+') = I7
		 * Sprung(10, '*') = I8
		 */
		const expectedCollection = [
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"]), new LR0Element(grammar, "S", ["E"]), new LR0Element(grammar, "E", ["E", "+", "T"]), new LR0Element(grammar, "E", ["T"]), new LR0Element(grammar, "T", ["T", "*", "F"]), new LR0Element(grammar, "T", ["F"]), new LR0Element(grammar, "F", ["(", "E", ")"]), new LR0Element(grammar, "F", ["x"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S", ["E"], 1), new LR0Element(grammar, "E", ["E", "+", "T"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "E", ["T"], 1), new LR0Element(grammar, "T", ["T", "*", "F"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "T", ["F"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "F", ["(", "E", ")"], 1), new LR0Element(grammar, "E", ["E", "+", "T"]), new LR0Element(grammar, "E", ["T"]), new LR0Element(grammar, "T", ["T", "*", "F"]), new LR0Element(grammar, "T", ["F"]), new LR0Element(grammar, "F", ["(", "E", ")"]), new LR0Element(grammar, "F", ["x"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "F", ["x"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "E", ["E", "+", "T"], 2), new LR0Element(grammar, "T", ["T", "*", "F"]), new LR0Element(grammar, "T", ["F"]), new LR0Element(grammar, "F", ["(", "E", ")"]), new LR0Element(grammar, "F", ["x"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "T", ["T", "*", "F"], 2), new LR0Element(grammar, "F", ["(", "E", ")"]), new LR0Element(grammar, "F", ["x"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "F", ["(", "E", ")"], 2), new LR0Element(grammar, "E", ["E", "+", "T"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "E", ["E", "+", "T"], 3), new LR0Element(grammar, "T", ["T", "*", "F"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "T", ["T", "*", "F"], 3)]),
			new LR0CollectionEntry([new LR0Element(grammar, "F", ["(", "E", ")"], 3)])
		];

		const expectedReductions = [
			new Reduction(1,  "S'", ["S"]),
			new Reduction(2,  "S",  ["E"]),
			new Reduction(3,  "E",  ["T"]),
			new Reduction(4,  "T",  ["F"]),
			new Reduction(6,  "F",  ["x"]),
			new Reduction(10, "E",  ["E", "+", "T"]),
			new Reduction(11, "T",  ["T", "*", "F"]),
			new Reduction(12, "F",  ["(", "E", ")"]),
		];

		// TODO: Test jumps for equality.

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		algorithms.genCollectionSLR();

		const resultCollection = algorithms.getCollectionSLR();
		const resultReductions = algorithms.getReductions();

		expect(compareCollections(expectedCollection, resultCollection)).toStrictEqual(0);
		expect(compareArrays(expectedReductions, resultReductions)).toStrictEqual(0);
	});

	it("SPL expressions", () => {
		/*
		 * S'   -> S
		 * S    -> exp
		 * exp  -> exp cmp exp1 | exp1
		 * exp1 -> exp1 pm exp2 | exp2
		 * exp2 -> exp2 md exp3 | exp3
		 * exp3 -> - exp4 | exp4
		 * exp4 -> ( exp ) | x | 0
		 * cmp  -> = | # | < | <= | >= | >
		 * pm   -> + | -
		 * md   -> * | /
		 */
		const grammar = Grammar.read(`S'   -> S
		S    -> exp
		exp  -> exp cmp exp1 | exp1
		exp1 -> exp1 pm exp2 | exp2
		exp2 -> exp2 md exp3 | exp3
		exp3 -> - exp4 | exp4
		exp4 -> ( exp ) | x | 0
		cmp  -> = | # | < | <= | >= | >
		pm   -> + | -
		md   -> * | /
		`);

		// NOTE: This should be folded if the editor supports it.
		/*
		 * I0 = Closure({ S' -> .S }) = { S' -> .S, S -> .exp, exp -> .exp cmp exp1, exp -> .exp1, exp1 -> .exp1 pm exp2, exp1 -> .exp2, exp2 -> .exp2 md exp3, exp2 -> .exp3, exp3 -> .- exp4, exp3 -> .exp4, exp4 -> .( exp ), exp4 -> .x, exp4 -> .0 }
		 * I1 = Jump(0, S) = Closure({ S' -> S. }) = { S' -> S. }
		 * Reduce: S' -> S
		 * I2 = Jump(0, exp) = Closure({ S -> exp., exp -> exp .cmp exp1 }) = { S -> exp., exp -> exp .cmp exp1, cmp -> .=, cmp -> .#, cmp -> .<, cmp -> .<=, cmp -> .>=, cmp -> .> }
		 * Reduce: S -> exp
		 * I3 = Jump(0, exp1) = Closure({ exp -> exp1., exp1 -> exp1 .pm exp2 }) = { exp -> exp1., exp1 -> exp1 .pm exp2, pm -> .+, pm -> .- }
		 * Reduce: exp -> exp1
		 * I4 = Jump(0, exp2) = Closure({ exp1 -> exp2., exp2 -> exp2 .md exp3 }) = { exp1 -> exp2., exp2 -> exp2 .md exp3, md -> .*, md -> ./ }
		 * Reduce: exp1 -> exp2
		 * I5 = Jump(0, exp3) = Closure({ exp2 -> exp3. }) = { exp2 -> exp3. }
		 * Reduce: exp2 -> exp3
		 * I6 = Jump(0, -) = Closure({ exp3 -> - .exp4 }) = { exp3 -> - .exp4, exp4 -> .( exp ), exp4 -> .x, exp4 -> .0 }
		 * I7 = Jump(0, exp4) = Closure({ exp3 -> exp4. }) = { exp3 -> exp4. }
		 * Reduce: exp3 -> exp4
		 * I8 = Jump(0, '(') = Closure({ exp4 -> ( .exp ) }) = { exp4 -> ( .exp ), exp -> .exp cmp exp1, exp -> .exp1, exp1 -> .exp1 pm exp2, exp1 -> .exp2, exp2 -> .exp2 md exp3, exp2 -> .exp3, exp3 -> .- exp4, exp3 -> .exp4, exp4 -> .( exp ), exp4 -> .x, exp4 -> .0 }
		 * I9 = Jump(0, x) = Closure({ exp4 -> x. }) = { exp4 -> x. }
		 * Reduce: exp4 -> x
		 * I10 = Jump(0, '0') = Closure({ exp4 -> 0. }) = { exp4 -> 0. }
		 * Reduce: exp4 -> 0
		 * I11 = Jump(2, cmp) = Closure({ exp -> exp cmp .exp1 }) = { exp -> exp cmp .exp1, exp1 -> .exp1 pm exp2, exp1 -> .exp2, exp2 -> .exp2 md exp3, exp2 -> .exp3, exp3 -> .- exp4, exp3 -> .exp4, exp4 -> .( exp ), exp4 -> .x, exp4 -> .0 }
		 * I12 = Jump(2, =) = Closure({ cmp -> =. }) = { cmp -> =. }
		 * Reduce: cmp -> =
		 * I13 = Jump(2, '#') = Closure({ cmp -> #. }) = { cmp -> #. }
		 * Reduce: cmp -> #.
		 * I14 = Jump(2, '<') = Closure({ cmp -> <. }) = { cmp -> <. }
		 * Reduce: cmp -> <.
		 * I15 = Jump(2, '<=') = Closure({ cmp -> <=. }) = { cmp -> <=. }
		 * Reduce: cmp -> <=.
		 * I16 = Jump(2, '>=') = Closure({ cmp -> >=. }) = { cmp -> >=. }
		 * Reduce: cmp -> >=.
		 * I17 = Jump(2, '>') = Closure({ cmp -> >. }) = { cmp -> >. }
		 * Reduce: cmp -> >.
		 * I18 = Jump(3, pm) = Closure({ exp1 -> exp1 pm .exp2 }) = { exp1 -> exp1 pm .exp2, exp2 -> .exp2 md exp3, exp2 -> .exp3, exp3 -> .- exp4, exp3 -> .exp4, exp4 -> .( exp ), exp4 -> .x, exp4 -> .0 }
		 * I19 = Jump(3, '+') = Closure({ pm -> +. }) = { pm -> +. }
		 * Reduce: pm -> +.
		 * I20 = Jump(3, '-') = Closure({ pm -> -. }) = { pm -> -. }
		 * Reduce: pm -> -.
		 * I21 = Jump(4, 'md') = Closure({ exp2 -> exp2 md .exp3 }) = { exp2 -> exp2 md .exp3, exp3 -> .- exp4, exp3 -> .exp4, exp4 -> .( exp ), exp4 -> .x, exp4 -> .0 }
		 * I22 = Jump(4, '*') = Closure({ md -> *. }) = { md -> *. }
		 * Reduce: md -> *.
		 * I23 = Jump(4, '/') = Closure({ md -> /. }) = { md -> /. }
		 * Reduce: md -> /.
		 * I24 = Jump(6, 'exp4') = Closure({ exp3 -> - exp4. }) = { exp3 -> - exp4. }
		 * Reduce: exp3 -> - exp4.
		 * Jump(6, '(') = I8
		 * Jump(6, 'x') = I9
		 * Jump(6, '0') = I10
		 * I25 = Jump(8, 'exp') = Closure({ exp4 -> ( exp .), exp -> exp .cmp exp1 }) = { exp4 -> ( exp .), exp -> exp .cmp exp1, cmp -> .=, cmp -> .#, cmp -> .<, cmp -> .<=, cmp -> .>=, cmp -> .> }
		 * Jump(8, 'exp1') = I3
		 * Jump(8, 'exp2') = I4
		 * Jump(8, 'exp3') = I5
		 * Jump(8, '-') = I6
		 * Jump(8, 'exp4') = I7
		 * Jump(8, '(') = I8
		 * Jump(8, 'x') = I9
		 * Jump(8, '0') = I10
		 * I26 = Jump(11, 'exp1') = Closure({ exp -> exp cmp exp1., exp1 -> exp1 .pm exp2 }) = { exp -> exp cmp exp1., exp1 -> exp1 .pm exp2, pm -> .+, pm -> .- }
		 * Reduce: exp -> exp cmp exp1.
		 * Jump(11, 'exp2') = I4
		 * Jump(11, 'exp3') = I5
		 * Jump(11, '-') = I6
		 * Jump(11, 'exp4') = I7
		 * Jump(11, '(') = I8
		 * Jump(11, 'x') = I9
		 * Jump(11, '0') = I10
		 * I27 = Jump(18, 'exp2') = Closure({ exp1 -> exp1 pm exp2., exp2 -> exp2 .md exp3 }) = { exp1 -> exp1 pm exp2., exp2 -> exp2 .md exp3, md -> .*, md -> ./ }
		 * Reduce: exp1 -> exp1 pm exp2.
		 * Jump(18, 'exp3') = I5
		 * Jump(18, '-') = I6
		 * Jump(18, 'exp4') = I7
		 * Jump(18, '(') = I8
		 * Jump(18, 'x') = I9
		 * Jump(18, '0') = I10
		 * I28 = Jump(21, 'exp3') = Closure({ exp2 -> exp2 md exp3. }) = { exp2 -> exp2 md exp3. }
		 * Reduce: exp2 -> exp2 md exp3.
		 * Jump(21, '-') = I6
		 * Jump(21, 'exp4') = I7
		 * Jump(21, '(') = I8
		 * Jump(21, 'x') = I9
		 * Jump(21, '0') = I10
		 * I29 = Jump(25, ')') = Closure({ exp4 -> ( exp ). }) = { exp4 -> ( exp ). }
		 * Reduce: exp4 -> ( exp ).
		 * Jump(25, 'cmp') = I11
		 * Jump(25, '=') = I12
		 * Jump(25, '#') = I13
		 * Jump(25, '<') = I14
		 * Jump(25, '<=') = I15
		 * Jump(25, '>=') = I16
		 * Jump(25, '>') = I17
		 * Jump(26, 'pm') = I18
		 * Jump(26, '+') = I19
		 * Jump(26, '-') = I20
		 * Jump(27, 'md') = I21
		 * Jump(27, '*') = I22
		 * Jump(27, '/') = I23
		 */

		// NOTE: This should be folded if the editor supports it.
		const expectedCollection = [
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"]), new LR0Element(grammar, "S", ["exp"]), new LR0Element(grammar, "exp", ["exp", "cmp", "exp1"]), new LR0Element(grammar, "exp", ["exp1"]), new LR0Element(grammar, "exp1", ["exp1", "pm", "exp2"]), new LR0Element(grammar, "exp1", ["exp2"]), new LR0Element(grammar, "exp2", ["exp2", "md", "exp3"]), new LR0Element(grammar, "exp2", ["exp3"]), new LR0Element(grammar, "exp3", ["-", "exp4"]), new LR0Element(grammar, "exp3", ["exp4"]), new LR0Element(grammar, "exp4", ["(", "exp", ")"]), new LR0Element(grammar, "exp4", ["x"]), new LR0Element(grammar, "exp4", ["0"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "S", ["exp"], 1), new LR0Element(grammar, "exp", ["exp", "cmp", "exp1"], 1), new LR0Element(grammar, "cmp", ["="]), new LR0Element(grammar, "cmp", ["#"]), new LR0Element(grammar, "cmp", ["<"]), new LR0Element(grammar, "cmp", ["<="]), new LR0Element(grammar, "cmp", [">="]), new LR0Element(grammar, "cmp", [">"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp", ["exp1"], 1), new LR0Element(grammar, "exp1", ["exp1", "pm", "exp2"], 1), new LR0Element(grammar, "pm", ["+"]), new LR0Element(grammar, "pm", ["-"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp1", ["exp2"], 1), new LR0Element(grammar, "exp2", ["exp2", "md", "exp3"], 1), new LR0Element(grammar, "md", ["*"]), new LR0Element(grammar, "md", ["/"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp2", ["exp3"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp3", ["-", "exp4"], 1), new LR0Element(grammar, "exp4", ["(", "exp", ")"]), new LR0Element(grammar, "exp4", ["x"]), new LR0Element(grammar, "exp4", ["0"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp3", ["exp4"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp4", ["(", "exp", ")"], 1), new LR0Element(grammar, "exp", ["exp", "cmp", "exp1"]), new LR0Element(grammar, "exp", ["exp1"]), new LR0Element(grammar, "exp1", ["exp1", "pm", "exp2"]), new LR0Element(grammar, "exp1", ["exp2"]), new LR0Element(grammar, "exp2", ["exp2", "md", "exp3"]), new LR0Element(grammar, "exp2", ["exp3"]), new LR0Element(grammar, "exp3", ["-", "exp4"]), new LR0Element(grammar, "exp3", ["exp4"]), new LR0Element(grammar, "exp4", ["(", "exp", ")"]), new LR0Element(grammar, "exp4", ["x"]), new LR0Element(grammar, "exp4", ["0"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp4", ["x"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp4", ["0"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp", ["exp", "cmp", "exp1"], 2), new LR0Element(grammar, "exp1", ["exp1", "pm", "exp2"]), new LR0Element(grammar, "exp1", ["exp2"]), new LR0Element(grammar, "exp2", ["exp2", "md", "exp3"]), new LR0Element(grammar, "exp2", ["exp3"]), new LR0Element(grammar, "exp3", ["-", "exp4"]), new LR0Element(grammar, "exp3", ["exp4"]), new LR0Element(grammar, "exp4", ["(", "exp", ")"]), new LR0Element(grammar, "exp4", ["x"]), new LR0Element(grammar, "exp4", ["0"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "cmp", ["="], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "cmp", ["#"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "cmp", ["<"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "cmp", ["<="], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "cmp", [">="], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "cmp", [">"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp1", ["exp1", "pm", "exp2"], 2), new LR0Element(grammar, "exp2", ["exp2", "md", "exp3"]), new LR0Element(grammar, "exp2", ["exp3"]), new LR0Element(grammar, "exp3", ["-", "exp4"]), new LR0Element(grammar, "exp3", ["exp4"]), new LR0Element(grammar, "exp4", ["(", "exp", ")"]), new LR0Element(grammar, "exp4", ["x"]), new LR0Element(grammar, "exp4", ["0"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "pm", ["+"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "pm", ["-"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp2", ["exp2", "md", "exp3"], 2), new LR0Element(grammar, "exp3", ["-", "exp4"]), new LR0Element(grammar, "exp3", ["exp4"]), new LR0Element(grammar, "exp4", ["(", "exp", ")"]), new LR0Element(grammar, "exp4", ["x"]), new LR0Element(grammar, "exp4", ["0"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "md", ["*"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "md", ["/"], 1)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp3", ["-", "exp4"], 2)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp4", ["(", "exp", ")"], 2), new LR0Element(grammar, "exp", ["exp", "cmp", "exp1"], 1), new LR0Element(grammar, "cmp", ["="]), new LR0Element(grammar, "cmp", ["#"]), new LR0Element(grammar, "cmp", ["<"]), new LR0Element(grammar, "cmp", ["<="]), new LR0Element(grammar, "cmp", [">="]), new LR0Element(grammar, "cmp", [">"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp", ["exp", "cmp", "exp1"], 3), new LR0Element(grammar, "exp1", ["exp1", "pm", "exp2"], 1), new LR0Element(grammar, "pm", ["+"]), new LR0Element(grammar, "pm", ["-"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp1", ["exp1", "pm", "exp2"], 3), new LR0Element(grammar, "exp2", ["exp2", "md", "exp3"], 1), new LR0Element(grammar, "md", ["*"]), new LR0Element(grammar, "md", ["/"])]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp2", ["exp2", "md", "exp3"], 3)]),
			new LR0CollectionEntry([new LR0Element(grammar, "exp4", ["(", "exp", ")"], 3)])
		];

		const expectedReductions = [
			new Reduction(1, "S'", ["S"]),
			new Reduction(2, "S", ["exp"]),
			new Reduction(3, "exp", ["exp1"]),
			new Reduction(4, "exp1", ["exp2"]),
			new Reduction(5, "exp2", ["exp3"]),
			new Reduction(7, "exp3", ["exp4"]),
			new Reduction(9, "exp4", ["x"]),
			new Reduction(10, "exp4", ["0"]),
			new Reduction(12, "cmp", ["="]),
			new Reduction(13, "cmp", ["#"]),
			new Reduction(14, "cmp", ["<"]),
			new Reduction(15, "cmp", ["<="]),
			new Reduction(16, "cmp", [">="]),
			new Reduction(17, "cmp", [">"]),
			new Reduction(19, "pm", ["+"]),
			new Reduction(20, "pm", ["-"]),
			new Reduction(22, "md", ["*"]),
			new Reduction(23, "md", ["/"]),
			new Reduction(24, "exp3", ["-", "exp4"]),
			new Reduction(26, "exp", ["exp", "cmp", "exp1"]),
			new Reduction(27, "exp1", ["exp1", "pm", "exp2"]),
			new Reduction(28, "exp2", ["exp2", "md", "exp3"]),
			new Reduction(29, "exp4", ["(", "exp", ")"]),
		];

		// TODO: Test jumps for equality.

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		algorithms.genCollectionSLR();

		const resultCollection = algorithms.getCollectionSLR();
		const resultReductions = algorithms.getReductions();

		expect(compareCollections(expectedCollection, resultCollection)).toStrictEqual(0);
		expect(compareArrays(expectedReductions, resultReductions)).toStrictEqual(0);
	});

	/*
	 * S -> R S | R
	 * R -> a b T
	 * T -> a T | c | epsilon
	 */

	// TODO: Add tests that should fail to prove, that not everything is correct.
});
