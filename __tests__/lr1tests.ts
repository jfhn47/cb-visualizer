import { AlgorithmModel, ParserAlgorithm } from "../src/AlgorithmModel";
import { Grammar } from "../src/Grammar";

describe("LR(1) tests", () => {
	it("", () => {
		const grammar = Grammar.read(`
		S -> z A x | A y | a x
		A -> a
		`);

		const algorithms = new AlgorithmModel(grammar, ParserAlgorithm.LR1);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		algorithms.generateCollection();
	});
});
