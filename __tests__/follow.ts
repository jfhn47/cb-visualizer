import { AlgorithmModel } from "../src/AlgorithmModel";
import { Grammar } from "../src/Grammar";
import { compareMaps } from "../src/testUtilFunctions"

describe("Test for follow sets", () => {
	it("Simple test for follow sets", () => {
		/*
		 * S -> A B
		 * A -> a
		 * B -> b B b
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A", "B", "a", "b"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B"]);
		grammar.terminals    = new Set(["a", "b"]);
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S", [["A", "B"]]],
			["A", [["a"]]],
			["B", [["b", "B", "b"]]]
		]);

		/*
		 * Follow(S) = { $ }
		 * Follow(A) = First(B) = { b }
		 * Follow(B) = Follow(S) u First(b) = { $, b }
		 */
		const expected = new Map<string, Set<string>>([
			["S'", new Set("$")],
			["S", new Set("$")],
			["A", new Set("b")],
			["B", new Set(["$", "b"])]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const result = algorithms.getFollowSets();
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Medium test for follow sets", () => {
		/*
		 * S -> A B C
		 * A -> a B
		 * B -> a C
		 * C -> c a A b
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A", "B", "C", "a", "b", "c"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B", "C"]);
		grammar.terminals    = new Set(["a", "b", "c"]);
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S", [["A", "B", "C"]]],
			["A", [["a", "B"]]],
			["B", [["a", "C"]]],
			["C", [["c", "a", "A", "b"]]]
		]);

		/*
		 * Follow(S) = { $ }
		 * Follow(A) = First(B) u First(b) = { a, b }
		 * Follow(B) = First(C) u Follow(A) = { a, b, c }
		 * Follow(C) = Follow(S) u Follow(B) = { $, a, b, c }
		 */
		const expected = new Map<string, Set<string>>([
			["S'", new Set("$")],
			["S", new Set("$")],
			["A", new Set(["a", "b"])],
			["B", new Set(["a", "b", "c"])],
			["C", new Set(["$", "a", "b", "c"])]
		]);
		console.log(expected);

		// grammar.nonTerminals.forEach(nt => algorithms.follow(nt));
		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const result = algorithms.getFollowSets();
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Recursion test for follow sets", () => {
		/*
		 * S’ -> S
		 * S  -> A B
		 * A  -> a
		 * B  -> b B
		 * B  -> epsilon
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S'", "S", "A", "B", "a", "b", "epsilon"]);
		grammar.nonTerminals = new Set(["S'", "S'", "S", "A", "B"]);
		grammar.terminals    = new Set(["a", "b", "epsilon"]);
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S",  [["A", "B"]]],
			["A",  [["a"]]],
			["B",  [["b", "B"], ["epsilon"]]]
		]);

		const expected = new Map<string, Set<string>>([
			["S'", new Set("$")],
			["S",  new Set("$")],
			["A",  new Set(["b", "$"])],
			["B",  new Set("$")]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const result = algorithms.getFollowSets();
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Recursion test for follow sets 1", () => {
		/*
		 * S -> A
		 * A -> A
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A"]);
		grammar.nonTerminals = new Set(["S'", "S", "A"]);
		grammar.terminals    = new Set();
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S", [["A"]]],
			["A", [["A"]]]
		]);

		const expected = new Map([
			["S'", new Set("$")],
			["S", new Set("$")],
			["A", new Set("$")]
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const result = algorithms.getFollowSets();
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("?", () => {
		/*
		 * S -> A
		 * A -> B B B
		 * B -> b
		 */
		expect(true).toBeTruthy();
	});

	it("Recursion test for follow sets 2", () => {
		/*
		 * S -> A
		 * A -> B
		 * B -> A | b
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A", "B", "b"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B"]);
		grammar.terminals    = new Set("b");
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S", [["A"]]],
			["A", [["B"]]],
			["B", [["A"], ["b"]]]
		]);

		const expected = new Map([
			["S'", new Set("$")],
			["S", new Set("$")],
			["A", new Set("$")],
			["B", new Set("$")],
		]);
		console.log(expected);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const result = algorithms.getFollowSets();
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});

	it("Mathematical Expression test", () => {
		/*
		 * E -> E + T | T
		 * T -> T * F | F
		 * F -> - A | A
		 * A -> ( E ) | x
		 */
		let grammar = new Grammar("E");
		grammar.symbols = new Set(["E", "T", "F", "A", "+", "*", "-", "(", ")", "x"]);
		grammar.nonTerminals = new Set(["E", "T", "F", "A"]);
		grammar.terminals = new Set(["+", "*", "-", "(", ")", "x"]);
		grammar.rules = new Map([
			["E", [["E", "+", "T"], ["T"]]],
			["T", [["T", "*", "F"], ["F"]]],
			["F", [["-", "A"], ["A"]]],
			["A", [["(", "E", ")"], ["x"]]]
		]);

		const expected = new Map([
			["E'", new Set(["$"])],
			["E", new Set(["+", ")"])],
			["T", new Set(["*", "+", ")"])],
			["F", new Set(["*", "+", ")"])],
			["A", new Set(["*", "+", ")"])]
		]);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const result = algorithms.getFollowSets();
		console.log(result);

		expect(compareMaps(expected, result)).toBeTruthy();
	});
});
