import {parseInputGrammar} from '../../src/vis-tree/InputParser';

describe("Tests for the input parser of the gamma tree.", () => {
	it("Test parser", () => {
		console.log(parseInputGrammar("( abc , def, (*, -, *), xyz) -> (q1, push(A, l)) \n\n\n\n\n ( abc , def, (*, -, *), xyz) -> (q1, push(A, l))"));
		console.log(parseInputGrammar(`( abc , def, (*, -, *), xyz) -> (q1, push(A, l))
			( abc , def, (*, -, *), xyz) -> (q1, push(A, l))
			( abc , def, (*, -, *), xyz) -> (q1, push(A, l))
		`));
	});
});
