import 'jest';
import {Automaton, UnificationError} from '../../src/vis-tree/Automaton';
import {InputModel, MappingLeft, MappingRight, Direction, HasDesc, NodeKind, NodeType} from '../../src/vis-tree/InputModel';
import {exists} from '../../src/util';

describe("Tests for the automaton.", () => {
	const inputModel = new InputModel(["a"], [
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(true), new HasDesc.Literal(false))), new MappingRight("q0", new Direction("s")), 1],
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(false), new HasDesc.Literal(false))), new MappingRight("q0", new Direction("s")), 2],
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Mvar(0), new HasDesc.Literal(false))), new MappingRight("q0", new Direction("s")), 3],
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Mvar(0), new HasDesc.Mvar(0), new HasDesc.Literal(false))), new MappingRight("q0", new Direction("s")), 4],
	]);
	const automaton = new Automaton("q0", ["q0"], inputModel);

	const inputModel2 = new InputModel(["a"], [
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("l"), new HasDesc.Literal(true), new HasDesc.Literal(false))), new MappingRight("q0", new Direction("s")), 1],
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("l"), new HasDesc.Literal(false), new HasDesc.Literal(false))), new MappingRight("q0", new Direction("s")), 2],
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("l"), new HasDesc.Mvar(0), new HasDesc.Mvar(0))), new MappingRight("q0", new Direction("s")), 3],
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Mvar(0), new HasDesc.Mvar(0), new HasDesc.Mvar(0))), new MappingRight("q0", new Direction("s")), 4],
	]);
	const automaton2 = new Automaton("q0", ["q0"], inputModel2);

	const inputModel3 = new InputModel(["a"], [
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("l"), new HasDesc.Literal(true), new HasDesc.Literal(false))), new MappingRight("q0", new Direction("s")), 1],
		[new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("l"), new HasDesc.Literal(false), new HasDesc.Literal(false))), new MappingRight("q0", new Direction("s")), 2],
	]);
	const automaton3 = new Automaton("q0", ["q0"], inputModel3);

	it("Tests the `getNextRule` function", () => {
		// 2nd rule.
		const expectedLeft = new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(false), new HasDesc.Literal(false)));
		const rule = automaton.testCall_getNextRule("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(false), new HasDesc.Literal(false)));
		expect(rule && rule[0].equals(expectedLeft)).toBeTruthy();
		if (exists(rule)) {
			console.log(`${rule![0].toString()}, ${rule![1].toString()}`);
		}

		// 4th rule.
		const expectedLeft2 = new MappingLeft("q0", "a", new NodeType(new NodeKind.Mvar(0), new HasDesc.Mvar(0), new HasDesc.Mvar(0)));
		const rule2 = automaton2.testCall_getNextRule("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(false), new HasDesc.Literal(false)));
		expect(rule && rule2![0].equals(expectedLeft2)).toBeTruthy();
		if (exists(rule2)) {
			console.log(`${rule2![0].toString()}, ${rule2![1].toString()}`);
		}

		// No matching rule, expect exception.
		try {
			automaton3.testCall_getNextRule("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(false), new HasDesc.Literal(false)));
		} catch (e: unknown) {
			if (e instanceof UnificationError) {
				console.log(e.msg);
			} else {
				expect(false).toBeTruthy();
			}
		}
	});

	it("Tests the `unifyNodeKind` function with a simple example", () => {
		const mvar = new NodeKind.Mvar(0);
		console.log(mvar);
		const lit  = new NodeKind.Literal("-");
		console.log(lit);
		automaton.testCall_unifyNodeKind(mvar, lit);
		console.log(mvar);
	});

	it("Tests the `unifyNodeKind` function with a simple mismatch", () => {
		const lit1 = new NodeKind.Literal("l");
		console.log(lit1);
		const lit2 = new NodeKind.Literal("-");
		console.log(lit2);
		try {
			automaton.testCall_unifyNodeKind(lit1, lit2);
			console.log(lit1);
			expect(false).toBeTruthy();
		} catch (e: unknown) {
			if (!(e instanceof UnificationError)) {
				expect(false).toBeTruthy();
			}
			console.log((e as UnificationError).msg);
		}
	});
});
