import 'jest';
import {Action, Direction, InputModel, MappingLeft, MappingRight, NodeKind, NodeType, PushInstruction} from "../../src/vis-tree/InputModel";

describe("Tests for the input model of the gamma tree.", () => {
	it("Dummy test", () => {
		const model = new InputModel();
		model.alphabet.add("abc");
		model.alphabet.add("def");
		model.alphabet.add("ghi");
		model.alphabet.add("abc");

		model.mappings.set(
			new MappingLeft(
				"q0", ["ab", "cde", model.endMarker],
				new NodeType(NodeKind.ROOT, false, false),
				"l1"),
			new MappingRight("q1", new Action(Direction.STAY, new PushInstruction("l2", Direction.RIGHT))));

		console.log(model.toString());

		expect(true).toBeTruthy();
	})
});
