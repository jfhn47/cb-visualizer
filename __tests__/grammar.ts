import "jest"
import {Grammar, InputError, testCall_parseTokens, testCall_tokenize, Token, TokenType} from "../src/Grammar"
import {compareArrays} from "../src/testUtilFunctions";

describe("Tokenizer", () => {
	it("Format test for tokenizer", () => {
		const input = `
		E -> E + T
		   | T
		T -> T * F
		   | F
		F -> ( E )
		   | x
		`;

		const expected = [
			new Token(TokenType.Symbol, "E"),
			new Token(TokenType.Arrow, "->"),
			new Token(TokenType.Symbol, "E"),
			new Token(TokenType.Symbol, "+"),
			new Token(TokenType.Symbol, "T"),
			new Token(TokenType.Separator, "|"),
			new Token(TokenType.Symbol, "T"),

			new Token(TokenType.Symbol, "T"),
			new Token(TokenType.Arrow, "->"),
			new Token(TokenType.Symbol, "T"),
			new Token(TokenType.Symbol, "*"),
			new Token(TokenType.Symbol, "F"),
			new Token(TokenType.Separator, "|"),
			new Token(TokenType.Symbol, "F"),

			new Token(TokenType.Symbol, "F"),
			new Token(TokenType.Arrow, "->"),
			new Token(TokenType.Symbol, "("),
			new Token(TokenType.Symbol, "E"),
			new Token(TokenType.Symbol, ")"),
			new Token(TokenType.Separator, "|"),
			new Token(TokenType.Symbol, "x"),
		];

		const result = testCall_tokenize(input);

		expect(compareArrays(expected, result)).toStrictEqual(0);
	});

	it("Almost reserved tokens check", () => {
		const input = `S -> - A A ::= - > a`;

		const expected = [
			new Token(TokenType.Symbol, "S"),
			new Token(TokenType.Arrow, "->"),
			new Token(TokenType.Symbol, "-"),
			new Token(TokenType.Symbol, "A"),
			new Token(TokenType.Symbol, "A"),
			new Token(TokenType.Arrow, "::="),
			new Token(TokenType.Symbol, "-"),
			new Token(TokenType.Symbol, ">"),
			new Token(TokenType.Symbol, "a")
		];

		const result = testCall_tokenize(input);

		expect(compareArrays(expected, result)).toStrictEqual(0);
	});
});

describe("Parser", () => {
	it("Invalid input", () => {
		const tokens = [
			new Token(TokenType.Symbol, "S"),
			new Token(TokenType.Arrow, "->"),
			new Token(TokenType.Arrow, "->"),
			new Token(TokenType.Symbol, "A"),
		];

		try {
			testCall_parseTokens(tokens);
			expect(true).toBe(false);
		} catch (e: unknown) {
			expect(e instanceof InputError).toBe(true);
		}
	});

	it("Invalid input 2", () => {
		const tokens = [
			new Token(TokenType.Symbol, "S"),
			new Token(TokenType.Arrow, "->"),
			new Token(TokenType.Symbol, "A"),
			new Token(TokenType.Separator, "->"),
			new Token(TokenType.Separator, "->"),
			new Token(TokenType.Symbol, "B"),
		];

		try {
			testCall_parseTokens(tokens);
			expect(true).toBe(false);
		} catch (e: unknown) {
			expect(e instanceof InputError).toBe(true);
		}
	});
});

describe("Checker", () => {
	it("Invalid input", () => {
		try {
			Grammar.read("S -> x A -> a");
			expect(true).toBe(false);
		} catch (e: unknown) {
			expect(e instanceof InputError).toBe(true);
		}
	});
});

describe("grammar", () => {
	it("Should throw an error because the input is empty", () => {
		const input = "";
		try {
			Grammar.read(input);
			expect(true).toBe(false);
		} catch (e: unknown) {
			expect(e instanceof InputError).toBe(true);
		}
	});

	it("Should throw an error because the input has a wrong rule format", () => {
		const input = "S";
		try {
			Grammar.read(input);
			expect(true).toBe(false);
		} catch (e: unknown) {
			expect(e instanceof InputError).toBe(true);
		}
	});

	it("Should throw an error because it does not have any terminal symbols", () => {
		const input = "S -> A\nA -> S";
		try {
			Grammar.read(input);
			expect(true).toBe(false);
		} catch (e: unknown) {
			expect(e instanceof InputError).toBe(true);
		}
	});

	// NOTE: This works only for not extended grammars.
	it("Should throw an error because the start symbol 'S' is not defined", () => {
		const input = "A -> a";
		try {
			Grammar.read(input);
			expect(true).toBe(false);
		} catch (e: unknown) {
			expect(e instanceof InputError).toBe(true);
		}
	});

	it("Should throw an error because the start symbol 'S' is not a non terminal", () => {
		const input = "A -> S";
		try {
			Grammar.read(input);
			expect(true).toBe(false);
		} catch (e: unknown) {
			expect(e instanceof InputError).toBe(true);
		}
	});

	it("Should be a valid input", () => {
		const input = "S => a b";

		let expected = new Grammar();
		expected.symbols = new Set(["S'", "S", "a", "b"]);
		expected.nonTerminals = new Set(["S'", "S"]);
		expected.terminals = new Set(["a", "b"]);
		expected.rules = new Map([
			["S'", [["S"]]],
			["S", [["a", "b"]]]
		]);

		const result = Grammar.read(input);
		expect(result.toString()).toEqual(expected.toString());
	});

	it("Should be a valid input", () => {
		const input = "S -> A A -> a";

		let expected = new Grammar();
		expected.symbols = new Set(["S'", "S", "A", "a"]);
		expected.nonTerminals = new Set(["S'", "S", "A"]);
		expected.terminals = new Set("a");
		expected.rules = new Map([
			["S'", [["S"]]],
			["S", [["A"]]],
			["A", [["a"]]]
		]);

		const result = Grammar.read(input);
		expect(result.toString()).toEqual(expected.toString());
	});

	it("Should be a valid input", () => {
		const input = "S -> A B\nA -> a A | aa\nA -> B\nB -> b";

		let expected = new Grammar();
		expected.symbols = new Set(["S'", "S", "A", "B", "a", "aa", "b"]);
		expected.nonTerminals = new Set(["S'", "S", "A", "B"]);
		expected.terminals = new Set(["a", "aa", "b"]);
		expected.rules = new Map([
			["S'", [["S"]]],
			["S", [["A", "B"]]],
			["A", [["a", "A"], ["aa"], ["B"]]],
			["B", [["b"]]]
		]);

		const result = Grammar.read(input);
		expect(result.toString()).toEqual(expected.toString());
	});

	// NOTE: This test exists to be sure that `toEqual` really works.
	it("Should be a valid input but the expected should not be equal to the actual result", () => {
		const input = "S -> A\nA -> a B\nB -> b";

		const expected = new Grammar();
		expected.symbols = new Set(["S'", "S", "A", "B", "a", "b"]);
		expected.nonTerminals = new Set(["S'", "S", "A", "B"]);
		expected.terminals = new Set(["a", "b", "c"]);
		expected.rules = new Map([
			["S'", [["S"]]],
			["S", [["A", "b"]]],
			["A", [["a", "B"]]],
			["B", [["B"]]]
		]);

		const result = Grammar.read(input);

		let counter = 0;
		counter += Array.from(expected.symbols).filter(_ => !result.symbols.has(_)).length;
		counter += Array.from(result.symbols).filter(_ => !expected.symbols.has(_)).length;

		counter += Array.from(expected.nonTerminals).filter(_ => !result.nonTerminals.has(_)).length;
		counter += Array.from(result.nonTerminals).filter(_ => !expected.nonTerminals.has(_)).length;

		counter += Array.from(expected.terminals).filter(_ => !result.terminals.has(_)).length;
		counter += Array.from(result.terminals).filter(_ => !expected.terminals.has(_)).length;

		counter += Math.abs(expected.rules.size - result.rules.size);

		for (const [lhs, rhs] of expected.rules) {
			if (result.rules.has(lhs)) {
				let otherRhs = result.rules.get(lhs);

				const rhsFlattened = rhs.flat(1);
				const otherRhsFlattened = otherRhs!.flat(1);

				counter += rhsFlattened.filter(_ => otherRhsFlattened!.indexOf(_) < 0).length;
				// counter += otherRhsFlattened!.filter(_ => rhsFlattened.indexOf(_) < 0).length;
			} else {
				++counter;
			}
		}

		expect(counter).toBeGreaterThan(0);
	});

	it("Should be a valid input", () => {
		const input = "S ::= A B\nA ::= a A | aa\nA ::= B\nB ::= b";

		let expected = new Grammar();
		expected.symbols = new Set(["S'", "S", "A", "B", "a", "aa", "b"]);
		expected.nonTerminals = new Set(["S'", "S", "A", "B"]);
		expected.terminals = new Set(["a", "aa", "b"]);
		expected.rules = new Map([
			["S'", [["S"]]],
			["S", [["A", "B"]]],
			["A", [["a", "A"], ["aa"], ["B"]]],
			["B", [["b"]]]
		]);

		const result = Grammar.read(input);
		expect(result.toString()).toEqual(expected.toString());
	});

	it("Should be a valid input", () => {
		const input = "S : A B\nA : a A | aa\nA : B\nB : b";

		let expected = new Grammar();
		expected.symbols = new Set(["S'", "S", "A", "B", "a", "aa", "b"]);
		expected.nonTerminals = new Set(["S'", "S", "A", "B"]);
		expected.terminals = new Set(["a", "aa", "b"]);
		expected.rules = new Map([
			["S'", [["S"]]],
			["S", [["A", "B"]]],
			["A", [["a", "A"], ["aa"], ["B"]]],
			["B", [["b"]]]
		]);

		const result = Grammar.read(input);
		expect(result.toString()).toEqual(expected.toString());
	});
});

export {}
