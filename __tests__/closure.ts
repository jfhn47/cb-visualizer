import "jest";
import { AlgorithmModel, LR0CollectionEntry, ParserAlgorithm } from "../src/AlgorithmModel";
import { Grammar } from "../src/Grammar";
import { LR0Element, LR1Element } from "../src/LR0Element";
import { ComparableSet, mkString, strEqualsFunc } from "../src/util";
import { compareArrays } from "../src/testUtilFunctions";

describe("Tests for the closure function", () => {
	it("Recursion test", () => {
		/*
		 * S' -> S
		 * S  -> A
		 * A  -> A
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A"]);
		grammar.nonTerminals = new Set(["S'", "S", "A"]);
		grammar.terminals    = new Set();
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S", [["A"]]],
			["A", [["A"]]]
		]);

		/*
		 * 0 = Closure({ S' -> .S }) = { S' -> .S, S -> .A, A -> .A }
		 * 1 = Jump(0, S) = Closure({ S' -> S. }) = { S' -> S. }
		 * 2 = Jump(0, A) = Closure({ S -> A., A -> A. }) = { S -> A., A -> A. }
		 */
		const expected = new LR0CollectionEntry([
			new LR0Element(grammar, "S'", ["S"]),
			new LR0Element(grammar, "S", ["A"]),
			new LR0Element(grammar, "A", ["A"]),
		]);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const start = new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"])]);
		const result = algorithms.testCall_closure(start);

		console.log(mkString(result));

		expect(expected.equals(result)).toBeTruthy();
	});

	it("Recursion test 2", () => {
		/*
		 * S' -> S
		 * S  -> A B
		 * A  -> a
		 * B  -> b B
		 * B  -> epsilon
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A", "B", "a", "b", "epsilon"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B"]);
		grammar.terminals    = new Set(["a", "b", "epsilon"]);
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S",  [["A", "B"]]],
			["A",  [["a"]]],
			["B",  [["b", "B"], ["epsilon"]]]
		]);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const inputElement = new LR0Element(grammar, "B", ["b", "B"], 1);

		/*
		 * Closure({ B -> b .B }) = { B -> b .B, B -> .b B, B -> . }
		 */
		const expected = new LR0CollectionEntry([new LR0Element(grammar, "B", ["b", "B"], 1), new LR0Element(grammar, "B", ["b", "B"]), new LR0Element(grammar, "B", [grammar.emptyWord])]);

		const result = algorithms.testCall_closure1(inputElement);

		console.log(mkString(result, _ => _ + ", "));
		expect(expected.equals(result)).toBeTruthy();
	});

	it("Mutual call test", () => {
		/*
		 * S -> A
		 * A -> B
		 * B -> A | b
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A", "B", "b"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B"]);
		grammar.terminals    = new Set("b");
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S", [["A"]]],
			["A", [["B"]]],
			["B", [["A"], ["b"]]]
		]);

		/*
		 * 0 = Closure({ S' -> .S }) = { S' -> .S, S -> .A, A -> .B, B -> .A, B -> .b }
		 * 1 = Jump(0, S) = Closure({ S' -> S. }) = { S' -> .S }
		 * 2 = Jump(0, A) = Closure({ S -> A., B -> A. }) = { S -> A., B -> A. }
		 * 3 = Jump(0, B) = Closure({ A -> B. }) = { A -> B. }
		 * 4 = Jump(0, b) = Closure({ B -> b. }) = { B -> b. }
		 */
		const expected = new LR0CollectionEntry([
			new LR0Element(grammar, "S'", ["S"]),
			new LR0Element(grammar, "S", ["A"]),
			new LR0Element(grammar, "A", ["B"]),
			new LR0Element(grammar, "B", ["A"]),
			new LR0Element(grammar, "B", ["b"]),
		]);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const start  = new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"])]);
		const result = algorithms.testCall_closure(start);

		console.log(mkString(result.toArray(), _ => _ + ", "));

		expect(expected.equals(result)).toBeTruthy();
	});

	it("List test", () => {
		/*
		 * S    -> List
		 * List -> List Pair | Pair
		 * Pair -> ( Pair ) | ( )
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "List", "Pair", "(", ")"]);
		grammar.nonTerminals = new Set(["S'", "S", "List", "Pair"]);
		grammar.terminals    = new Set(["(", ")"]);
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S", [["List"]]],
			["List", [["List", "Pair"], ["Pair"]]],
			["Pair", [["(", "Pair", ")"], ["(", ")"]]]
		]);

		/*
		 * Closure({ S' -> .S }) = { S' -> .S, S -> .List, List -> .List Pair, List -> .Pair, Pair -> .( Pair ), Pair -> .( ) }
		 */
		const expected = new LR0CollectionEntry([
			new LR0Element(grammar, "S'", ["S"]),
			new LR0Element(grammar, "S", ["List"]),
			new LR0Element(grammar, "List", ["List", "Pair"]),
			new LR0Element(grammar, "List", ["Pair"]),
			new LR0Element(grammar, "Pair", ["(", "Pair", ")"]),
			new LR0Element(grammar, "Pair", ["(", ")"]),
		]);

		const algorithms = new AlgorithmModel(grammar);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const start = new LR0CollectionEntry([new LR0Element(grammar, "S'", ["S"])]);
		const result = algorithms.testCall_closure(start);

		console.log(mkString(result.toArray(), _ => _ + ", "));

		expect(expected.equals(result)).toBeTruthy();
	});

	it("LR(1) test", () => {
		const grammar = Grammar.read(`
		S    -> exp
		exp  -> exp cmp exp1 | exp1
		exp1 -> exp1 pm exp2 | exp2
		exp2 -> exp2 md exp3 | exp3
		exp3 -> - exp4 | exp4
		exp4 -> ( exp ) | x | 0
		cmp  -> = | # | < | <= | >= | >
		pm   -> + | -
		md   -> * | /`);
		grammar.addSymbol("$");
		grammar.terminals.add("$");

		const algorithms = new AlgorithmModel(grammar, ParserAlgorithm.LR1);
		algorithms.calculateFirstSets();
		algorithms.calculateFollowSets();
		const start  = new LR1Element(grammar, "S'", ["S"], new ComparableSet(strEqualsFunc, ["$"]));
		const result = algorithms.testCall_lr1Closure1(start);

		console.log(mkString(result.toArray(), _ => _ + ", "));
	})
});

describe("Tests for the jump function", () => {
	it("", () => {
		/*
		 * S -> A
		 * A -> B
		 * B -> A | b
		 */
		let grammar          = new Grammar();
		grammar.symbols      = new Set(["S'", "S", "A", "B", "b"]);
		grammar.nonTerminals = new Set(["S'", "S", "A", "B"]);
		grammar.terminals    = new Set("b");
		grammar.rules        = new Map([
			["S'", [["S"]]],
			["S", [["A"]]],
			["A", [["B"]]],
			["B", [["A"], ["b"]]]
		]);

		const algorithms = new AlgorithmModel(grammar);

		/*
		 * 0 = Closure({ S' -> .S }) = { S' -> .S, S -> .A, A -> .B, B -> .A, B -> .b }
		 * 1 = Jump(0, S) = Closure({ S' -> S. }) = { S' -> .S }
		 * 2 = Jump(0, A) = Closure({ S -> A., B -> A. }) = { S -> A., B -> A. }
		 * 3 = Jump(0, B) = Closure({ A -> B. }) = { A -> B. }
		 * 4 = Jump(0, b) = Closure({ B -> b. }) = { B -> b. }
		 */
		algorithms.test_setCollectionSLR([
			new LR0CollectionEntry([
				new LR0Element(grammar, "S'", ["S"]),
				new LR0Element(grammar, "S", ["A"]),
				new LR0Element(grammar, "A", ["B"]),
				new LR0Element(grammar, "B", ["A"]),
				new LR0Element(grammar, "B", ["b"]),
			])
		]);

		const expectedJump1 = [new LR0Element(grammar, "S'", ["S"])];
		const expectedJump2 = [new LR0Element(grammar, "S", ["A"]), new LR0Element(grammar, "B", ["A"])];
		const expectedJump3 = [new LR0Element(grammar, "A", ["B"])];
		const expectedJump4 = [new LR0Element(grammar, "B", ["b"])];

		const resultJump1 = algorithms.testCall_jump(0, "S");
		const resultJump2 = algorithms.testCall_jump(0, "A");
		const resultJump3 = algorithms.testCall_jump(0, "B");
		const resultJump4 = algorithms.testCall_jump(0, "b");

		console.log(mkString(resultJump1, e => e.toString() + ", "));
		console.log(mkString(resultJump2, e => e.toString() + ", "));
		console.log(mkString(resultJump3, e => e.toString() + ", "));
		console.log(mkString(resultJump4, e => e.toString() + ", "));

		expect(compareArrays(expectedJump1, resultJump1)).toBeTruthy();
		expect(compareArrays(expectedJump2, resultJump2)).toBeTruthy();
		expect(compareArrays(expectedJump3, resultJump3)).toBeTruthy();
		expect(compareArrays(expectedJump4, resultJump4)).toBeTruthy();
	});
});
