import "jest";
import {Comparable, ComparableSet, max, mkString, stableSort, Stack, UniqueQueue} from "../src/util";
import {HasDesc, MappingLeft, NodeKind, NodeType} from "../src/vis-tree/InputModel";

describe("Tests for the comparable set", () => {
	class TestClass implements Comparable<TestClass> {
		x: number;
		y: number;
		z: number;

		constructor(x: number, y: number, z: number) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		equals(other: TestClass): boolean {
			return this.x == other.x && this.y == other.y && this.z == other.z;
		}
	}

	it("Test the add method", () => {
		let set = new ComparableSet<TestClass>((elem, newElem) => elem.equals(newElem));
		set.add(new TestClass(1, 2, 3));
		set.add(new TestClass(1, 2, 3));

		expect(set.getSize()).toStrictEqual(1);
	});

	it("Test the has method", () => {
		let set = new ComparableSet<TestClass>((elem, newElem) => elem.equals(newElem));
		set.add(new TestClass(1, 2, 3));

		expect(set.has(new TestClass(1, 2, 3))).toBeTruthy();
	});

	it("Test the equals method", () => {
		let set1 = new ComparableSet<TestClass>((elem, newElem) => elem.equals(newElem), [new TestClass(1, 2, 3), new TestClass(3, 4, 5)]);
		let set2 = new ComparableSet<TestClass>((elem, newElem) => elem.equals(newElem), [new TestClass(1, 2, 3), new TestClass(3, 4, 5)]);

		expect(set1.equals(set2)).toBeTruthy();
	});
});

describe("Tests for the unique queue", () => {
	it("Test the add method", () => {
		let queue = new UniqueQueue<number>((elem, newElem) => elem === newElem);
		queue.enqueue(1);
		queue.enqueue(1);
		expect(queue.getSize()).toStrictEqual(1);

		queue.enqueue(2);
		expect(queue.getSize()).toStrictEqual(2);
	});
});

describe("Tests for other util functions", () => {
	it("Tests the mkString function", () => {
		expect(mkString(["B"], _ => _ + " ", 1)).toStrictEqual("B")
	});

	it("Tests the max function", () => {
		expect(max(1,2,3,4)).toStrictEqual(4);
		expect(max(-254, 2, -3)).toStrictEqual(2);
		expect(max(-1, -24577, -24526)).toStrictEqual(-1);
	});

	it("Tests the stable merge sort function with numbers", () => {
		const expected = [-1, 0, 2, 2, 2, 4, 5, 6, 8, 23, 636];
		const actual   = stableSort([6,2,-1,23,5,4,2,2,0,8,636]);
		for (let i = 0; i < expected.length; i++) {
			expect(actual[i]).toStrictEqual(expected[i]);
		}
	});

	it("Tests the stable merge sort function with mappings", () => {
		const expected = [
			new MappingLeft("q1", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(true), new HasDesc.Literal(false))),
			new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(false), new HasDesc.Literal(false))),
			new MappingLeft("q2", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Mvar(0), new HasDesc.Literal(false))),
			new MappingLeft("q3", "a", new NodeType(new NodeKind.Mvar(0), new HasDesc.Mvar(0), new HasDesc.Literal(false))),
		];
		const actual = stableSort([
			new MappingLeft("q1", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(true), new HasDesc.Literal(false))),
			new MappingLeft("q3", "a", new NodeType(new NodeKind.Mvar(0), new HasDesc.Mvar(0), new HasDesc.Literal(false))),
			new MappingLeft("q2", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Mvar(0), new HasDesc.Literal(false))),
			new MappingLeft("q0", "a", new NodeType(new NodeKind.Literal("-"), new HasDesc.Literal(false), new HasDesc.Literal(false))),
		], (l1, l2) => {
			let l1Score = 0, l2Score = 0;

			if (l1.typ.kind     instanceof NodeKind.Mvar) l1Score++;
			if (l1.typ.hasLeft  instanceof HasDesc.Mvar)  l1Score++;
			if (l1.typ.hasRight instanceof HasDesc.Mvar)  l1Score++;

			if (l2.typ.kind     instanceof NodeKind.Mvar) l2Score++;
			if (l2.typ.hasLeft  instanceof HasDesc.Mvar)  l2Score++;
			if (l2.typ.hasRight instanceof HasDesc.Mvar)  l2Score++;

			return l1Score <= l2Score;
		});
		for (let i = 0; i < 4; i++) {
			console.log(`Compare ${actual[i]} with ${expected[i]}`);
			expect(actual[i].equals(expected[i])).toBeTruthy();
		}
	});

	it("Tests the stack class", () => {
		const stack = new Stack();
		stack.push(5);
		stack.push(4);
		const top = stack.top();
		console.log(JSON.stringify(stack));
		console.log(top);
		expect(top).toStrictEqual(4);
	});
});
