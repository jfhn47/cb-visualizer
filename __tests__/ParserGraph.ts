import 'jest';
import { ComparableSet } from "../src/util";
import { LR0Element } from "../src/LR0Element";
import { Jumps, CollectionSLR } from "../src/AlgorithmModel";
import { GraphNode, ParserGraphModel } from "../src/ParserGraphModel";
import { Grammar } from '../src/Grammar';

const compareGraphs = (expected: GraphNode, actual: GraphNode): boolean => {
	if (!expected.equals(actual)) {
		return false;
	}

	// TODO: Remove redundancy.
	for (const next of expected.neighbours.keys()) {
		// NOTE: Do not check recursion in graphs
		if (expected.neighbours.get(next)!.equals(expected)) {
			continue;
		}

		if (!actual.neighbours.has(next)) {
			console.log("Actual graph did not contain: " + expected.neighbours.get(next)!.getName());
			return false;
		}

		if (!compareGraphs(expected.neighbours.get(next)!, actual.neighbours.get(next)!)) {
			console.log("Actual did not match expected: Expected = " + expected.neighbours.get(next)!.getName() + ", actual = " + actual.neighbours.get(next)!.getName());
			return false;
		}
	}

	for (const next of actual.neighbours.keys()) {
		// NOTE: Do not check recursion in graphs
		if (actual.neighbours.get(next)!.equals(actual)) {
			continue;
		}

		if (!expected.neighbours.has(next)) {
			console.log("Actual had additional: " + actual.neighbours.get(next)!.getName());
			return false;
		}

		if (!compareGraphs(expected.neighbours.get(next)!, actual.neighbours.get(next)!)) {
			return false;
		}
	}

	return true;
}

describe("Tests for the parser state machine", () => {
	it("Test the parser state machine", () => {
		/*
		 * S’ -> S
		 * S  -> A B
		 * A  -> a
		 * B  -> b B
		 * B  -> epsilon
		 */

		/*
		 * C = { Closure({ S' -> .S }) }
		 * I0 = Closure({ S' -> .S }) = { S' -> .S, S -> .A B, A -> .a }
		 * I1 = Jump(I0, S) = Closure({ S' -> S. }) = { S' -> S. } // reduce
		 * I2 = Jump(I0, A) = Closure({ S -> A .B }) = { S -> A .B, B -> .b B, B -> . } // reduce
		 * I3 = Jump(I0, a) = Closure({ A -> a. }) = { A -> a. } // reduce
		 * I4 = Jump(I2, B) = Closure({ S -> A B. }) = { S -> A B. } // reduce
		 * I5 = Jump(I2, b) = Closure({ B -> b .B }) = { B -> b .B, B -> .b B, B -> . } // reduce
		 * I6 = Jump(I5, B) = Closure({ B -> b B. }) = { B -> b B. } // reduce
		 * Jump(I5, b) = I5
		 *
		 * Reductions: { (I1, S' -> S), (I2, B -> epsilon), (I3, A -> a), (I4, S -> A B), (I5, B -> epsilon), (I6, B -> b B) }
		 */

		const grammar = new Grammar();

		const compareFunc = (elem: LR0Element, newElem: LR0Element) => elem.equals(newElem);

		const config: CollectionSLR = [
			new ComparableSet(compareFunc, [new LR0Element(grammar, "S'", ["S"], 0), new LR0Element(grammar, "S", ["A", "B"], 0), new LR0Element(grammar, "A", ["a"], 0)]),
			new ComparableSet(compareFunc, [new LR0Element(grammar, "S'", ["S"], 1)]),
			new ComparableSet(compareFunc, [new LR0Element(grammar, "S", ["A", "B"], 1), new LR0Element(grammar, "B", ["b", "B"], 0), new LR0Element(grammar, "B", ["epsilon"], 0)]),
			new ComparableSet(compareFunc, [new LR0Element(grammar, "A", ["a"], 1)]),
			new ComparableSet(compareFunc, [new LR0Element(grammar, "S", ["A", "B"], 2)]),
			new ComparableSet(compareFunc, [new LR0Element(grammar, "B", ["b", "B"], 1), new LR0Element(grammar, "B", ["b", "B"], 0), new LR0Element(grammar, "B", ["epsilon"], 0)]),
			new ComparableSet(compareFunc, [new LR0Element(grammar, "B", ["b", "B"], 2)])
		];

		const jumps: Jumps = new Map([
			[[0, "S"], 1],
			[[0, "A"], 2],
			[[0, "a"], 3],
			[[2, "B"], 4],
			[[2, "b"], 5],
			[[5, "B"], 6],
			[[5, "b"], 5]
		]);

		const expectedGraph: GraphNode[] = [
			new GraphNode(0, config[0].toArray()),
			new GraphNode(1, config[1].toArray(), true),
			new GraphNode(2, config[2].toArray()),
			new GraphNode(3, config[3].toArray()),
			new GraphNode(4, config[4].toArray()),
			new GraphNode(5, config[5].toArray()),
			new GraphNode(6, config[6].toArray()),
		];

		expectedGraph[0].addNeighbour("S", expectedGraph[1]);
		expectedGraph[0].addNeighbour("A", expectedGraph[2]);
		expectedGraph[0].addNeighbour("a", expectedGraph[3]);
		expectedGraph[2].addNeighbour("B", expectedGraph[4]);
		expectedGraph[2].addNeighbour("b", expectedGraph[5]);
		expectedGraph[5].addNeighbour("B", expectedGraph[6]);
		expectedGraph[5].addNeighbour("b", expectedGraph[5]);

		console.log("Expected:\n" + expectedGraph.toString());

		// const expectedGraph = new GraphNode("I0", config[0].toArray());
		// expectedGraph.addNeighbour("S", new GraphNode("I1", config[1].toArray()));
		// expectedGraph.addNeighbour("A", new GraphNode("I2", config[2].toArray()));
		// expectedGraph.addNeighbour("a", new GraphNode("I3", config[3].toArray()));
		// expectedGraph.addNeighbour("B", new GraphNode("I4", config[4].toArray()));
		// expectedGraph.addNeighbour("b", new GraphNode("I5", config[5].toArray()));
		// expectedGraph.addNeighbour("B", new GraphNode("I6", config[6].toArray()));
		// expectedGraph.addNeighbour("b", new GraphNode("I5", config[5].toArray())); // Recursive node
		// console.log(expectedGraph.toString());

		const parserStateMachine = new ParserGraphModel(config, jumps);
		console.log("Actual:\n" + parserStateMachine.graphNodes.toString());

		expect(compareGraphs(expectedGraph[0], parserStateMachine.currentState)).toBeTruthy();
	});
});
