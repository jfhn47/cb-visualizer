FROM node:lts-alpine

RUN npm install -g serve

WORKDIR /home/app

COPY ./public ./public
COPY ./package.json ./package.json
COPY ./tsconfig.json ./tsconfig.json
COPY ./.env ./.env
COPY ./src ./src
COPY ./yarn.lock ./yarn.lock

RUN yarn install
RUN yarn build-unix

ENTRYPOINT [ "serve", "-s", "build" ]
